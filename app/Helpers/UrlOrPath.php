<?php

function urlOrPath($str = '', $mode = null)
{
    if (is_null($mode)) {
        $mode = $_ENV['urlOrPath'];
    }
    if (is_null($mode)) {
        $mode = "url";
    }
    if ($mode == "url") {
        return URL::to($str);
    } else {
        return base_path('public/'.$str);
    }
}
