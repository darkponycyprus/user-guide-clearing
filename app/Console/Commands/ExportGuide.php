<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class ExportGuide extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:guide {format} {--section=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert the html user guide into various formats';

    /**
     * Select the export format
     * - docx
     * - pdf
     * @var null
     */
    private $format = null;

    /**
     * The output filename
     *
     * @var null
     */
    private $filename = 'CLR Guide';

    /**
     * Our html guide
     *
     * @var null
     */
    private $html = null;

    private $all_sections = [
        "archive",
        "audits",
        "branches",
        "create-files",
        "foreign-cheques",
        "holidays",
        "jobs",
        "load-files",
        "options",
        "reconciliation",
        "returns",
        "roles",
        "scanners",
        "session",
        "users",
        "web-scan",
    ];

    private $export_sections = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $_ENV['urlOrPath'] = "base";
        //echo urlOrPath('', $_ENV['urlOrPath']);
    }

    public function handle()
    {
        $this->format = $this->argument('format');
        if (!is_null($this->option('section'))) {
            $users_sections = $this->option('section');
            foreach ($this->option('section') as $key => $section) {
                if (!in_array($section, $this->all_sections)) {
                    unset($users_sections[$key]);
                }
            }
            if (!empty($users_sections)) {
               $this->export_sections = $users_sections;
            }
        }
        //$this->htmlSource();

        switch ($this->format) {
            case "docx":
                $this->exportDocx();
                $this->info('Guide exported successfully @ '.date('d-m-Y H.i.s'));
                break;
            case "pdf":
                $this->exportPdf();
                $this->info('Guide exported successfully @ '.date('d-m-Y H.i.s'));
                break;
            default:
                $this->info('Please select a valid export format');
                break;
        }
    }

    private function htmlSource()
    {
        if (is_null($this->export_sections)) {
            $this->export_sections = $this->all_sections;
            //$this->html = view('pages.export.index', [])->render();
        }

        $this->html = '';
        foreach ($this->export_sections as $key => $section) {
            $this->html .= view('pages.help.'.$section.'.export', [])->render();
        }
        $this->html = view('layouts.export.main', ["custom_export" => $this->html])->render();

        return $this->html;
        //dd($this->html);
    }

    private function exportDocx()
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        /*
        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(true);
        $fontStyle->setName('Arial');
        $fontStyle->setSize(16);

        $myTextElement = $section->addText('Hello World');
        $myTextElement->setFontStyle($fontStyle);
        */

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $this->htmlSource());
        //header($this->headerContentType());
        //header('Content-Disposition: attachment;filename="'.$this->filename.'.'.$this->format.'"');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(storage_path('exports/'.$this->filename.' - '.date('d-m-Y H.i.s').'.'.$this->format));
    }

    private function exportPdf()
    {
        //->setOrientation('landscape')
        //->setOption('margin-bottom', 0)
        $pdf = PDF::loadHTML($this->htmlSource())
            ->setPaper('a4')
            ->setOption('toc', true)
            ->setOption('footer-right','[page]')
            ->save(storage_path('exports/'.$this->filename.' - '.date('d-m-Y H.i.s').'.'.$this->format));
    }

    private function headerContentType()
    {
        if ($this->format == "pdf") {
            return 'Content-Type: application/pdf';
        } elseif ($this->format == "docx") {
            return 'Content-Type: application/octet-stream';
        } else {
            return '';
        }
    }
}
