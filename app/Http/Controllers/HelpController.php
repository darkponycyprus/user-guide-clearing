<?php

namespace App\Http\Controllers;

class HelpController
{
    public function __construct()
    {
        $_ENV['urlOrPath'] = "url";
        //echo urlOrPath('');
    }

    /**
     * Help section web scan page
     */
    public function options()
    {
        return view('pages.help.options.index');
    }

    /**
     * Help section web scan page
     */
    public function audits()
    {
        return view('pages.help.audits.index');
    }

    /**
     * Help section web scan page
     */
    public function roles()
    {
        return view('pages.help.roles.index');
    }

    /**
     * Help section web scan page
     */
    public function users()
    {
        return view('pages.help.users.index');
    }

    /**
     * Help section web scan page
     */
    public function webScan()
    {
        return view('pages.help.web-scan.index');
    }

    /**
     * Help section web scan page
     */
    public function holidays()
    {
        return view('pages.help.holidays.index');
    }

    /**
     * Help section reconciliation page
     */
    public function reconciliation()
    {
        return view('pages.help.reconciliation.index');
    }

    /**
     * Help section reconciliation page
     */
    public function scanners()
    {
        return view('pages.help.scanners.index');
    }

    /**
     * Help section reconciliation page
     */
    public function branches()
    {
        return view('pages.help.branches.index');
    }

    /**
     * Help section reconciliation page
     */
    public function jobs()
    {
        return view('pages.help.jobs.index');
    }

    /**
     * Help section archive page
     */
    public function archive()
    {
        return view('pages.help.archive.index');
    }

    /**
     * Help section session page
     */
    public function session()
    {
        return view('pages.help.session.index');
    }

    /**
     * Help section foreignCheques page
     */
    public function foreignCheques()
    {
        return view('pages.help.foreign-cheques.index');
    }

    /**
     * Help section load files page
     */
    public function loadFiles()
    {
        return view('pages.help.load-files.index');
    }

    /**
     * Help section create files page
     */
    public function createFiles()
    {
        return view('pages.help.create-files.index');
    }

    /**
     * Help section returns page
     */
    public function returns()
    {
        return view('pages.help.returns.index');
    }


}
