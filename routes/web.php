<?php

Route::prefix('help')->as('help.')->group(function () {
    Route::get('web-scan', 'HelpController@webScan')->name('web-scan');
    Route::get('reconciliation', 'HelpController@reconciliation')->name('reconciliation');
    Route::get('archive', 'HelpController@archive')->name('archive');
    Route::get('session', 'HelpController@session')->name('session');
    Route::get('foreignCheques', 'HelpController@foreignCheques')->name('foreignCheques');
    Route::get('load-files', 'HelpController@loadFiles')->name('load-files');
    Route::get('create-files', 'HelpController@createFiles')->name('create-files');
    Route::get('returns', 'HelpController@returns')->name('returns');
    Route::get('scanners', 'HelpController@scanners')->name('scanners');
    Route::get('branches', 'HelpController@branches')->name('branches');
    Route::get('jobs', 'HelpController@jobs')->name('jobs');
    Route::get('options', 'HelpController@options')->name('options');
    Route::get('holidays', 'HelpController@holidays')->name('holidays');
    Route::get('audits', 'HelpController@audits')->name('audits');
    Route::get('roles', 'HelpController@roles')->name('roles');
    Route::get('users', 'HelpController@users')->name('users');
});
