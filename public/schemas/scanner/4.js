{
  "title": "Scanner parameters",
  "type": "object",
  "properties": {
    "codeline_start_of": {
      "type": "string",
      "description": "Codeline starting with",
      "maxLength": 2
    },
    "codeline_separator": {
      "type": "string",
      "description": "Codeline separating fields with",
      "maxLength": 2
    },
    "codeline_end_of": {
      "type": "string",
      "description": "Codeline ending with",
      "maxLength": 2
    }
  }
}