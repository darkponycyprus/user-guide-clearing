$(document).ready(function () {
    //handle ajax calls in institutes index eccs switch
    $('body').on('click', "#cheque-status-index input.ajax-checkbox", function() {
        Parallax.Loader.show();

        var statesMap = {"true": 1, "false": 0};
        var field = $(this).data("field");
        var state = $(this).prop("checked");
        var chequeStatusId = $(this).data("status-id");

        var sdata = {'field':field, 'state': statesMap[state], 'chequeStatusId': chequeStatusId};
        $.ajax({ url: Parallax.Route.get('cheque-status.field'), type: "post",
            data: sdata,
            success: function(data) {
                if(data){
                    toastr.options = {"closeButton":true,"debug":false,"newestOnTop":true,"progressBar":true,"positionClass":"toast-top-right","preventDuplicates":true,"onclick":null,"showDuration":"1000","hideDuration":"2000","timeOut":"5000","extendedTimeOut":"5000","showEasing":"swing","hideEasing":"linear","showMethod":"fadeIn","hideMethod":"fadeOut"};
                    toastr.success(Parallax.Lang.get('cheque_status_edit_success'),'');
                }
                Parallax.Loader.hide();
            }
        });
    });
});
