$(document).ready(function () {
    //handle ajax calls in institutes index eccs switch
    $('body').on('click', "#remove-vendor-image-btn", function() {
        Parallax.Loader.show();
        var id = $(this).data("vendor-id");
        var sdata = {'id': id};

        $.ajax({ url: Parallax.Route.get('vendors.image.delete'), type: "post",
            data: sdata,
            success: function(data) {
                toastr.options = {"closeButton":true,"debug":false,"newestOnTop":true,"progressBar":true,"positionClass":"toast-top-right","preventDuplicates":true,"onclick":null,"showDuration":"1000","hideDuration":"2000","timeOut":"5000","extendedTimeOut":"5000","showEasing":"swing","hideEasing":"linear","showMethod":"fadeIn","hideMethod":"fadeOut"};
                if(data == 1){
                    $('#vendor-img').attr('src','img/placeholder.jpg');
                    $('#remove-vendor-image-btn-container').addClass('d-none');
                    $('#add-vendor-image-btn-container').removeClass('d-none');
                    toastr.success(Parallax.Lang.get('vendor_remove_image_success'),'');
                }else{
                    toastr.error(Parallax.Lang.get('vendor_remove_image_error'),'');
                }
                Parallax.Loader.hide();
            }
        });
    });
});
