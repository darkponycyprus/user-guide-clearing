$(document).ready(function () {
    var title = Parallax.Lang.get('current_insitution_title');
    var msg = Parallax.Lang.get('current_insitution_msg');

    //if an inst is added or edited and the current switch turns to true inform that we can only have one current
    $('body').on('click', "#institutions-form input[name='is_active']", function() {
        if($(this).prop("checked")){
            swal(
              title,
              msg,
              'info'
            )
        }
    });

    //handle ajax calls in institutes index eccs switch
    $('body').on('click', "#institutions-index input[name='in_eccs']", function() {
        Parallax.Loader.show();

        var statesMap = {"true": 1, "false": 0};
        var field = 'in_eccs';
        var state = $(this).prop("checked");
        var insId = $(this).data("ins-id");

        var sdata = {'field':field, 'state': statesMap[state], 'insId': insId};
        $.ajax({ url: Parallax.Route.get('institutions.field'), type: "post",
            data: sdata,
            success: function(data) {
                if(data){
                    toastr.options = {"closeButton":true,"debug":false,"newestOnTop":true,"progressBar":true,"positionClass":"toast-top-right","preventDuplicates":true,"onclick":null,"showDuration":"1000","hideDuration":"2000","timeOut":"5000","extendedTimeOut":"5000","showEasing":"swing","hideEasing":"linear","showMethod":"fadeIn","hideMethod":"fadeOut"};
                    toastr.success(Parallax.Lang.get('institution_edit_success'),'');
                }
                Parallax.Loader.hide();
            }
        });
    });
});
