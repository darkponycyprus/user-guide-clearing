$(document).ready(function () {
    $("div[data-branch-id]").addClass('d-none');
    $("div[data-branch-id='"+ $('#branch_id').val() +"']").removeClass('d-none');

    $('body').on('change', '#branch_id', function () {
        $("div[data-branch-id]").addClass('d-none');
        $("div[data-branch-id='"+ $(this).val() +"']").removeClass('d-none');
    });
});