$(document).ready(function () {
    $(document.body).on('click', '.submitForm', function () {
        var element = $(this);
        var task = element.data('task');
        var action = element.data('action');
        var method = element.data('method');
        var form = element.closest('form');

        if (task === "delete") {
            swal({
                title: 'Delete this Item(s)?',
                text: 'These item(s) will be permanently deleted and cannot be recovered.',
                type: 'warning',
                showCloseButton: true,
                showCancelButton: true
            }).then(function(result) {
                if (result.value) {
                    form.attr({action: action});
                    form.find('input[name=_method]').remove();
                    form.find('input[name=task]').remove();
                    $('<input>').attr({type: 'hidden',name: '_method',value: method}).appendTo(form);
                    $('<input>').attr({type: 'hidden',name: 'task',value: task}).appendTo(form);
                    form.submit();
                }
            });
        }
    });

    $("div[data-branch-id]").addClass('d-none');
    $("div[data-branch-id='"+ $('#branch_id').val() +"']").removeClass('d-none');

    $('body').on('change', '#branch_id', function () {
        $("div[data-branch-id]").addClass('d-none');
        $("div[data-branch-id='"+ $(this).val() +"']").removeClass('d-none');
    });
});