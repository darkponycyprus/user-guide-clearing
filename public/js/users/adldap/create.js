$(document).ready(function () {
    $('#duallistbox-users').bootstrapDualListbox({
        filterPlaceHolder: Parallax.Lang.get('users_duallistbox_filterPlaceHolder'),
    });
});