$(document).ready(function () {
    $('.change-status').on('click', function (e) {
        if($(this).hasClass('bg-green')){
            $(this).removeClass('bg-green');
            $(this).addClass('bg-red');
            $(this).removeClass('fa-check');
            $(this).addClass('fa-times');
        }else{
            $(this).addClass('bg-green');
            $(this).removeClass('bg-red');
            $(this).addClass('fa-check');
            $(this).removeClass('fa-times');
        }
    });

    // Donut chart
    // -----------------
    var donutData1 = [{
        'color':'#19AA89',
        'data': 200,
        'label': 'Cheques'
    }, {
        'color': '#f3482c',
        'data': 40,
        'label': 'Returns'
    }];
    var donutData2 = [{
        'color':'#19AA89',
        'data': 100,
        'label': 'Cheques'
    }, {
        'color': '#f3482c',
        'data': 20,
        'label': 'Returns'
    }];
    var donutData3 = [{
        'color':'#19AA89',
        'data': 50,
        'label': 'Cheques'
    }, {
        'color': '#f3482c',
        'data': 15,
        'label': 'Returns'
    }];
    var donutOptions = {
        series: {
            pie: {
                stroke: {
                    width: 0,
                    color: '#a1a1a1'
                },
                show: true,
                innerRadius: 0.5 // This makes the donut shape
            }
        },
        legend: {
            show: false
        }
    };

    function labelFormatter(label, series) {
        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>"    + label + "<br/>" + series.data[0][1] + "%</div>";
    }

    $('#donut-dashboard1').plot(donutData1, donutOptions);
    $('#donut-dashboard2').plot(donutData2, donutOptions);
    $('#donut-dashboard3').plot(donutData3, donutOptions);
    $('#donut-dashboard4').plot(donutData3, donutOptions);

    var areaData = [{
        "label": "Inward Clearing",
        "color": "#19AA89",
        "data": [
            ["Dec 2017", getRandomInt(10000, 40000)],
            ["Jan 2018", getRandomInt(10000, 50000)],
            ["Feb 2018", getRandomInt(10000, 40000)],
            ["Mar 2018", getRandomInt(10000, 40000)],
            ["Apr 2018", getRandomInt(10000, 70000)],
            ["May 2018", getRandomInt(10000, 20000)],
            ["Jun 2018", getRandomInt(10000, 50000)],
            ["Jul 2018", getRandomInt(10000, 80000)],
            ["Aug 2018", getRandomInt(10000, 40000)],
            ["Sep 2018", getRandomInt(10000, 20000)],
            ["Oct 2018", getRandomInt(10000, 70000)],
            ["Nov 2018", getRandomInt(10000, 40000)]
        ]
    }, {
        "label": "Outward Clearing",
        "color": "#009FF2",
        "data": [
            ["Dec 2017", getRandomInt(10000, 40000)],
            ["Jan 2018", getRandomInt(10000, 40000)],
            ["Feb 2018", getRandomInt(10000, 80000)],
            ["Mar 2018", getRandomInt(10000, 70000)],
            ["Apr 2018", getRandomInt(10000, 30000)],
            ["May 2018", getRandomInt(10000, 20000)],
            ["Jun 2018", getRandomInt(10000, 60000)],
            ["Jul 2018", getRandomInt(10000, 50000)],
            ["Aug 2018", getRandomInt(10000, 40000)],
            ["Sep 2018", getRandomInt(10000, 20000)],
            ["Oct 2018", getRandomInt(10000, 40000)],
            ["Nov 2018", getRandomInt(10000, 60000)]
        ]
    }, {
        "label": "Remote Deposit Clearing",
        "color": "#EDA046",
        "data": [
            ["Dec 2017", getRandomInt(10000, 40000)],
            ["Jan 2018", getRandomInt(10000, 80000)],
            ["Feb 2018", getRandomInt(10000, 40000)],
            ["Mar 2018", getRandomInt(10000, 70000)],
            ["Apr 2018", getRandomInt(10000, 30000)],
            ["May 2018", getRandomInt(10000, 20000)],
            ["Jun 2018", getRandomInt(10000, 60000)],
            ["Jul 2018", getRandomInt(10000, 50000)],
            ["Aug 2018", getRandomInt(10000, 80000)],
            ["Sep 2018", getRandomInt(10000, 20000)],
            ["Oct 2018", getRandomInt(10000, 80000)],
            ["Nov 2018", getRandomInt(10000, 40000)]
        ]
    }];
    var areaOptions = {
        series: {
            lines: {
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.5
                    }, {
                        opacity: 0.9
                    }]
                }
            },
            points: {
                show: false
            }
        },
        grid: {
            borderColor: 'rgba(162,162,162,.26)',
            borderWidth: 1,
            hoverable: true,
            backgroundColor: 'transparent'
        },
        tooltip: true,
        tooltipOpts: {
            content: function (label, x, y) {
                return x + ' : &euro;' + y.toLocaleString();
            }
        },
        xaxis: {
            tickColor: 'rgba(162,162,162,.26)',
            font: {
                color: '#FFFFFF'
            },
            mode: 'categories'
        },
        yaxis: {
            min: 0,
            max: 90000,
            tickColor: 'rgba(162,162,162,.26)',
            font: {
                color: '#FFFFFF'
            },
            // position: (isRTL ? 'right' : 'left')
        },
        shadowSize: 0
    };

    $('#processes-flotchart').plot(areaData, areaOptions);

    /**
     * Get a random integer between `min` and `max`.
     *
     * @param {number} min - min number
     * @param {number} max - max number
     * @return {number} a random integer
     */
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
});