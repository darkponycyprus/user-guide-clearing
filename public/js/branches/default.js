$(document).ready(function () {
    //handle ajax calls in branches index cpu switch
    $('body').on('click', "#branches-index input[name='is_cpu']", function() {
        var field = 'is_cpu';
        updateField($(this), field);
    }).on('click', "#branches-index input[name='is_virtual']", function() {
        var field = 'is_virtual';
        updateField($(this), field);
    });
    
    function updateField(elem, field) {
        Parallax.Loader.show();

        var statesMap = {"true": 1, "false": 0};
        var state = elem.prop("checked");
        var branchId = elem.data("branch-id");

        var sdata = {'field':field, 'state': statesMap[state], 'branchId': branchId};
        $.ajax({ url: Parallax.Route.get('branches.field'), type: "post",
            data: sdata,
            success: function(data) {
                if(data){
                    toastr.options = {"closeButton":true,"debug":false,"newestOnTop":true,"progressBar":true,"positionClass":"toast-top-right","preventDuplicates":true,"onclick":null,"showDuration":"1000","hideDuration":"2000","timeOut":"5000","extendedTimeOut":"5000","showEasing":"swing","hideEasing":"linear","showMethod":"fadeIn","hideMethod":"fadeOut"};
                    toastr.success(Parallax.Lang.get('branch_edit_success'),'');
                }
                Parallax.Loader.hide();
            }
        });
    }
});
