$(document).ready(function () {
    var tdWidths = [];
    $('#main-table').find('tbody .visible-row td').each(function(id, value){
        tdWidths.push($(value).width());
    });

    $('#main-table').find('tbody .hidden-row').each(function(id, value){
        $(value).find('.inner-td').each(function(id2, value2){
            $(value2).width(tdWidths[id2]);
        });
    });
    $('#collapse-cheques').on('show.bs.collapse', function () {
        $('#cheques-totals').find('td span').slice(2).hide();
    }).on('hide.bs.collapse', function () {
        $('#cheques-totals').find('td span').slice(2).show();
    });
    $('#collapse-returns').on('show.bs.collapse', function () {
        $('#returns-totals').find('td span').slice(2).hide();
    }).on('hide.bs.collapse', function () {
        $('#returns-totals').find('td span').slice(2).show();
    });
    $(document).on('click', '#validate', function (e) {
        Parallax.Loader.show();
        e.preventDefault();
        $.ajax({
            url: Parallax.Route.get('create-files.eccs.validation'),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    });
    $(document).on('click', '#create', function (e) {
        Parallax.Loader.show();
        e.preventDefault();
        $.ajax({
            url: Parallax.Route.get('create-files.eccs.create'),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    });
});