$( document ).ready(function() {
    $('#duallistbox-dbtables').bootstrapDualListbox({
        filterPlaceHolder: Parallax.Lang.get('dbtables_duallistbox_filterPlaceHolder'),
    });
    $(".bootstrap-duallistbox-container").find("*").prop("disabled",true);
});