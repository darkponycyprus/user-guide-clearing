$(document).ready(function () {
    $('body').on('click', '.unlock', function (e) {
        e.preventDefault();
        var buttonClicked = $(this);

        swal({
            title: 'Are you sure?',
            text: $(this).data('message'),
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, unlock it!'
        }).then(function(result) {
            if (result.value) {
                buttonClicked.closest('form').submit();
            }
        });
    }).on('click', '.restore', function (e) {
        var buttonClicked = $(this);

        swal({
            title: 'Are you sure?',
            text: $(this).data('message'),
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, restore it!'
        }).then(function(result) {
            if (result.value) {
                buttonClicked.closest('form').submit();
            }
        });
    });
});