$(document).ready(function () {
    $('.edit-holiday').on('click', function(){
        $("input[name='holiday_id_to_update']").val($(this).data('holiday-id'));
        $('#holiday-modal').attr('data-mode', 'update');
        $('#holiday-modal .modal-body #holiday-rows-container').html(holidayRowHtml);
        $('#holiday-rows-container .holiday-datepicker').val($(this).data('date'));
        $('#holiday-rows-container .holiday-name-input').val($(this).data('name'));
    });
    $('.add-holiday').on('click', function(){
        $("input[name='holiday_id_to_update']").val('');
        $('#holiday-modal').attr('data-mode', 'add');
    });
    $('#add-single-row-modal-holiday').on("click", function(e){
        e.preventDefault();
        $('#holiday-modal .modal-body #holiday-rows-container').append(holidayRowHtml);
    });
    $('body').on("click", '.remove-history-row', function(e){
        e.preventDefault();
        $(this).parents('.row').remove();
    });
    $("button[data-target='#holiday-modal']").on('click', function(){
        $('#holiday-modal .modal-body #holiday-rows-container').html('');
    });
});