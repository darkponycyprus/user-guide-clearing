$(document).ready(function () {
    $(document).on('click', '#load-files', function (e) {
        var session_id = $('#session_id').val();
        if(session_id !== '') {
            e.preventDefault();
            Parallax.Loader.show();
            $.ajax({
                url : Parallax.Route.get('load-files.xml.was-processed', {session_id: session_id}),
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    if(data.processed == 1) {
                        swal({
                            title: data.title,
                            html: data.message,
                            type: 'warning',
                            reverseButtons: true,
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: Parallax.Lang.get('buttons.reload'),
                            cancelButtonText: Parallax.Lang.get('buttons.cancel')
                        }).then(function(result) {
                            if (result.value) {
                                loadSessionFiles(1);
                            }
                        });
                    } else {
                        loadSessionFiles(0);
                    }
                } else {
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        } else {
            toastr.error(Parallax.Lang.get('load_files.session_required'), '');
        }
    }).on('click', '#load-items', function (e) {
        var session_id = $('input[name=session_id]').val();
        if(session_id !== '') {
            e.preventDefault();
            Parallax.Loader.show();
            $.ajax({
                url : Parallax.Route.get('load-files.xml.were-imported', {session_id: session_id}),
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    if(data.imported == true) {
                        swal({
                            title: data.title,
                            html: data.message,
                            type: 'error',
                            reverseButtons: true,
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: Parallax.Lang.get('buttons.ok')
                        }).then(function(result) {
                            if (result.value) {
                                window.location.href = Parallax.Route.get('load-files.xml.index');
                            }
                        });
                    } else {
                        loadItems()
                    }
                } else {
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        } else {
            toastr.error(Parallax.Lang.get('load_files.eos_file_required'), '');
        }
    }).on('click', '#import', function (e) {
        var session_id = $('input[name=session_id]').val();
        if(session_id !== '') {
            e.preventDefault();
            Parallax.Loader.show();
            $.ajax({
                url : Parallax.Route.get('load-files.xml.was-imported', {session_id: session_id}),
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    if(data.imported == 1) {
                        swal({
                            title: data.title,
                            html: data.message,
                            type: 'warning',
                            reverseButtons: true,
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: Parallax.Lang.get('buttons.yes'),
                            cancelButtonText: Parallax.Lang.get('buttons.no')
                        }).then(function(result) {
                            if (result.value) {
                                $('input[name=import]').val(1);
                                importing();
                            }
                        });
                    } else {
                        importing();
                    }
                } else {
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        } else {
            toastr.error(Parallax.Lang.get('load_files.session_required'), '');
        }
    })
});

function loadSessionFiles(reloadSession) {
    reloadSession = reloadSession || 0;
    var session_id = $('#session_id').val();
    Parallax.Loader.show();
    $.ajax({
        url: Parallax.Route.get('load-files.xml.load-session'),
        type: 'POST',
        dataType: 'json',
        data: {session_id: session_id, reload: reloadSession}
    }).done(function (data) {
        if (data.return === true) {
            Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
        } else {
            toastr.error(data.message, '');
        }
        Parallax.Loader.hide();
    });
}

function loadItems() {
    var session_id = $('input[name=session_id]').val();
    Parallax.Loader.show();
    $.ajax({
        url: Parallax.Route.get('load-files.xml.load-items'),
        type: 'POST',
        dataType: 'json',
        data: {session_id: session_id}
    }).done(function (data) {
        if (data.return === true) {
            Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
        } else {
            toastr.error(data.message, '');
        }
        Parallax.Loader.hide();
    });
}

function importing() {
    var session_id = $('input[name=session_id]').val();
    Parallax.Loader.show();
    $.ajax({
        url: Parallax.Route.get('load-files.xml.import'),
        type: 'POST',
        dataType: 'json',
        data: {session_id: session_id}
    }).done(function (data) {
        if (data.return === true) {
            Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
        } else {
            toastr.error(data.message, '');
        }
        Parallax.Loader.hide();
    });
}