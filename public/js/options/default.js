$( document ).ready(function() {
    // create the schema editor
    var container = document.getElementById("jsoneditor");
    var editor = new JSONEditor(container, {
        mode: 'code',
        modes: ['code', 'tree'],
        onChange: function(){
            $('#json-settings').val(editor.getText());
        }
    });
    var settings = $('#json-settings').val();
    if(settings === '') {
        editor.set({});
    } else {
        editor.set($.parseJSON(settings));
    }
});