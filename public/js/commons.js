(window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || []).push([[3],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var common_1 = __webpack_require__(1);
var loader_1 = __webpack_require__(2);
var router_1 = __importDefault(__webpack_require__(3));
var cheque_1 = __webpack_require__(5);
var translator_1 = __importDefault(__webpack_require__(7));
var process_1 = __importDefault(__webpack_require__(4));
var image_1 = __webpack_require__(9);
var keys_1 = __importDefault(__webpack_require__(10));
var data_entry_1 = __webpack_require__(11);
var data_entry_2 = __webpack_require__(13);
var progress_bar_1 = __webpack_require__(14);
var rejected_documents_1 = __webpack_require__(17);
var rejected_documents_2 = __webpack_require__(19);
var rejected_documents_3 = __webpack_require__(20);
var data_entry_3 = __webpack_require__(21);
var technical_control_1 = __webpack_require__(22);
var held_cheques_1 = __webpack_require__(24);
var codeline_generator_1 = __webpack_require__(26);
var Parallax = common_1.Parallax;
Parallax.Loader = loader_1.Loader;
Parallax.Process = process_1.default;
Parallax.Route = router_1.default;
Parallax.Lang = translator_1.default;
Parallax.Validators = {
    Cheque: cheque_1.Cheque
};
Parallax.Plugins = {
    Image: image_1.Image,
    Keys: keys_1.default
};
Parallax.Task = {
    ProgressBar: progress_bar_1.ProgressBar
};
Parallax.TradeFinance = {
    BatchDataEntry: data_entry_2.TradeFinanceBatchDataEntry,
    BatchRejectedDocuments: rejected_documents_2.TradeFinanceBatchRejectedDocuments,
    CustomQueryDataEntry: data_entry_2.TradeFinanceCustomQueryDataEntry,
    CustomQueryRejectedDocuments: rejected_documents_2.TradeFinanceCustomQueryRejectedDocuments
};
Parallax.Clearing = {
    BatchDataEntry: data_entry_1.ClearingBatchDataEntry,
    BatchRejectedDocuments: rejected_documents_1.ClearingBatchRejectedDocuments,
    CustomQueryDataEntry: data_entry_1.ClearingCustomQueryDataEntry,
    CustomQueryRejectedDocuments: rejected_documents_1.ClearingCustomQueryRejectedDocuments,
    CodelineGenerator: codeline_generator_1.ClearingBatchCodelineGenerator,
    CustomQueryCodelineGenerator: codeline_generator_1.ClearingCustomQueryCodelineGenerator,
    TechnicalControl: technical_control_1.ClearingTechnicalControl,
    HeldCheques: held_cheques_1.ClearingHeldCheques
};
Parallax.ForeignCheques = {
    BatchDataEntry: data_entry_3.ForeignChequesBatchDataEntry,
    BatchRejectedDocuments: rejected_documents_3.ForeignChequesBatchRejectedDocuments,
    CustomQueryDataEntry: data_entry_3.ForeignChequesCustomQueryDataEntry,
    CustomQueryRejectedDocuments: rejected_documents_3.ForeignChequesCustomQueryRejectedDocuments
};
module.exports = Parallax;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var registeredComponents = {};
exports.Parallax = {
    version: '1.0',
    baseUrl: '',
    registeredComponent: function (identity, component) {
        registeredComponents[identity] = component;
    },
    getComponent: function (identity) {
        return registeredComponents[identity];
    },
    namespace: function (ns) {
        var autoCreate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
        var nsParts = ns.split('.');
        var root = window;
        for (var partIndex = 0; partIndex < nsParts.length; partIndex++) {
            if (typeof root[nsParts[partIndex]] === 'undefined') {
                if (autoCreate) {
                    root[nsParts[partIndex]] = {};
                }
                else {
                    return undefined;
                }
            }
            root = root[nsParts[partIndex]];
        }
        return root;
    },
    onReady: function (callback) {
        document.addEventListener("DOMContentLoaded", function (e) {
            callback();
        });
    }
};


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Loader = /** @class */ (function () {
    function Loader() {
    }
    Loader.show = function (message) {
        if (message === void 0) { message = ''; }
        message = message || '';
        $('#full-screen-message').text(message);
        $('#full-screen-loading').addClass('open');
    };
    Loader.hide = function () {
        $('#full-screen-loading').removeClass('open');
        $('#full-screen-message').text('');
    };
    return Loader;
}());
exports.Loader = Loader;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = __webpack_require__(1);
var process_1 = __importDefault(__webpack_require__(4));
var Router = /** @class */ (function () {
    function Router() {
        this.baseUrl = '';
        this.template = '';
    }
    Router.prototype.add = function (routes) {
        if (typeof routes['routes'] === "undefined") {
            throw new Error("Router Error: The named routes are not set.");
        }
        this.routes = routes['routes'];
        this.baseUrl = routes['baseUrl'];
    };
    Router.prototype.get = function (name, params) {
        var _this = this;
        if (typeof name === 'undefined') {
            throw new Error('Router Error: You must provide a route name');
        }
        var route = undefined;
        route = this.routes[name];
        if (typeof route === 'undefined') {
            //Give priority to a route with a prefix.
            //Starts with polyfill.
            if (process_1.default.getPrefix() !== '') {
                if (name.substring(0, process_1.default.getPrefix().length) === process_1.default.getPrefix()) {
                    route = this.routes[name];
                }
                else {
                    route = this.routes[process_1.default.getPrefix() + '.' + name];
                }
            }
        }
        if (typeof route === 'undefined') {
            throw new Error("Router Error: route '" + name + "' is not found in the routes list");
        }
        /**
         * If the common base url is not set then get the base url from initialisation.
         */
        if (this.baseUrl === '') {
            if (common_1.Parallax.baseUrl === '') {
                throw new Error("Router Error: The base url was not set.");
            }
            else {
                this.baseUrl = common_1.Parallax.baseUrl;
            }
        }
        this.template = this.baseUrl + route.uri;
        var urlParams = this.normalizeParams(params);
        var queryParams = this.normalizeParams(params);
        var tags = urlParams, paramsArrayKey = 0;
        return this.template.replace(/{([^}]+)}/gi, function (tag, i) {
            var keyName = tag.replace(/{|}|\?/g, ''), key = _this.numericParamIndices ? paramsArrayKey : keyName;
            paramsArrayKey++;
            if (typeof tags[key] !== 'undefined') {
                delete queryParams[key];
                return tags[key].id || encodeURIComponent(tags[key]);
            }
            if (tag.indexOf('?') === -1) {
                throw new Error("Router Error: '" + keyName + "' key is required for route '" + name + "'");
            }
            else {
                return '';
            }
        });
    };
    Router.prototype.normalizeParams = function (params) {
        if (typeof params === 'undefined')
            return {};
        params = typeof params !== 'object' ? [params] : params;
        if (params.hasOwnProperty('id') && this.template.indexOf('{id}') == -1) {
            params = [params.id];
        }
        this.numericParamIndices = Array.isArray(params);
        var obj = {};
        return __assign({}, obj, params);
    };
    return Router;
}());
exports.Router = Router;
exports.default = new Router();


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Process = /** @class */ (function () {
    function Process() {
        this.id = 0;
        this.prefix = '';
    }
    Process.prototype.set = function (id, prefix) {
        this.id = id;
        this.prefix = prefix;
    };
    Process.prototype.getPrefix = function () {
        return this.prefix;
    };
    return Process;
}());
module.exports = new Process();


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = __webpack_require__(6);
var luhn_1 = __webpack_require__(8);
/**
 * ChequeValidation Class.
 */
var Cheque = /** @class */ (function (_super) {
    __extends(Cheque, _super);
    /**
     * ChequeValidation Constructor.
     *
     * @param config
     */
    function Cheque(config) {
        var _this = _super.call(this, config) || this;
        _this.valid_banks = [];
        _this.id = _this.getConfigParam('id');
        _this.validation_rules = _this.getConfigParam('validation_rules');
        _this.padding = _this.getConfigParam('padding');
        _this.valid_banks = _this.getConfigParam('valid_banks');
        return _this;
    }
    /**
     * Check if the cheque is valid.
     *
     * @param {ChequeInterface} cheque
     *
     * @returns {boolean}
     */
    Cheque.prototype.isValid = function (cheque) {
        return (!this.checkDigit1CodeLineRules(cheque) || !this.checkDigit2CodeLineRules(cheque));
    };
    /**
     * Check if it has a valid trans code.
     *
     * @param {ChequeInterface} cheque
     *
     * @returns {boolean}
     */
    Cheque.prototype.hasValidTransCode = function (cheque) {
        return this.inArray(cheque.trans_code, this.validation_rules.trans_codes);
    };
    /**
     * Check if it has a valid bank code.
     *
     * @param {ChequeInterface}  cheque
     *
     * @returns {boolean}
     */
    Cheque.prototype.hasValidBankCode = function (cheque) {
        return this.inArray(cheque.bank_code, this.valid_banks);
    };
    /**
     * Check if the code line until check digit 1 is valid.
     *
     * @param {ChequeInterface} cheque
     *
     * @returns {boolean}
     */
    Cheque.prototype.hasValidCheckDigit1CodeLine = function (cheque) {
        return this.checkDigit1CodeLineRules(cheque);
    };
    /**
     * Check if the code line until check digit 2 is valid.
     *
     * @param {ChequeInterface} item
     *
     * @returns {boolean}
     */
    Cheque.prototype.hasValidCheckDigit2CodeLine = function (item) {
        return this.checkDigit2CodeLineRules(item);
    };
    /**
     * Check if the Cheque No has a valid string length.
     *
     * @param {string} cheque_no
     *
     * @return boolean
     */
    Cheque.prototype.hasValidChequeNoLength = function (cheque_no) {
        return cheque_no.length === this.padding.cheque_no;
    };
    /**
     * Check if the Cheque Account No has a valid string length.
     *
     * @param cheque_account_no
     */
    Cheque.prototype.hasValidChequeAccountNoLength = function (cheque_account_no) {
        return cheque_account_no.length === this.padding.cheque_account_no;
    };
    /**
     * Set the rules for the code line until check digit 1.
     *
     * @param {ChequeInterface} cheque
     *
     * @return boolean
     */
    Cheque.prototype.checkDigit1CodeLineRules = function (cheque) {
        if (!this.isValidTransCode(cheque.trans_code)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_trans_code'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.trans_code)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_trans_code'));
                return false;
            }
        }
        if (!this.isValidChequeNo(cheque.cheque_no)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_cheque_no'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.cheque_no)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_cheque_no'));
                return false;
            }
        }
        if (!this.isValidBankCode(cheque.bank_code)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_bank_code'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.bank_code)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_bank_code'));
                return false;
            }
        }
        if (!this.isValidChequeAccountNo(cheque.cheque_account_no)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_cheque_account_no'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.cheque_account_no)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_cheque_account_no'));
                return false;
            }
        }
        if (!this.isValidCheckDigit1(cheque.check_digit_1)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_check_digit_1'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.check_digit_1)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_check_digit_1'));
                return false;
            }
        }
        if (!this.isValidCheckDigit1Luhn(cheque.trans_code, cheque.cheque_no, cheque.bank_code, cheque.cheque_account_no, cheque.check_digit_1)) {
            toastr.error(this.translator.get('reconciliation.validation.wrong_check_digit_1'));
            return false;
        }
        return true;
    };
    /**
     * Set the rules for the code line until check digit 2.
     *
     * @param {ChequeInterface} cheque
     *
     * @return boolean
     */
    Cheque.prototype.checkDigit2CodeLineRules = function (cheque) {
        if (!this.isValidAmount(cheque.amount)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.amount)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_amount'));
                return false;
            }
        }
        if (!this.isValidCheckDigit2(cheque.check_digit_2)) {
            toastr.error(this.translator.get('reconciliation.validation.empty_check_digit_2'));
            return false;
        }
        else {
            if (!this.isNumber(+cheque.check_digit_2)) {
                toastr.error(this.translator.get('reconciliation.validation.not_numeric_check_digit_2'));
                return false;
            }
        }
        if (!this.isValidCheckDigit2Luhn(cheque.amount, cheque.check_digit_2)) {
            toastr.error(this.translator.get('reconciliation.validation.wrong_check_digit_2'));
            return false;
        }
        return true;
    };
    /**
     * Calculate and check the check digit 1 Luhn.
     *
     * @param {string} trans_code
     * @param {string} cheque_no
     * @param {string} bank_code
     * @param {string} cheque_account_no
     * @param {string} check_digit_1
     *
     * @returns {boolean}
     */
    Cheque.prototype.isValidCheckDigit1Luhn = function (trans_code, cheque_no, bank_code, cheque_account_no, check_digit_1) {
        if (luhn_1.Luhn(this.pad(trans_code, this.padding.trans_code) +
            this.pad(cheque_no, this.padding.cheque_no) +
            this.pad(bank_code, this.padding.bank_code) +
            this.pad(cheque_account_no, this.padding.cheque_account_no)) !== +check_digit_1) {
            return false;
        }
        return true;
    };
    /**
     * Calculate and check the check digit 2 Luhn.
     *
     * @param {string} amount
     * @param {string} check_digit_2
     *
     * @returns {boolean}
     */
    Cheque.prototype.isValidCheckDigit2Luhn = function (amount, check_digit_2) {
        if (luhn_1.Luhn(this.pad(amount, this.padding.amount)) !== +check_digit_2) {
            return false;
        }
        return true;
    };
    /**
     * Check if the Trans Code is null or an empty string.
     *
     * @param {string} trans_code
     *
     * @return boolean
     */
    Cheque.prototype.isValidTransCode = function (trans_code) {
        if (trans_code === null || trans_code === '') {
            return false;
        }
        return true;
    };
    /**
     * Check if the Cheque No is null or an empty string.
     *
     * @param {string} cheque_no
     *
     * @return boolean
     */
    Cheque.prototype.isValidChequeNo = function (cheque_no) {
        if (cheque_no === null || cheque_no === '') {
            return false;
        }
        return true;
    };
    /**
     * Check if the Bank Code is null or an empty string.
     *
     * @param {string} bank_code
     *
     * @return boolean
     */
    Cheque.prototype.isValidBankCode = function (bank_code) {
        if (bank_code === null || bank_code === '') {
            return false;
        }
        return true;
    };
    /**
     * Check if the Cheque Account No is null or an empty string.
     *
     * @param {string} cheque_account_no
     *
     * @return boolean
     */
    Cheque.prototype.isValidChequeAccountNo = function (cheque_account_no) {
        if (cheque_account_no === null || cheque_account_no === '') {
            return false;
        }
        return true;
    };
    /**
     * Check if the CD1 is null or an empty string and if CD1 has any other number except between 0 and 9.
     *
     * @param {string} cd1
     *
     * @return boolean
     */
    Cheque.prototype.isValidCheckDigit1 = function (cd1) {
        if (cd1 === null || cd1 === '' || parseInt(cd1) < 0 || parseInt(cd1) > 9) {
            return false;
        }
        return true;
    };
    /**
     * Check if the Amount is null, an empty string or has a negative value.
     *
     * @param {string} amount
     *
     * @return boolean
     */
    Cheque.prototype.isValidAmount = function (amount) {
        if (amount === null || amount === '' || +amount <= 0) {
            return false;
        }
        return true;
    };
    /**
     * Check if the CD2 is null or an empty string and if CD2 has any other number except between 0 and 9.
     *
     * @param {string} cd2
     *
     * @return boolean
     */
    Cheque.prototype.isValidCheckDigit2 = function (cd2) {
        if (cd2 === null || cd2 === '' || +cd2 < 0 || +cd2 > 9) {
            return false;
        }
        return true;
    };
    /**
     * Check if the MICR codeline is empty or null.
     *
     * @param {string} micr
     *
     * @return boolean
     */
    Cheque.prototype.isValidMicr = function (micr) {
        if (micr === null || micr === '') {
            return false;
        }
        return true;
    };
    /**
     * Check if the string is not a number.
     *
     * @param {number} number
     *
     * @return boolean
     */
    Cheque.prototype.isNumber = function (number) {
        if (isNaN(+number)) {
            return false;
        }
        return true;
    };
    /**
     * Check if the deposit date is valid date and can be deposited.
     *
     * @param {string} date
     *
     * @return boolean
     */
    Cheque.prototype.canDeposit = function (date) {
        var deposit_date = moment.utc(date, 'DD/MM/YYYY', true);
        if (!this.isValidDate(deposit_date)) {
            return false;
        }
        var today = moment.utc(new Date());
        var duration = moment.duration(today.diff(deposit_date));
        if (typeof this.validation_rules.max_deposit_date_months == "undefined") {
            return true;
        }
        else {
            return (duration.asMonths() < parseFloat(this.validation_rules.max_deposit_date_months));
        }
    };
    /**
     * Check if it's a valid date.
     *
     * @param {string} date_string
     *
     * @return boolean
     */
    Cheque.prototype.isValidDate = function (date_string) {
        var date = moment.utc(date_string, 'DD/MM/YYYY', true);
        return date.isValid();
    };
    /**
     * Padding left with "0"
     *
     * @param str
     * @param max
     *
     * @return string
     */
    Cheque.prototype.pad = function (str, max) {
        str = str.toString();
        return str.length < max ? this.pad("0" + str, max) : str;
    };
    /**
     * Is it in array?
     *
     * @param needle
     * @param haystack
     *
     * @return boolean
     */
    Cheque.prototype.inArray = function (needle, haystack) {
        var count = haystack.length;
        for (var i = 0; i < count; i++) {
            if (haystack[i] === needle) {
                return true;
            }
        }
        return false;
    };
    return Cheque;
}(component_1.Component));
exports.Cheque = Cheque;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = __webpack_require__(1);
var translator_1 = __importDefault(__webpack_require__(7));
var Component = /** @class */ (function () {
    function Component(config) {
        this.id = '';
        this.translator = translator_1.default;
        this.config = config;
        this.id = this.getConfigParam('id');
        this.componentElement = $('#' + this.id);
        this.registerComponent();
        this.setTranslator(this.getConfigParam('translations'));
    }
    Component.prototype.getConfigParam = function (name, defaultvalue) {
        return this.hasConfigParam(name) ? this.config[name] : defaultvalue;
    };
    Component.prototype.hasConfigParam = function (name) {
        return 'undefined' !== typeof this.config[name];
    };
    Component.createComponent = function (config) {
        if (!config.componentType || config instanceof Component) {
            return config;
        }
        var Class = common_1.Parallax.namespace(config.componentType);
        if (!Class) {
            throw new Error('Unrecognized class name: ' + config.componentType);
        }
        return new Class(config);
    };
    Component.prototype.registerComponent = function () {
        common_1.Parallax.registeredComponent(this.id, this);
    };
    Component.prototype.setTranslator = function (translations) {
        this.translator.addMessages(translations);
    };
    Component.prototype.getTranslator = function () {
        return this.translator;
    };
    Component.prototype.hide = function () {
        this.componentElement.hide();
    };
    Component.prototype.show = function () {
        this.componentElement.show();
    };
    return Component;
}());
exports.Component = Component;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Translator = /** @class */ (function () {
    function Translator() {
        this.messages = {};
        // Derived from: https://github.com/symfony/translation/blob/460390765eb7bb9338a4a323b8a4e815a47541ba/Interval.php
        this.intervalRegexp = /^({\s*(\-?\d+(\.\d+)?[\s*,\s*\-?\d+(\.\d+)?]*)\s*})|([\[\]])\s*(-Inf|\*|\-?\d+(\.\d+)?)\s*,\s*(\+?Inf|\*|\-?\d+(\.\d+)?)\s*([\[\]])$/;
        this.anyIntervalRegexp = /({\s*(\-?\d+(\.\d+)?[\s*,\s*\-?\d+(\.\d+)?]*)\s*})|([\[\]])\s*(-Inf|\*|\-?\d+(\.\d+)?)\s*,\s*(\+?Inf|\*|\-?\d+(\.\d+)?)\s*([\[\]])/;
    }
    Translator.prototype.get = function (key, replacements, boolean) {
        if (replacements === void 0) { replacements = null; }
        if (boolean === void 0) { boolean = false; }
        var parts = key.split("."), length = parts.length, i = 0, message = this.messages;
        for (i; i < length; i++) {
            message = message[parts[i]];
        }
        if (typeof message === "undefined") {
            if (!boolean) {
                return key;
            }
            else {
                return false;
            }
        }
        if (replacements) {
            return this.applyReplacements(message, replacements);
        }
        return message;
    };
    Translator.prototype.choice = function (key, count, replacements) {
        if (replacements === void 0) { replacements = null; }
        var message = this.get(key, replacements, true);
        if (message !== false) {
            return key;
        }
        var messageSplitted = message.split('|');
        // Get the explicit rules, If any
        var explicitRules = [];
        for (var i = 0; i < messageSplitted.length; i++) {
            messageSplitted[i] = messageSplitted[i].trim();
            if (this.anyIntervalRegexp.test(messageSplitted[i])) {
                var messageSpaceSplit = messageSplitted[i].split(/\s/);
                explicitRules.push(messageSpaceSplit.shift());
                messageSplitted[i] = messageSpaceSplit.join(' ');
            }
        }
        // Check if there's only one message
        if (messageSplitted.length === 1) {
            // Nothing to do here
            return message;
        }
        // Check the explicit rules
        for (var j = 0; j < explicitRules.length; j++) {
            if (this.testInterval(count, explicitRules[j])) {
                return messageSplitted[j];
            }
        }
        if (count == 1) {
            return messageSplitted[0];
        }
        else {
            return messageSplitted[1];
        }
    };
    Translator.prototype.addMessages = function (_messages) {
        for (var key in _messages) {
            this.messages[key] = _messages[key];
        }
    };
    Translator.prototype.has = function (key) {
        if (typeof key !== 'string' || !this.messages) {
            return false;
        }
        return !(this.get(key, null, true) === false);
    };
    Translator.prototype.testInterval = function (count, interval) {
        if (typeof interval !== 'string') {
            throw 'Invalid interval: should be a string.';
        }
        interval = interval.trim();
        var matches = interval.match(this.intervalRegexp);
        if (!matches) {
            throw 'Invalid interval: ' + interval;
        }
        if (matches[2]) {
            var items = matches[2].split(',');
            for (var i = 0; i < items.length; i++) {
                if (parseInt(items[i], 10) === count) {
                    return true;
                }
            }
        }
        else {
            // Remove falsy values.
            matches = matches.filter(function (match) {
                return !!match;
            });
            var leftDelimiter = matches[1];
            var leftNumber = this.convertNumber(matches[2]);
            if (leftNumber === Infinity) {
                leftNumber = -Infinity;
            }
            var rightNumber = this.convertNumber(matches[3]);
            var rightDelimiter = matches[4];
            return (leftDelimiter === '[' ? count >= leftNumber : count > leftNumber)
                && (rightDelimiter === ']' ? count <= rightNumber : count < rightNumber);
        }
        return false;
    };
    Translator.prototype.convertNumber = function (str) {
        if (str === '-Inf') {
            return -Infinity;
        }
        else if (str === '+Inf' || str === 'Inf' || str === '*') {
            return Infinity;
        }
        return parseInt(str, 10);
    };
    Translator.prototype.applyReplacements = function (message, replacements) {
        for (var replacementName in replacements) {
            var replacement = replacements[replacementName];
            // 'welcome' => 'Welcome, :name' => 'Welcome, dayle'
            message = message.replace(new RegExp(':' + replacementName, 'g'), replacement);
            // 'welcome' => 'Welcome, :NAME' => 'Welcome, DAYLE'
            message = message.replace(new RegExp(':' + String(replacementName).toUpperCase(), 'g'), String(replacement).toUpperCase());
            // 'welcome' => 'Welcome, :Name' => 'Welcome, Dayle'
            message = message.replace(new RegExp(':' + (String(replacementName).charAt(0).toUpperCase() + String(replacementName).substr(1)), 'g'), String(replacement).charAt(0).toUpperCase() + String(replacement).substr(1));
        }
        return message;
    };
    return Translator;
}());
exports.default = new Translator;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function Luhn(originalStr) {
    var sum = 0, delta = [0, 1, 2, 3, 4, -4, -3, -2, -1, 0];
    for (var i = 0; i < originalStr.length; i++) {
        sum += parseInt(originalStr.substring(i, i + 1));
    }
    for (var i = originalStr.length - 1; i >= 0; i -= 2) {
        sum += delta[parseInt(originalStr.substring(i, i + 1))];
    }
    if (10 - (sum % 10) === 10) {
        return 0;
    }
    return 10 - (sum % 10);
}
exports.Luhn = Luhn;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var loader_1 = __webpack_require__(2);
var router_1 = __importDefault(__webpack_require__(3));
var Image = /** @class */ (function () {
    function Image(element, routes) {
        if (routes === void 0) { routes = {}; }
        this.Images = [];
        this.allowDrag = 0;
        this.initial = { x: 0, y: 0 };
        this.cursorDistanceDiff = { x: 0, y: 0 };
        this.lastTranslated = { x: 0, y: 0 };
        this.translated = { x: 0, y: 0 };
        this.zoomed = 1;
        this.rotated = 0;
        this.routes = {
            rotate: 'image.rotate',
            swap: 'image.swap'
        };
        this.routes = __assign({}, this.routes, routes);
        this.jQueryElement = $(element).find('.image-manipulation-plugin');
        this.mainImg = this.jQueryElement.find('.image-manipulation-main');
        this.rotateBtn = this.jQueryElement.find('.rotate-img-btn');
        this.simpleRotateBtn = this.jQueryElement.find('.simple-rotate-img-btn');
        this.flipBtn = this.jQueryElement.find('.flip-img-btn');
        this.swapBtn = this.jQueryElement.find('.swap-img-btn');
        this.resetBtn = this.jQueryElement.find('.reset-img-btn');
        this.zoomSlider = this.jQueryElement.find('.image-zoom-slider').get(0);
        this.init();
    }
    Image.prototype.init = function () {
        var _this = this;
        this.mainImg.on("mousedown", function (e) {
            e.preventDefault();
            _this.allowDrag = 1;
            _this.initial.x = 0;
            _this.initial.y = 0;
        });
        this.mainImg.on("mouseup", function () {
            _this.allowDrag = 0;
            _this.initial.x = 0;
            _this.initial.y = 0;
            _this.lastTranslated.x = _this.translated.x;
            _this.lastTranslated.y = _this.translated.y;
            _this.translated.x = 0;
            _this.translated.y = 0;
        });
        this.mainImg.on("mousemove", function (e) {
            e.preventDefault();
            if (_this.allowDrag && _this.zoomed > 1) {
                if (_this.initial.x == 0 || _this.initial.y == 0) {
                    _this.initial.x = e.pageX;
                    _this.initial.y = e.pageY;
                }
                _this.cursorDistanceDiff.x = e.pageX - _this.initial.x;
                _this.cursorDistanceDiff.y = e.pageY - _this.initial.y;
                _this.translated.x = _this.lastTranslated.x + _this.cursorDistanceDiff.x;
                _this.translated.y = _this.lastTranslated.y + _this.cursorDistanceDiff.y;
                Image.renderImage(_this.mainImg, _this.translated, _this.zoomed, _this.rotated);
            }
        });
        //slider and zoomedvar zoomSlider = $('#image-zoom-slider').get(0);
        if (this.zoomSlider !== undefined) {
            // @ts-ignore
            noUiSlider.create(this.zoomSlider, {
                start: 0,
                connect: true,
                range: {
                    'min': 1,
                    'max': 3,
                }
            });
            this.zoomSlider.noUiSlider.on('update', function (e) {
                if (e[0] == 1 || e[0] == 1.00) {
                    //reset
                    _this.allowDrag = 0;
                    _this.cursorDistanceDiff = { x: 0, y: 0 };
                    _this.initial = { x: 0, y: 0 };
                    _this.lastTranslated = { x: 0, y: 0 };
                    _this.zoomed = 1;
                    _this.rotated = 0;
                    _this.translated = { x: 0, y: 0 };
                    Image.renderImage(_this.mainImg, _this.lastTranslated, _this.zoomed, _this.rotated);
                }
                else {
                    Image.renderImage(_this.mainImg, _this.lastTranslated, e[0], _this.rotated);
                    _this.zoomed = parseFloat(e[0]);
                }
            });
        }
        //dblclick zoom
        this.mainImg.on("dblclick", function () {
            _this.zoomed = _this.zoomed + 1;
            if (_this.zoomed > 3) {
                _this.zoomed = 3;
            }
            _this.zoomSlider.noUiSlider.set(_this.zoomed);
            Image.renderImage(_this.mainImg, _this.lastTranslated, _this.zoomed, _this.rotated);
        });
        //Swap & Save
        this.swapBtn.on('click', function () {
            loader_1.Loader.show();
            $.ajax({
                url: router_1.default.get(_this.routes.swap, { image_id: _this.mainImg.attr('data-image-id') }),
                type: 'PUT',
                dataType: 'json'
            }).done(function (data) {
                if (data.return == true) {
                    var id = _this.mainImg.attr('data-image-id');
                    if (id != undefined) {
                        _this.swap(data.front_image, data.back_image);
                        toastr.success(data.message, '');
                    }
                    else {
                        toastr.error(data.message, '');
                    }
                }
                else {
                    toastr.error(data.message, '');
                }
                loader_1.Loader.hide();
            });
        });
        //Flip only
        this.flipBtn.on('click', function () {
            _this.flip();
        });
        //Rotate & Save
        this.rotateBtn.on('click', function () {
            loader_1.Loader.show();
            $.ajax({
                url: router_1.default.get(_this.routes.rotate, { image_id: _this.mainImg.attr('data-image-id') }),
                type: 'PUT',
                dataType: 'json'
            }).done(function (data) {
                if (data.return == true) {
                    var id = _this.mainImg.attr('data-image-id');
                    if (id != undefined) {
                        _this.rotate(data.front_image, data.back_image);
                        toastr.success(data.message, '');
                    }
                    else {
                        toastr.error(data.message, '');
                    }
                }
                else {
                    toastr.error(data.message, '');
                }
                loader_1.Loader.hide();
            });
        });
        //css simple rotate
        this.simpleRotateBtn.on('click', function () {
            _this.rotated = _this.rotated + 180;
            Image.renderImage(_this.mainImg, _this.lastTranslated, _this.zoomed, _this.rotated);
        });
        //reset
        this.resetBtn.on('click', function () {
            _this.zoomSlider.noUiSlider.set(1);
            //reset
            _this.allowDrag = 0;
            _this.cursorDistanceDiff = { x: 0, y: 0 };
            _this.initial = { x: 0, y: 0 };
            _this.lastTranslated = { x: 0, y: 0 };
            _this.zoomed = 1;
            _this.rotated = 0;
            _this.translated = { x: 0, y: 0 };
            Image.renderImage(_this.mainImg, _this.lastTranslated, _this.zoomed, _this.rotated);
        });
    };
    Image.prototype.rotate = function (front_image, back_image) {
        if (this.mainImg.attr('data-orientation') === 'front') {
            this.mainImg.attr('src', front_image).attr('data-orientation', 'front');
        }
        else {
            this.mainImg.attr('src', back_image).attr('data-orientation', 'back');
        }
        this.mainImg.attr('data-front-image', front_image).attr('data-back-image', back_image);
    };
    Image.prototype.swap = function (front_image, back_image) {
        if (this.mainImg.attr('data-orientation') === 'front') {
            this.mainImg.attr('src', front_image).attr('data-orientation', 'front');
        }
        else {
            this.mainImg.attr('src', back_image).attr('data-orientation', 'back');
        }
        this.mainImg.attr('data-front-image', front_image).attr('data-back-image', back_image);
    };
    Image.prototype.flip = function () {
        var back_image = this.mainImg.attr('data-back-image');
        var front_image = this.mainImg.attr('data-front-image');
        if (this.mainImg.attr('data-orientation') === 'front') {
            this.mainImg.attr('src', back_image).attr('data-orientation', 'back');
        }
        else {
            this.mainImg.attr('src', front_image).attr('data-orientation', 'front');
        }
    };
    Image.renderImage = function (mainImg, translate, zoom, rotate) {
        mainImg.css({
            '-webkit-transform': 'translate(' + translate.x + 'px,' + translate.y + 'px) scale(' + zoom + ') rotate(' + rotate + 'deg)',
            '-moz-transform': 'translate(' + translate.x + 'px,' + translate.y + 'px) scale(' + zoom + ') rotate(' + rotate + 'deg)',
            '-ms-transform': 'translate(' + translate.x + 'px,' + translate.y + 'px) scale(' + zoom + ') rotate(' + rotate + 'deg)',
            '-o-transform': 'translate(' + translate.x + 'px,' + translate.y + 'px) scale(' + zoom + ') rotate(' + rotate + 'deg)',
            'transform': 'translate(' + translate.x + 'px,' + translate.y + 'px) scale(' + zoom + ') rotate(' + rotate + 'deg)'
        });
    };
    return Image;
}());
exports.Image = Image;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Keys = /** @class */ (function () {
    function Keys() {
        this.current = 0;
        this.length = 0;
    }
    Keys.prototype.init = function (element) {
        if (this.table !== $(element).find('.table')) {
            this.off();
            this.table = $(element).find('.table');
            this.rows = this.table.find('tbody tr');
            this.length = this.rows.length - 1;
            this.on();
            return this;
        }
        else {
            return this;
        }
    };
    Keys.prototype.currentIndex = function (value) {
        this.current = value;
    };
    Keys.prototype.off = function () {
        $(document).off('keydown');
    };
    Keys.prototype.on = function () {
        var _this = this;
        $(document).on('keydown', function (e) {
            var tabIndex = Number(_this.table.find('tr[tabindex=' + _this.current + ']').attr('tabindex'));
            var height = Number(_this.rows.height());
            if (tabIndex === undefined) {
                throw new Error('Tabindex is undefined.');
            }
            if (e.keyCode === 40) { //down
                e.preventDefault();
                _this.current++;
                if (_this.current > _this.length) {
                    _this.current = 0;
                }
                _this.table.find('tr[tabindex=' + _this.current + '] td:first').trigger("click").scrollTop(tabIndex * height);
            }
            if (e.keyCode === 38) { //up
                e.preventDefault();
                _this.current--;
                if (_this.current < 0) {
                    _this.current = _this.length;
                }
                _this.table.find('tr[tabindex=' + _this.current + '] td:first').trigger("click").scrollTop(tabIndex * height);
            }
        });
    };
    return Keys;
}());
module.exports = new Keys();


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var data_entry_1 = __webpack_require__(12);
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var ClearingBatchDataEntry = /** @class */ (function (_super) {
    __extends(ClearingBatchDataEntry, _super);
    function ClearingBatchDataEntry(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.route = route;
        return _this;
    }
    ClearingBatchDataEntry.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(String(cheque.batch_no));
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#amount').off().val(cheque.amount).priceFormat(__assign({}, this.priceFormat, { 'clearOnEmpty': 'true' }));
    };
    ClearingBatchDataEntry.prototype.updateBatchData = function (batch_data) {
        var div = document.querySelector('#reconciliation-batch-documents-general-info-box');
        div.querySelector('#batch-data-batch-documents').innerHTML = String(batch_data.batch_documents);
        div.querySelector('#batch-data-valid-documents').innerHTML = String(batch_data.valid_documents);
        div.querySelector('#batch-data-rejected-documents').innerHTML = String(batch_data.rejected_documents);
        div.querySelector('#batch-data-deleted-documents').innerHTML = String(batch_data.deleted_documents);
        div.querySelector('#batch-data-documents-difference').innerHTML = String(batch_data.documents_difference);
        div.querySelector('#batch-data-batch-total-amount').innerHTML = String(batch_data.batch_total_amount);
        div.querySelector('#batch-data-cheques-amount').innerHTML = String(batch_data.cheques_amount);
        div.querySelector('#batch-data-amount-difference').innerHTML = String(batch_data.amount_difference);
        if (batch_data.documents_difference == 0) {
            div.querySelector('#batch-data-documents-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-documents-difference').classList.add('bg-red-a-0-5');
        }
        if (batch_data.amount_difference_without_formatting == 0) {
            div.querySelector('#batch-data-amount-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-amount-difference').classList.add('bg-red-a-0-5');
        }
    };
    ClearingBatchDataEntry.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='amount']").off().html(cheque.amount).priceFormat(this.priceFormat);
        row.find("td[data-name='check_digit_2']").html(String(cheque.check_digit_2));
    };
    /**
     * Save item.
     *
     * @param index
     * @returns {boolean}
     */
    ClearingBatchDataEntry.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            amount: this.element.find('#amount').unmask().trim(),
        };
        cheque.amount = this.element.find('#amount').unmask().trim();
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidAmount(data.amount)) {
            toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.amount = data.amount;
                cheque.check_digit_2 = response.item.check_digit_2;
                _this.calculateBatchAmounts();
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return ClearingBatchDataEntry;
}(data_entry_1.DataEntry));
exports.ClearingBatchDataEntry = ClearingBatchDataEntry;
var ClearingCustomQueryDataEntry = /** @class */ (function (_super) {
    __extends(ClearingCustomQueryDataEntry, _super);
    function ClearingCustomQueryDataEntry(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        return _this;
    }
    /**
     * Save item.
     *
     * @param index
     * @returns {boolean}
     */
    ClearingCustomQueryDataEntry.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            amount: this.element.find('#amount').unmask().trim(),
        };
        cheque.amount = this.element.find('#amount').unmask().trim();
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidAmount(data.amount)) {
            toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.amount = data.amount;
                cheque.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return ClearingCustomQueryDataEntry;
}(ClearingBatchDataEntry));
exports.ClearingCustomQueryDataEntry = ClearingCustomQueryDataEntry;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var DataEntry = /** @class */ (function () {
    function DataEntry(element, options) {
        this.priceFormat = {};
        this.keys_enabled = true;
        this.alertForEndOfDocument = true;
        this.nextInputToFocus = 0;
        this.currentItemIndex = 0;
        this.id = 0;
        this.images = [];
        this.element = $(element);
        this.cheques = options.items;
        this.batch = options.batch;
        this.image_manipulation_main = this.element.find('.image-manipulation-main');
        this.priceFormat = __assign({}, this.priceFormat, options.priceFormat);
        this.init(this.element);
    }
    DataEntry.prototype.init = function (element) {
        var _this = this;
        this.cheques.forEach(function (cheque, index) {
            if (cheque.id === _this.id) {
                _this.currentItemIndex = index;
                return false;
            }
        });
        this.id = $('div.documents-list').find('tr.active').data('item-id');
        this.focusableInputs = element.find('.user-input');
        this.cheques.forEach(function (cheque) {
            _this.images[cheque.id] = cheque.image;
        });
        this.setEventHandlers();
    };
    DataEntry.prototype.setEventHandlers = function () {
        var _this = this;
        this.element.off();
        this.element.on('show.bs.modal', function () {
            _this.element.draggable({
                handle: ".modal-content"
            });
            _this.displayImage();
            _this.setBatchAmounts();
            _this.enableKeys();
        });
        this.element.on('shown.bs.modal', function () {
            _this.displayItem();
        });
        this.element.on('hidden.bs.modal', function () {
            $(document).off('keydown');
            $(document).off('keyup');
            _this.element.draggable("destroy");
        });
        this.element.on('focus', '.user-input', function (e) {
            _this.nextInputToFocus = +String($(e.currentTarget).attr('tabindex'));
        });
        this.element.find('.first-item').on("click", function () {
            _this.firstItem();
            _this.displayImage();
        });
        this.element.find('.last-item').on("click", function () {
            _this.lastItem();
            _this.displayImage();
        });
        this.element.find('.prev-item').on("click", function () {
            _this.prevItem();
            _this.displayImage();
        });
        this.element.find('.next-item').on("click", function () {
            _this.nextItem();
            _this.displayImage();
        });
    };
    DataEntry.prototype.enableKeys = function () {
        var _this = this;
        this.keys_enabled = true;
        $(document).on('keydown', function (e) {
            if (_this.element.hasClass('show')) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    if (_this.element.find('#doc_no input').is(":focus")) {
                        _this.nextInputToFocus = 0;
                    }
                    else {
                        _this.nextInputToFocus = _this.nextInputToFocus + 1;
                        _this.focusInputByIndex();
                    }
                }
                if (keyCode === 9) {
                    e.preventDefault();
                    _this.nextInputToFocus = _this.nextInputToFocus + 1;
                    if (_this.nextInputToFocus > (_this.focusableInputs.length - 1)) {
                        _this.nextInputToFocus = 0;
                    }
                }
                if (keyCode === 38) {
                    e.preventDefault();
                    _this.nextInputToFocus = 0;
                    _this.nextItem();
                }
                if (keyCode === 40) {
                    e.preventDefault();
                    _this.nextInputToFocus = 0;
                    _this.prevItem();
                }
            }
        }).on('keyup', function (e) {
            if (_this.element.hasClass('show')) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    if (_this.element.find('#doc_no input').is(":focus")) {
                        _this.changeDocNo();
                        return false;
                    }
                    else {
                        if (_this.nextInputToFocus > (_this.focusableInputs.length - 1)) {
                            _this.nextInputToFocus = 0;
                            _this.nextItem(true);
                            _this.displayImage();
                        }
                    }
                }
                if (keyCode === 38) {
                    e.preventDefault();
                    _this.displayImage();
                }
                if (keyCode === 40) {
                    e.preventDefault();
                    _this.displayImage();
                }
            }
        });
    };
    DataEntry.prototype.calculateBatchAmounts = function () {
        var cheques_amount = 0;
        this.cheques.forEach(function (cheque) {
            cheques_amount += +cheque.amount;
        });
        this.element.find('#cheques_amount').off().val(cheques_amount).priceFormat(this.priceFormat);
        this.element.find('#amount_diff').off().val(+this.batch.total_amount - cheques_amount).priceFormat(__assign({}, this.priceFormat, { 'allowNegative': 'true' }));
        if ((+this.batch.total_amount - cheques_amount) !== 0) {
            this.element.find('#total_amount, #cheques_amount, #amount_diff').removeClass('color-green').addClass('color-red');
        }
        else {
            this.element.find('#total_amount, #cheques_amount, #amount_diff').removeClass('color-red').addClass('color-green');
        }
    };
    DataEntry.prototype.setBatchAmounts = function () {
        this.element.find('#total_amount').off().val(+this.batch.total_amount).priceFormat(this.priceFormat);
        this.calculateBatchAmounts();
    };
    DataEntry.prototype.changeDocNo = function () {
        var doc_no_input = this.element.find('#doc_no input');
        var doc_no = +String(doc_no_input.val());
        if (!isNaN(doc_no) && doc_no > 0) {
            if (doc_no <= this.cheques.length) {
                this.currentItemIndex = +String(doc_no_input.val()) - 1;
                this.displayItem();
                this.displayImage();
            }
            else {
                toastr.error('The maximum number of documents is ' + this.cheques.length + '.');
            }
        }
    };
    /**
     * Set Progress Bar
     */
    DataEntry.prototype.setProgressBar = function () {
        var current = this.currentItemIndex + 1;
        var percentage = (100 * (this.currentItemIndex + 1)) / this.cheques.length;
        this.element.find('.current').html(String(current));
        this.element.find('.total').html(String(this.cheques.length));
        this.element.find('.progress-bar').width(percentage + '%');
        if (percentage > 70) {
            this.element.find('.progress-bar-w-centered-numbers span:first').addClass('color-white');
        }
        else {
            this.element.find('.progress-bar-w-centered-numbers span:first').removeClass('color-white');
        }
    };
    /**
     * Focus by index.
     */
    DataEntry.prototype.focusInputByIndex = function () {
        $(this.focusableInputs[this.nextInputToFocus]).trigger("focus").trigger("select");
    };
    /**
     * Go to the first item.
     */
    DataEntry.prototype.firstItem = function () {
        this.currentItemIndex = 0;
        this.displayItem();
    };
    /**
     * Got to the last item.
     */
    DataEntry.prototype.lastItem = function () {
        this.currentItemIndex = this.cheques.length - 1;
        this.displayItem();
    };
    /**
     * Got to previous item.
     */
    DataEntry.prototype.prevItem = function () {
        var previousItemIndex = this.currentItemIndex - 1;
        if (previousItemIndex >= 0) {
            this.currentItemIndex = previousItemIndex;
            this.displayItem();
        }
        else {
            previousItemIndex = this.cheques.length - 1;
            this.currentItemIndex = previousItemIndex;
            this.displayItem();
        }
    };
    /**
     * Go to next item.
     *
     * @param save
     * @returns {boolean}
     */
    DataEntry.prototype.nextItem = function (save) {
        save = save || false;
        if (save === true) {
            if (this.currentItemIndex <= this.cheques.length - 1) {
                if (!this.saveItem(this.currentItemIndex)) {
                    return false;
                }
            }
        }
        if (this.currentItemIndex === this.cheques.length - 1) {
            if (this.alertForEndOfDocument) {
                // @ts-ignore
                swal({
                    title: 'End of document',
                    timer: 1500,
                    type: 'info'
                });
                this.alertForEndOfDocument = false;
            }
            else {
                this.alertForEndOfDocument = true;
                var nextItemIndex = 0;
                if (nextItemIndex <= this.cheques.length - 1) {
                    this.currentItemIndex = nextItemIndex;
                    this.displayItem();
                }
            }
        }
        else {
            var nextItemIndex = this.currentItemIndex + 1;
            if (nextItemIndex <= this.cheques.length - 1) {
                this.currentItemIndex = nextItemIndex;
                this.displayItem();
            }
        }
    };
    DataEntry.prototype.displayItem = function () {
        this.setItem();
        this.setProgressBar();
        this.focusInputByIndex();
    };
    DataEntry.prototype.displayImage = function () {
        var cheque = this.cheques[this.currentItemIndex];
        if (cheque.image !== null) {
            this.image_manipulation_main.attr('data-image-id', this.images[cheque.id].id).attr('data-orientation', 'front');
            if (this.images[cheque.id].front_filename !== null) {
                this.image_manipulation_main.attr('src', this.images[cheque.id].front_filename).attr('data-front-image', this.images[cheque.id].front_filename);
            }
            if (this.images[cheque.id].back_filename !== null) {
                this.image_manipulation_main.attr('data-back-image', this.images[cheque.id].back_filename);
            }
        }
    };
    return DataEntry;
}());
exports.DataEntry = DataEntry;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var data_entry_1 = __webpack_require__(12);
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var TradeFinanceBatchDataEntry = /** @class */ (function (_super) {
    __extends(TradeFinanceBatchDataEntry, _super);
    /**
     * TradeFinanceBatchDataEntry Constructor
     *
     * @param element
     * @param options
     * @param route
     */
    function TradeFinanceBatchDataEntry(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.route = route;
        return _this;
    }
    /**
     * Set the item input fields.
     */
    TradeFinanceBatchDataEntry.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(String(cheque.batch_no));
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#deposit-date').val(cheque.deposit_date).inputmask('dd/mm/yyyy', {
            clearMaskOnLostFocus: true,
            positionCaretOnClick: "none",
            placeholder: "dd/mm/" + new Date().getFullYear()
        });
        this.element.find('#amount').off().val(cheque.amount).priceFormat(__assign({}, this.priceFormat, { 'clearOnEmpty': 'true' }));
    };
    /**
     * Update the batch data of the general info box.
     *
     * @param {BatchData} batch_data
     */
    TradeFinanceBatchDataEntry.prototype.updateBatchData = function (batch_data) {
        var div = document.querySelector('#reconciliation-batch-documents-general-info-box');
        div.querySelector('#batch-data-batch-documents').innerHTML = String(batch_data.batch_documents);
        div.querySelector('#batch-data-valid-documents').innerHTML = String(batch_data.valid_documents);
        div.querySelector('#batch-data-rejected-documents').innerHTML = String(batch_data.rejected_documents);
        div.querySelector('#batch-data-deleted-documents').innerHTML = String(batch_data.deleted_documents);
        div.querySelector('#batch-data-documents-difference').innerHTML = String(batch_data.documents_difference);
        div.querySelector('#batch-data-batch-total-amount').innerHTML = String(batch_data.batch_total_amount);
        div.querySelector('#batch-data-cheques-amount').innerHTML = String(batch_data.cheques_amount);
        div.querySelector('#batch-data-amount-difference').innerHTML = String(batch_data.amount_difference);
        if (batch_data.documents_difference == 0) {
            div.querySelector('#batch-data-documents-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-documents-difference').classList.add('bg-red-a-0-5');
        }
        if (batch_data.amount_difference_without_formatting == 0) {
            div.querySelector('#batch-data-amount-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-amount-difference').classList.add('bg-red-a-0-5');
        }
    };
    /**
     * Update the row cell.
     *
     * @param {Cheque} cheque
     */
    TradeFinanceBatchDataEntry.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='amount']").off().html(cheque.amount).priceFormat(this.priceFormat);
        row.find("td[data-name='check_digit_2']").html(String(cheque.check_digit_2));
        row.find("td[data-name='deposit_date']").html(String(cheque.deposit_date));
    };
    /**
     * Save item.
     *
     * @param index
     * @returns {boolean}
     */
    TradeFinanceBatchDataEntry.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            amount: this.element.find('#amount').unmask().trim(),
            deposit_date: this.element.find('#deposit-date').val(),
        };
        cheque.amount = this.element.find('#amount').unmask().trim();
        cheque.deposit_date = this.element.find('#deposit-date').val();
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidAmount(data.amount)) {
            toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        if (!validator.canDeposit(data.deposit_date)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_deposit_date'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.amount = data.amount;
                cheque.check_digit_2 = response.item.check_digit_2;
                cheque.deposit_date = data.deposit_date;
                _this.calculateBatchAmounts();
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return TradeFinanceBatchDataEntry;
}(data_entry_1.DataEntry));
exports.TradeFinanceBatchDataEntry = TradeFinanceBatchDataEntry;
var TradeFinanceCustomQueryDataEntry = /** @class */ (function (_super) {
    __extends(TradeFinanceCustomQueryDataEntry, _super);
    function TradeFinanceCustomQueryDataEntry(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        return _this;
    }
    /**
     * Save item.
     *
     * @param index
     * @returns {boolean}
     */
    TradeFinanceCustomQueryDataEntry.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            amount: this.element.find('#amount').unmask().trim(),
            deposit_date: this.element.find('#deposit-date').val(),
        };
        cheque.amount = this.element.find('#amount').unmask().trim();
        cheque.deposit_date = this.element.find('#deposit-date').val();
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidAmount(data.amount)) {
            toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        if (!validator.canDeposit(data.deposit_date)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_deposit_date'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.amount = data.amount;
                cheque.check_digit_2 = response.item.check_digit_2;
                cheque.deposit_date = data.deposit_date;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return TradeFinanceCustomQueryDataEntry;
}(TradeFinanceBatchDataEntry));
exports.TradeFinanceCustomQueryDataEntry = TradeFinanceCustomQueryDataEntry;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = __webpack_require__(6);
var cookie_1 = __webpack_require__(15);
var progress_dialog_1 = __webpack_require__(16);
var ProgressBar = /** @class */ (function (_super) {
    __extends(ProgressBar, _super);
    function ProgressBar(config) {
        var _this = _super.call(this, config) || this;
        _this.itemsReady = false;
        _this.items = [];
        _this.id = _this.getConfigParam('id');
        _this.elem = _this.getConfigParam('renderTo');
        _this.contentAreaId = _this.id + '-content-area';
        _this.container = $('#' + _this.id);
        _this.isAsyncProgressBarCollapsed = cookie_1.Cookie.get('isAsyncProgressBarCollapsed');
        _this.pe = null;
        if (_this.isAsyncProgressBarCollapsed) {
            _this.container.children(":first").addClass('async-progress-bar-collapsed');
        }
        else {
            _this.container.children(":first").removeClass('async-progress-bar-collapsed');
        }
        _this.addEvents();
        _this.update();
        return _this;
    }
    ProgressBar.prototype.update = function () {
        var _this = this;
        $.ajax({
            url: this.getConfigParam('progressUrl'),
            dataType: 'json',
            success: function (data) {
                if (data == undefined) {
                    return;
                }
                _this.setItems(data.items);
                _this.updateProgressDialog();
                _this.itemsReady = true;
                _this.renderItems();
            }
        });
    };
    ProgressBar.prototype.getItems = function () {
        return this.items;
    };
    ProgressBar.prototype.getItem = function (id) {
        for (var _i = 0, _a = this.getItems(); _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.getId() === id) {
                return item;
            }
        }
        return false;
    };
    ProgressBar.prototype.checkPreviousStatus = function (newItems) {
        var _this = this;
        newItems.forEach(function (newItem) {
            var item = _this.getItem(newItem.id);
            if (item) {
                if (newItem.status !== item.status) {
                    ProgressBar.onItemStatusChange(newItem);
                }
            }
            else {
            }
        });
    };
    ProgressBar.onItemStatusChange = function (newItem) {
        var isComplete = function (status) {
            return ['finished', 'failed'].indexOf(status) !== -1;
        };
        if (isComplete(newItem.status)) {
        }
    };
    ProgressBar.prototype.initItems = function (items) {
        this.items = items.filter(function (item) {
            return !!item;
        }).map(function (item) {
            return new ProgressBar.Item(item);
        });
    };
    ProgressBar.prototype.setItems = function (items) {
        this.initItems(items);
    };
    ProgressBar.prototype.removeCompletedTask = function () {
        var _this = this;
        var ids = this.items.filter(function (item) {
            return !(item.isQueued() || item.isExecuting());
        }).map(function (item) {
            return item.getId();
        });
        if (ids.length > 0) {
            $.ajax({
                url: this.getConfigParam('removeUrl'),
                dataType: 'json',
                method: 'post',
                data: { 'ids[]': ids },
                success: function (data) {
                    _this.removeItemsByIds(ids);
                }
            });
        }
    };
    ProgressBar.prototype.createProgressDialog = function (item) {
        var _this = this;
        var progressBarItem = item instanceof ProgressBar.Item ? item : component_1.Component.createComponent(item);
        this.progressDialog = new progress_dialog_1.ProgressDialog(Object.create({
            id: "task-progress-modal",
            progressBarItem: progressBarItem,
            locale: this.translator,
            onHide: $('#task-progress-modal').on('hidden.bs.modal', function () {
                delete _this.progressDialog;
                _this.show();
            }),
            onComplete: function () {
                if (_this.progressDialog) {
                    var progressBarItem_1 = _this.progressDialog.progressBarItem;
                    var redirect = progressBarItem_1.getConfigParam('redirect');
                    if (redirect.url) {
                        $(location).attr("href", redirect.url);
                    }
                }
            }
        }));
        this.update();
        this.hide();
    };
    ProgressBar.prototype.updateProgressDialog = function () {
        if (this.progressDialog) {
            var itemId = this.progressDialog.progressBarItem.getId();
            var item = this.getItem(itemId);
            if (item instanceof ProgressBar.Item) {
                this.progressDialog.updateItem(item);
                $('#' + this.progressDialog.dialogElement).modal({
                    keyboard: false
                });
            }
        }
    };
    ProgressBar.prototype.renderItems = function () {
        var _this = this;
        this.updateTitle();
        if (this.items.length) {
            this.items.forEach(function (item) {
                _this.renderItem(item);
            });
            this.container.removeClass('d-none');
            this.setIntervalExecutor();
        }
        else {
            this.container.addClass('d-none');
        }
    };
    ProgressBar.prototype.setIntervalExecutor = function () {
        var _this = this;
        if (null === this.pe && this.hasInProgressTasks()) {
            this.pe = setInterval(function () {
                _this.update();
                if (!_this.hasInProgressTasks()) {
                    clearInterval(_this.pe);
                    _this.pe = null;
                }
            }, 2000);
        }
    };
    ProgressBar.prototype.hasInProgressTasks = function () {
        return this.items.some(function (item) {
            return item.isExecuting() || item.isQueued();
        });
    };
    ProgressBar.prototype.renderItem = function (item) {
        var renderTargetId = this.id + '-item-' + item.getId();
        if (!$('#' + renderTargetId).length) {
            $('#' + this.contentAreaId).prepend('<li id="' + renderTargetId + '" class="async-progress-bar-item"></li>');
        }
        item.setProgressBarElement(this);
        item.setRenderTarget($('#' + renderTargetId));
        item.render();
    };
    ProgressBar.prototype.removeItemsByIds = function (ids) {
        if (!ids.length) {
            return;
        }
        var items = this.getItems();
        for (var i = 0; i < ids.length; i++) {
            for (var j = 0; j < items.length; j++) {
                if (items[j].getId() === ids[i]) {
                    items[j].getRenderTarget().remove();
                    items.splice(j, 1);
                    break;
                }
            }
        }
        this.setItems(items);
        this.renderItems();
    };
    ProgressBar.prototype.toggle = function () {
        var element = $('#asyncProgressBar');
        element.toggleClass('async-progress-bar-collapsed');
        cookie_1.Cookie.setPermanent('isAsyncProgressBarCollapsed', String(element.hasClass('async-progress-bar-collapsed')), '/');
        this.updateTitle();
    };
    ProgressBar.prototype.updateTitle = function () {
        var countRunning = 0;
        var countCompleteError = 0;
        var countCompleteSuccessfully = 0;
        var countCompleteWarning = 0;
        this.items.forEach(function (item) {
            if (item.isFinishedSuccessfully()) {
                countCompleteSuccessfully++;
            }
            else if (item.isFinishedWithWarning()) {
                countCompleteWarning++;
            }
            else if (item.isFailed()) {
                countCompleteError++;
            }
            else {
                countRunning++;
            }
        });
        var taskRunningElement = $('#asyncProgressBarTitleTasks');
        if (countRunning > 0) {
            if (countRunning === this.items.length) {
                taskRunningElement.html(this.translator.get('taskInProgress', { count: countRunning }));
            }
            else {
                taskRunningElement.html(String(countRunning));
            }
            $('#asyncProgressBar').removeClass('async-progress-bar-complete');
            taskRunningElement.removeClass('d-none');
        }
        else {
            $('#asyncProgressBar').addClass('async-progress-bar-complete');
            taskRunningElement.addClass('d-none');
        }
        if (countCompleteSuccessfully > 0 || countCompleteWarning > 0 || countCompleteError > 0) {
            $('#asyncProgressBarHideCompletedTasks').removeClass('d-none');
        }
        else {
            $('#asyncProgressBarHideCompletedTasks').addClass('d-none');
        }
        this.updateTaskTitleElement('#asyncProgressBarTitleTasksError', countCompleteError);
        this.updateTaskTitleElement('#asyncProgressBarTitleTasksWarning', countCompleteWarning);
        this.updateTaskTitleElement('#asyncProgressBarTitleTasksComplete', $('#asyncProgressBar').hasClass('async-progress-bar-collapsed') && countCompleteSuccessfully === this.items.length ? this.translator.get('allTasksCompleted', { num: countCompleteSuccessfully }) : countCompleteSuccessfully);
    };
    ProgressBar.prototype.updateTaskTitleElement = function (id, title) {
        if (!this.itemsReady) {
            return;
        }
        var element = $(id);
        element.html(String(title));
        element.toggleClass('d-none', title === 0);
    };
    ProgressBar.prototype.addEvents = function () {
        var _this = this;
        $('#asyncProgressBarTop').on('click', function (e) {
            e.preventDefault();
            _this.toggle();
        });
        $('#asyncProgressBarHideCompletedTasks').on('click', function (e) {
            e.preventDefault();
            _this.removeCompletedTask();
        });
    };
    return ProgressBar;
}(component_1.Component));
exports.ProgressBar = ProgressBar;
(function (ProgressBar) {
    var Item = /** @class */ (function (_super) {
        __extends(Item, _super);
        function Item(config) {
            var _this = _super.call(this, config) || this;
            _this.STATUS_QUEUED = 'queued';
            _this.STATUS_EXECUTING = 'executing';
            _this.STATUS_FINISHED = 'finished';
            _this.STATUS_FAILED = 'failed';
            _this.status = '';
            _this.title = '';
            _this.progressText = {};
            _this.currentProgress = 0;
            _this.totalProgress = 0;
            _this.percentageProgress = 0;
            _this.canAccessJobs = false;
            _this.id = _this.getConfigParam('id');
            _this.status = _this.getConfigParam('status');
            _this.totalProgress = _this.getConfigParam('total_progress');
            _this.currentProgress = _this.getConfigParam('current_progress');
            _this.percentageProgress = _this.getConfigParam('progress_percentage');
            _this.redirect = _this.getConfigParam('redirect', false);
            _this.title = _this.getConfigParam('title');
            _this.progressText = _this.getConfigParam('progressText');
            _this.jobs = _this.getConfigParam('jobs', []);
            _this.errors = _this.getConfigParam('errors', []);
            _this.canAccessJobs = _this.getConfigParam('canAccessJobs', false);
            return _this;
        }
        Item.prototype.setRenderTarget = function (target) {
            this.renderTarget = target;
        };
        Item.prototype.getRenderTarget = function () {
            return this.renderTarget;
        };
        Item.prototype.render = function () {
            var progressBar = '';
            var element = this.getRenderTarget();
            if (this.isFinishedSuccessfully()) {
                element.addClass('async-progress-bar-item-complete');
                progressBar =
                    '<div class="async-progress-bar-item-wrap">' +
                        '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent(element) +
                        '<div class="async-progress-bar-item-msg-body">' + this.getProgressText();
                if (this.redirect !== false) {
                    progressBar += '<div><a href="' + this.redirect.url + '">' + this.redirect.title + '</a></div>';
                }
                progressBar +=
                    '</div>' +
                        '</div>' +
                        '</div>';
            }
            else if (this.isFinishedWithWarning()) {
                element.addClass('async-progress-bar-item-warning');
                progressBar =
                    '<div class="async-progress-bar-item-wrap">' +
                        '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent(element) +
                        '<div class="async-progress-bar-item-msg-body">' + this.getProgressText() +
                        '<div>' + this.getErrorsString() + '</div>';
                if (this.redirect !== false) {
                    progressBar += '<div><a href="' + this.redirect.url + '">' + this.redirect.title + '</a></div>';
                }
                progressBar += '</div>' +
                    '</div>' +
                    '</div>';
            }
            else if (this.isFailed()) {
                element.addClass('async-progress-bar-item-error');
                progressBar =
                    '<div class="async-progress-bar-item-wrap">' +
                        '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent(element) +
                        '<div class="async-progress-bar-item-msg-body">' + this.getProgressText() +
                        '<div>' + this.getFailedText() + '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
            }
            else {
                progressBar =
                    '<div class="async-progress-bar-item-wrap">' +
                        '<div class="async-progress-bar-item-heading">' +
                        '<div class="async-progress-bar-item-title">' + this.getProgressText() + '</div>' +
                        '<div class="progress progress-sm">' +
                        '<div style="width:' + this.getPercentageProgress() + '%" class="progress-bar progress-bar-striped active"></div>' +
                        '</div>' +
                        '<div class="async-progress-bar-item-footer">';
                if (this.getTotalProgress() != 0) {
                    progressBar += '<div class="async-progress-bar-item-control float-right text-right">' + this.getCurrentProgress() + '/' + this.getTotalProgress() + '</div>';
                }
                progressBar += '<div class="async-progress-bar-item-control float-left text-left">' + this.getProgressDialogLink() + '</div>';
                progressBar += '</div>' +
                    '</div>' +
                    '</div>';
            }
            element.html(progressBar);
        };
        Item.prototype.closeButtonEvent = function (element) {
            var _this = this;
            element.off('click', '#item-' + this.id);
            element.on('click', '#item-' + this.id, function (e) {
                e.preventDefault();
                _this.remove();
            });
            return '<a id="item-' + this.id + '" href="#"><span class="close"></span></a>';
        };
        Item.prototype.getTitle = function () {
            return this.title;
        };
        Item.prototype.getFailedText = function () {
            if (this.canAccessJobs) {
                return '<a href="' + this.jobs.url + '">' + this.jobs.title + '</a>';
            }
            else {
                return this.translator.get('administrator');
            }
        };
        Item.prototype.getProgressText = function () {
            return this.progressText[this.status];
        };
        Item.prototype.setProgressBarElement = function (progressBarElement) {
            this.progressBarElement = progressBarElement;
        };
        Item.prototype.getProgressBarElement = function () {
            return this.progressBarElement;
        };
        Item.prototype.getProgressDialogLink = function () {
            var _this = this;
            $(document).off('click', '#progressDialog-' + this.id);
            $(document).on('click', '#progressDialog-' + this.id, function (e) {
                e.preventDefault();
                _this.progressBarElement.createProgressDialog(_this);
            });
            return '<a href="#" id="progressDialog-' + this.id + '">' + this.translator.get('progressDialogLink') + '</a>';
        };
        Item.prototype.isQueued = function () {
            return this.STATUS_QUEUED === this.status;
        };
        Item.prototype.isFinished = function () {
            return this.STATUS_FINISHED === this.status;
        };
        Item.prototype.isFinishedSuccessfully = function () {
            return this.STATUS_FINISHED === this.status && !this.hasErrors();
        };
        Item.prototype.isFinishedWithWarning = function () {
            return this.STATUS_FINISHED === this.status && this.hasErrors();
        };
        Item.prototype.isExecuting = function () {
            return this.STATUS_EXECUTING === this.status;
        };
        Item.prototype.isFailed = function () {
            return this.STATUS_FAILED === this.status;
        };
        Item.prototype.getCurrentProgress = function () {
            return this.currentProgress;
        };
        Item.prototype.getTotalProgress = function () {
            return this.totalProgress;
        };
        Item.prototype.getPercentageProgress = function () {
            return this.percentageProgress;
        };
        Item.prototype.getId = function () {
            return this.id;
        };
        Item.prototype.hasErrors = function () {
            return this.errors.length > 0;
        };
        Item.prototype.getErrors = function () {
            return this.errors;
        };
        Item.prototype.getErrorsString = function () {
            var string = '<div class="error-details"><ul>';
            string += this.getErrors().map(function (error) {
                return '<li>' + String(error) + '</li>';
            });
            string += '</ul></div>';
            return string;
        };
        Item.prototype.remove = function () {
            var _this = this;
            $.ajax({
                url: this.getProgressBarElement().getConfigParam('removeUrl'),
                dataType: 'json',
                method: 'post',
                data: { 'ids[]': [this.getId()] },
                success: function (data) {
                    if (data.return === true) {
                        _this.getProgressBarElement().removeItemsByIds([_this.getId()]);
                    }
                }
            });
        };
        return Item;
    }(component_1.Component));
    ProgressBar.Item = Item;
})(ProgressBar = exports.ProgressBar || (exports.ProgressBar = {}));
exports.ProgressBar = ProgressBar;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Cookie = /** @class */ (function () {
    function Cookie() {
    }
    Cookie.get = function (name) {
        var cookies = document.cookie.split('; ');
        for (var i = 0; i < cookies.length; i++) {
            var crumbs = cookies[i].split('=');
            if (name === crumbs[0]) {
                return String(unescape(crumbs[1])) == 'true';
            }
        }
        return null;
    };
    Cookie.set = function (name, value, expires, path) {
        var cookie = name + '=' + escape(value) + '; ';
        if ('undefined' !== typeof expires && expires) {
            cookie += 'expires=' + expires + '; ';
        }
        if ('undefined' !== typeof path && path) {
            cookie += 'path=' + path + '; ';
        }
        if (window.location.protocol === 'https:') {
            cookie += 'secure; ';
        }
        document.cookie = cookie;
    };
    Cookie.setPermanent = function (name, value, path) {
        var date = new Date();
        date.setTime(date.getTime() + 365 * 24 * 60 * 60 * 1000);
        var expires = date.toUTCString();
        Cookie.set(name, value, expires, path);
    };
    Cookie.remove = function (name, path) {
        Cookie.set(name, '', 'Fri, 31 Dec 1999 23:59:59 GMT', path);
    };
    return Cookie;
}());
exports.Cookie = Cookie;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var component_1 = __webpack_require__(6);
var ProgressDialog = /** @class */ (function (_super) {
    __extends(ProgressDialog, _super);
    function ProgressDialog(config) {
        var _this = _super.call(this, config) || this;
        _this.id = _this.getConfigParam('id');
        _this.progressBarItem = _this.getConfigParam('progressBarItem');
        _this.dialogElement = _this.id;
        return _this;
    }
    ProgressDialog.prototype.updateItem = function (progressBarItem) {
        var isFinished = this.progressBarItem.isFinished();
        this.progressBarItem = progressBarItem;
        if (!isFinished && this.progressBarItem.isFinished()) {
            setTimeout(this.getConfigParam('onComplete'), 2000);
        }
        $('#' + this.id + ' .modal-header').html(this.renderTitle(this.progressBarItem));
        $('#' + this.id + ' .modal-body').html(this.renderProgress(this.progressBarItem));
        if (!$('#' + this.id).is(':visible')) {
            $('#' + this.id).modal('show');
        }
    };
    ProgressDialog.prototype.renderTitle = function (item) {
        return '<h5 class="mt-0 modal-title color-white">' + item.getTitle() + '</h5>' +
            '<a class="s-btn color-white" type="button" data-dismiss="modal" aria-label="Close"><i class="fa icon-background-tasks color-blue"> </i>' + this.translator.get('minimize') + '</a>';
    };
    ProgressDialog.prototype.renderProgress = function (item) {
        return '<div class="progress-step ' + this.getStepClass() + '">' +
            '<div class="progress-step-icon">' +
            /*
            '<i class="icon-md">' +
                '<img src="" />' +
            '</i>' +
            */
            '</div>' +
            '<div class="progress-step-body">' +
            this.renderStatus() +
            '<div class="progress-step-name"><h6>' + item.getProgressText() + '</h6></div>' +
            '<div class="progress-step-content">' + this.renderContent(item) + '</div>' +
            '</div>' +
            '</div>';
    };
    ProgressDialog.prototype.getStepClass = function () {
        if (this.progressBarItem.isFinished()) {
            return 'progress-step-success';
        }
        if (this.progressBarItem.isFailed()) {
            return 'progress-step-danger';
        }
        return null;
    };
    ProgressDialog.prototype.renderStatus = function () {
        if (this.progressBarItem.isFinished()) {
            return '<div class="progress-step-status"><i class="fa fa-check-circle fa-lg color-green"></i> ' + this.translator.get('statusDone') + '</div>';
        }
        if (this.progressBarItem.isFailed()) {
            return '<div class="progress-step-status"><i class="fa fa-warning fa-lg color-orange"></i> ' + this.translator.get('statusError') + '</div>';
        }
        if (this.progressBarItem.isQueued()) {
            return '<div class="progress-step-status text-muted"><i class="fa fa-clock-o fa-lg"></i> ' + this.translator.get('statusQueue') + '</div>';
        }
        return '';
    };
    ProgressDialog.prototype.renderContent = function (item) {
        var content = void 0;
        if (item.isExecuting()) {
            content = this.renderProgressIndicator(item);
        }
        return (content === undefined) ? '' : '<div class="progress-step-content">' + content + '</div>';
    };
    ProgressDialog.prototype.renderProgressIndicator = function (item) {
        var value = parseInt(String(item.getCurrentProgress()));
        if (-1 === value) {
            return '<div class="progress">' +
                '<div class="progress-bar progress-bar-striped active" style="width: 100%"></div>' +
                '</div>';
        }
        else {
            return '<div class="progress">' +
                '<div class="progress-bar progress-bar-striped progress-bar-animated active" style="width: ' + item.getPercentageProgress() + '%"></div>' +
                '</div>' +
                '<div class="progress-label text-right">' + item.getCurrentProgress() + '/' + item.getTotalProgress() +
                '</div>';
        }
    };
    return ProgressDialog;
}(component_1.Component));
exports.ProgressDialog = ProgressDialog;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var rejected_documents_1 = __webpack_require__(18);
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var translator_1 = __importDefault(__webpack_require__(7));
var ClearingBatchRejectedDocuments = /** @class */ (function (_super) {
    __extends(ClearingBatchRejectedDocuments, _super);
    function ClearingBatchRejectedDocuments(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.route = route;
        return _this;
    }
    ClearingBatchRejectedDocuments.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='trans_code']").html(cheque.trans_code);
        row.find("td[data-name='cheque_no']").html(cheque.cheque_no);
        if (cheque.rejected == 1) {
            row.find('td.reject').removeClass('color-red').addClass('color-green').html(translator_1.default.get('reconciliation.rc'));
        }
        row.find("td[data-name='bank_code']").html(cheque.bank_code);
        row.find("td[data-name='cheque_account_no']").html(cheque.cheque_account_no);
        row.find("td[data-name='check_digit_1']").html(cheque.check_digit_1);
        row.find("td[data-name='amount']").off().html(cheque.amount).priceFormat(this.priceFormat);
        row.find("td[data-name='check_digit_2']").html(cheque.check_digit_2);
    };
    ClearingBatchRejectedDocuments.prototype.updateBatchData = function (batch_data) {
        var div = document.querySelector('#reconciliation-batch-documents-general-info-box');
        div.querySelector('#batch-data-batch-documents').innerHTML = String(batch_data.batch_documents);
        div.querySelector('#batch-data-valid-documents').innerHTML = String(batch_data.valid_documents);
        div.querySelector('#batch-data-rejected-documents').innerHTML = String(batch_data.rejected_documents);
        div.querySelector('#batch-data-deleted-documents').innerHTML = String(batch_data.deleted_documents);
        div.querySelector('#batch-data-documents-difference').innerHTML = String(batch_data.documents_difference);
        div.querySelector('#batch-data-batch-total-amount').innerHTML = String(batch_data.batch_total_amount);
        div.querySelector('#batch-data-cheques-amount').innerHTML = String(batch_data.cheques_amount);
        div.querySelector('#batch-data-amount-difference').innerHTML = String(batch_data.amount_difference);
        if (batch_data.documents_difference == 0) {
            div.querySelector('#batch-data-documents-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-documents-difference').classList.add('bg-red-a-0-5');
        }
        if (batch_data.amount_difference_without_formatting == 0) {
            div.querySelector('#batch-data-amount-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-amount-difference').classList.add('bg-red-a-0-5');
        }
    };
    ClearingBatchRejectedDocuments.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            trans_code: String(this.element.find('#trans_code').val()),
            cheque_no: String(this.element.find('#cheque_no').val()),
            bank_code: String(this.element.find('#bank_code').val()),
            cheque_account_no: String(this.element.find('#cheque_account_no').val()),
            check_digit_1: String(this.element.find('#check_digit_1').val()),
            amount: String(this.element.find('#amount').unmask().trim()),
            check_digit_2: String(this.element.find('#check_digit_2').val())
        };
        cheque.trans_code = String(this.element.find('#trans_code').val());
        cheque.cheque_no = String(this.element.find('#cheque_no').val());
        cheque.bank_code = String(this.element.find('#bank_code').val());
        cheque.cheque_account_no = String(this.element.find('#cheque_account_no').val());
        cheque.check_digit_1 = String(this.element.find('#check_digit_1').val());
        cheque.amount = String(this.element.find('#amount').unmask().trim());
        cheque.check_digit_2 = String(this.element.find('#check_digit_2').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.hasValidTransCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_trans_code'));
            return false;
        }
        if (!validator.hasValidBankCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_bank_code'));
            return false;
        }
        //Validate the whole Check Digit 1 Code Line.
        if (!validator.hasValidCheckDigit1CodeLine(data)) {
            return false;
        }
        //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
        if (validator.isValidAmount(data.amount)) {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                if (!validator.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(validator.translator.get('reconciliation.validation.empty_check_digit_2'));
                    return false;
                }
                if (!validator.hasValidCheckDigit2CodeLine(data)) {
                    return false;
                }
            }
        }
        else {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
                return false;
            }
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.trans_code = data.trans_code;
                cheque.cheque_no = data.cheque_no;
                cheque.bank_code = data.bank_code;
                cheque.cheque_account_no = data.cheque_account_no;
                cheque.check_digit_1 = data.check_digit_1;
                cheque.amount = data.amount;
                cheque.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    ClearingBatchRejectedDocuments.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(cheque.batch_no);
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#trans_code').val(cheque.trans_code);
        this.element.find('#cheque_no').val(cheque.cheque_no);
        this.element.find('#bank_code').val(cheque.bank_code);
        this.element.find('#cheque_account_no').val(cheque.cheque_account_no);
        this.element.find('#check_digit_1').val(cheque.check_digit_1);
        this.element.find('#amount').off().val(cheque.amount).priceFormat($.extend({}, this.priceFormat, { 'clearOnEmpty': 'true' }));
        if (cheque.check_digit_2 !== null && cheque.check_digit_2 !== '') {
            this.element.find('#check_digit_2').val(cheque.check_digit_2);
        }
        else {
            this.element.find('#check_digit_2').val('');
        }
    };
    return ClearingBatchRejectedDocuments;
}(rejected_documents_1.RejectedDocuments));
exports.ClearingBatchRejectedDocuments = ClearingBatchRejectedDocuments;
var ClearingCustomQueryRejectedDocuments = /** @class */ (function (_super) {
    __extends(ClearingCustomQueryRejectedDocuments, _super);
    function ClearingCustomQueryRejectedDocuments(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        return _this;
    }
    ClearingCustomQueryRejectedDocuments.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            trans_code: String(this.element.find('#trans_code').val()),
            cheque_no: String(this.element.find('#cheque_no').val()),
            bank_code: String(this.element.find('#bank_code').val()),
            cheque_account_no: String(this.element.find('#cheque_account_no').val()),
            check_digit_1: String(this.element.find('#check_digit_1').val()),
            amount: String(this.element.find('#amount').unmask().trim()),
            check_digit_2: String(this.element.find('#check_digit_2').val())
        };
        cheque.trans_code = String(this.element.find('#trans_code').val());
        cheque.cheque_no = String(this.element.find('#cheque_no').val());
        cheque.bank_code = String(this.element.find('#bank_code').val());
        cheque.cheque_account_no = String(this.element.find('#cheque_account_no').val());
        cheque.check_digit_1 = String(this.element.find('#check_digit_1').val());
        cheque.amount = String(this.element.find('#amount').unmask().trim());
        cheque.check_digit_2 = String(this.element.find('#check_digit_2').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.hasValidTransCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_trans_code'));
            return false;
        }
        if (!validator.hasValidBankCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_bank_code'));
            return false;
        }
        //Validate the whole Check Digit 1 Code Line.
        if (!validator.hasValidCheckDigit1CodeLine(data)) {
            return false;
        }
        //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
        if (validator.isValidAmount(data.amount)) {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                if (!validator.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(validator.translator.get('reconciliation.validation.empty_check_digit_2'));
                    return false;
                }
                if (!validator.hasValidCheckDigit2CodeLine(data)) {
                    return false;
                }
            }
        }
        else {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
                return false;
            }
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.trans_code = data.trans_code;
                cheque.cheque_no = data.cheque_no;
                cheque.bank_code = data.bank_code;
                cheque.cheque_account_no = data.cheque_account_no;
                cheque.check_digit_1 = data.check_digit_1;
                cheque.amount = data.amount;
                cheque.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return ClearingCustomQueryRejectedDocuments;
}(ClearingBatchRejectedDocuments));
exports.ClearingCustomQueryRejectedDocuments = ClearingCustomQueryRejectedDocuments;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var RejectedDocuments = /** @class */ (function () {
    function RejectedDocuments(element, options) {
        var _this = this;
        this.priceFormat = {};
        this.focusableInputsErrors = [];
        this.nextErrorToFocus = 0;
        this.keys_enabled = true;
        this.alertForEndOfDocument = true;
        this.nextInputToFocus = 0;
        this.currentItemIndex = 0;
        this.id = 0;
        this.images = [];
        this.element = $(element);
        this.cheques = options.items;
        this.image_manipulation_main = this.element.find('.image-manipulation-main');
        this.priceFormat = __assign({}, this.priceFormat, options.priceFormat);
        this.cheques.forEach(function (cheque) {
            _this.images[cheque.id] = cheque.image;
        });
        this.focusableInputs = this.element.find('.user-input');
        this.id = $('div.documents-list').find('tr.active').data('item-id');
        this.cheques.forEach(function (cheque, index) {
            if (cheque.id === _this.id) {
                _this.currentItemIndex = index;
                return false;
            }
        });
        this.setEventHandlers();
    }
    /**
     * Set the event handlers.
     */
    RejectedDocuments.prototype.setEventHandlers = function () {
        var _this = this;
        this.element.off();
        this.element.on('show.bs.modal', function () {
            _this.displayImage();
            _this.enableKeyDown();
        });
        this.element.on('shown.bs.modal', function () {
            _this.element.draggable({
                handle: ".modal-content"
            });
            _this.displayItem();
        });
        this.element.on('hidden.bs.modal', function () {
            $(document).off('keydown');
            $(document).off('keyup');
            _this.element.draggable("destroy");
        });
        this.element.on('focus', '.user-input', function (e) {
            _this.nextInputToFocus = +String($(e.currentTarget).attr('tabindex'));
        });
        this.element.find('.first-item').on("click", function () {
            _this.nextInputToFocus = 0;
            _this.firstItem();
            _this.displayImage();
        });
        this.element.find('.last-item').on("click", function () {
            _this.nextInputToFocus = 0;
            _this.lastItem();
            _this.displayImage();
        });
        this.element.find('.prev-item').on("click", function () {
            _this.nextInputToFocus = 0;
            _this.prevItem();
            _this.displayImage();
        });
        this.element.find('.next-item').on("click", function () {
            _this.nextInputToFocus = 0;
            _this.nextItem();
            _this.displayImage();
        });
    };
    /**
     * Enable keydown event.
     */
    RejectedDocuments.prototype.enableKeyDown = function () {
        var _this = this;
        this.keys_enabled = true;
        $(document).on('keydown', function (e) {
            //this.searchForErrors();
            if (_this.element.hasClass('show')) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    if (_this.element.find('#doc_no input').is(":focus")) {
                        _this.nextInputToFocus = 0;
                    }
                    else {
                        if (_this.focusableInputsErrors.length === 0) {
                            _this.nextInputToFocus = 0;
                            _this.focusInputByIndex();
                        }
                        else {
                            //this.focusInputByError();
                        }
                    }
                }
                if (keyCode === 9) {
                    e.preventDefault();
                    if (_this.focusableInputsErrors.length === 0) {
                        _this.nextInputToFocus = _this.nextInputToFocus + 1;
                        if (_this.nextInputToFocus > (_this.focusableInputs.length - 1)) {
                            _this.nextInputToFocus = 0;
                        }
                        _this.focusInputByIndex();
                    }
                    else {
                        //this.focusInputByError();
                    }
                }
                if (keyCode === 38) {
                    e.preventDefault();
                    _this.nextInputToFocus = 0;
                    _this.nextItem();
                }
                if (keyCode === 40) {
                    e.preventDefault();
                    _this.nextInputToFocus = 0;
                    _this.prevItem();
                }
            }
        }).on('keyup', function (e) {
            if (_this.element.hasClass('show')) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    if (_this.element.find('#doc_no input').is(":focus")) {
                        _this.changeDocNo();
                        return false;
                    }
                    else {
                        if (_this.focusableInputsErrors.length === 0) {
                            e.preventDefault();
                            _this.nextItem(true);
                            _this.displayImage();
                        }
                    }
                }
                if (keyCode === 38) {
                    e.preventDefault();
                    _this.displayImage();
                }
                if (keyCode === 40) {
                    e.preventDefault();
                    _this.displayImage();
                }
            }
        });
    };
    /**
     * Move to the following cheque doc no.
     */
    RejectedDocuments.prototype.changeDocNo = function () {
        var doc_no_input = this.element.find('#doc_no input');
        var doc_no = +String(doc_no_input.val());
        if (!isNaN(doc_no) && doc_no > 0) {
            if (doc_no <= this.cheques.length) {
                this.currentItemIndex = +String(doc_no_input.val()) - 1;
                this.displayItem();
                this.displayImage();
            }
            else {
                toastr.error('The maximum number of documents is ' + this.cheques.length + '.');
            }
        }
    };
    /**
     * Set Progress Bar
     */
    RejectedDocuments.prototype.setProgressBar = function () {
        var current = this.currentItemIndex + 1;
        var percentage = (100 * (this.currentItemIndex + 1)) / this.cheques.length;
        this.element.find('.current').html(String(current));
        this.element.find('.total').html(String(this.cheques.length));
        this.element.find('.progress-bar').width(percentage + '%');
        if (percentage > 70) {
            this.element.find('.progress-bar-w-centered-numbers span:first').addClass('color-white');
        }
        else {
            this.element.find('.progress-bar-w-centered-numbers span:first').removeClass('color-white');
        }
    };
    /**
     * Focus by index.
     */
    RejectedDocuments.prototype.focusInputByIndex = function () {
        $(this.focusableInputs[this.nextInputToFocus]).trigger("focus").trigger("select");
    };
    /**
     * Go to the first item.
     */
    RejectedDocuments.prototype.firstItem = function () {
        this.currentItemIndex = 0;
        this.displayItem();
    };
    /**
     * Got to the last item.
     */
    RejectedDocuments.prototype.lastItem = function () {
        this.currentItemIndex = this.cheques.length - 1;
        this.displayItem();
    };
    /**
     * Got to previous item.
     */
    RejectedDocuments.prototype.prevItem = function () {
        var previousItemIndex = this.currentItemIndex - 1;
        if (previousItemIndex >= 0) {
            this.currentItemIndex = previousItemIndex;
            this.displayItem();
        }
        else {
            previousItemIndex = this.cheques.length - 1;
            this.currentItemIndex = previousItemIndex;
            this.displayItem();
        }
    };
    /**
     * Go to next item.
     *
     * @param save
     * @returns {boolean}
     */
    RejectedDocuments.prototype.nextItem = function (save) {
        if (save === void 0) { save = false; }
        save = save || false;
        if (save === true) {
            if (this.currentItemIndex <= this.cheques.length - 1) {
                if (!this.saveItem(this.currentItemIndex)) {
                    return false;
                }
            }
        }
        if (this.currentItemIndex === this.cheques.length - 1) {
            if (this.alertForEndOfDocument) {
                // @ts-ignore
                swal({
                    title: 'End of document',
                    timer: 1500,
                    type: 'info'
                });
                this.alertForEndOfDocument = false;
            }
            else {
                this.alertForEndOfDocument = true;
                var nextItemIndex = 0;
                if (nextItemIndex <= this.cheques.length - 1) {
                    this.currentItemIndex = nextItemIndex;
                    this.displayItem();
                }
            }
        }
        else {
            var nextItemIndex = this.currentItemIndex + 1;
            if (nextItemIndex <= this.cheques.length - 1) {
                this.currentItemIndex = nextItemIndex;
                this.displayItem();
            }
        }
    };
    RejectedDocuments.prototype.searchForErrors = function () {
        this.focusableInputsErrors = [];
        $.each(this.focusableInputs, function (key, elem) {
            var hasError = false, str = $(elem).val();
            for (var i = 0; i < str.length; i++) {
                var error = {};
                error['element'] = elem;
                error['character'] = i;
                if (str[i] === "?" || str[i] === ":" || str[i] === ";") {
                    this.focusableInputsErrors.push(error);
                    hasError = true;
                }
            }
            if (hasError) {
                $(elem).addClass('color-red');
            }
            else {
                $(elem).removeClass('color-red');
            }
        });
        this.nextErrorToFocus = 0;
    };
    RejectedDocuments.prototype.displayItem = function () {
        this.setItem();
        //this.searchForErrors();
        this.setProgressBar();
        this.focusInputByIndex();
    };
    RejectedDocuments.prototype.displayImage = function () {
        var cheque = this.cheques[this.currentItemIndex];
        if (cheque.image !== null) {
            this.image_manipulation_main.attr('data-image-id', this.images[cheque.id].id).attr('data-orientation', 'front');
            if (this.images[cheque.id].front_filename !== null) {
                this.image_manipulation_main.attr('src', this.images[cheque.id].front_filename).attr('data-front-image', this.images[cheque.id].front_filename);
            }
            if (this.images[cheque.id].back_filename !== null) {
                this.image_manipulation_main.attr('data-back-image', this.images[cheque.id].back_filename);
            }
        }
    };
    return RejectedDocuments;
}());
exports.RejectedDocuments = RejectedDocuments;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var rejected_documents_1 = __webpack_require__(18);
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var translator_1 = __importDefault(__webpack_require__(7));
var TradeFinanceBatchRejectedDocuments = /** @class */ (function (_super) {
    __extends(TradeFinanceBatchRejectedDocuments, _super);
    function TradeFinanceBatchRejectedDocuments(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.route = route;
        return _this;
    }
    TradeFinanceBatchRejectedDocuments.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='trans_code']").html(cheque.trans_code);
        row.find("td[data-name='cheque_no']").html(cheque.cheque_no);
        if (cheque.rejected == 1) {
            row.find('td.reject').removeClass('color-red').addClass('color-green').html(translator_1.default.get('reconciliation.rc'));
        }
        row.find("td[data-name='bank_code']").html(cheque.bank_code);
        row.find("td[data-name='cheque_account_no']").html(cheque.cheque_account_no);
        row.find("td[data-name='check_digit_1']").html(cheque.check_digit_1);
        row.find("td[data-name='amount']").off().html(cheque.amount).priceFormat(this.priceFormat);
        row.find("td[data-name='check_digit_2']").html(cheque.check_digit_2);
    };
    TradeFinanceBatchRejectedDocuments.prototype.updateBatchData = function (batch_data) {
        var div = document.querySelector('#reconciliation-batch-documents-general-info-box');
        div.querySelector('#batch-data-batch-documents').innerHTML = String(batch_data.batch_documents);
        div.querySelector('#batch-data-valid-documents').innerHTML = String(batch_data.valid_documents);
        div.querySelector('#batch-data-rejected-documents').innerHTML = String(batch_data.rejected_documents);
        div.querySelector('#batch-data-deleted-documents').innerHTML = String(batch_data.deleted_documents);
        div.querySelector('#batch-data-documents-difference').innerHTML = String(batch_data.documents_difference);
        div.querySelector('#batch-data-batch-total-amount').innerHTML = String(batch_data.batch_total_amount);
        div.querySelector('#batch-data-cheques-amount').innerHTML = String(batch_data.cheques_amount);
        div.querySelector('#batch-data-amount-difference').innerHTML = String(batch_data.amount_difference);
        if (batch_data.documents_difference == 0) {
            div.querySelector('#batch-data-documents-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-documents-difference').classList.add('bg-red-a-0-5');
        }
        if (batch_data.amount_difference_without_formatting == 0) {
            div.querySelector('#batch-data-amount-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-amount-difference').classList.add('bg-red-a-0-5');
        }
    };
    TradeFinanceBatchRejectedDocuments.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            trans_code: String(this.element.find('#trans_code').val()),
            cheque_no: String(this.element.find('#cheque_no').val()),
            bank_code: String(this.element.find('#bank_code').val()),
            cheque_account_no: String(this.element.find('#cheque_account_no').val()),
            check_digit_1: String(this.element.find('#check_digit_1').val()),
            amount: String(this.element.find('#amount').unmask().trim()),
            check_digit_2: String(this.element.find('#check_digit_2').val())
        };
        cheque.trans_code = String(this.element.find('#trans_code').val());
        cheque.cheque_no = String(this.element.find('#cheque_no').val());
        cheque.bank_code = String(this.element.find('#bank_code').val());
        cheque.cheque_account_no = String(this.element.find('#cheque_account_no').val());
        cheque.check_digit_1 = String(this.element.find('#check_digit_1').val());
        cheque.amount = String(this.element.find('#amount').unmask().trim());
        cheque.check_digit_2 = String(this.element.find('#check_digit_2').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.hasValidTransCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_trans_code'));
            return false;
        }
        if (!validator.hasValidBankCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_bank_code'));
            return false;
        }
        //Validate the whole Check Digit 1 Code Line.
        if (!validator.hasValidCheckDigit1CodeLine(data)) {
            return false;
        }
        //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
        if (validator.isValidAmount(data.amount)) {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                if (!validator.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(validator.translator.get('reconciliation.validation.empty_check_digit_2'));
                    return false;
                }
                if (!validator.hasValidCheckDigit2CodeLine(data)) {
                    return false;
                }
            }
        }
        else {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
                return false;
            }
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.trans_code = data.trans_code;
                cheque.cheque_no = data.cheque_no;
                cheque.bank_code = data.bank_code;
                cheque.cheque_account_no = data.cheque_account_no;
                cheque.check_digit_1 = data.check_digit_1;
                cheque.amount = data.amount;
                cheque.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    TradeFinanceBatchRejectedDocuments.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(cheque.batch_no);
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#trans_code').val(cheque.trans_code);
        this.element.find('#cheque_no').val(cheque.cheque_no);
        this.element.find('#bank_code').val(cheque.bank_code);
        this.element.find('#cheque_account_no').val(cheque.cheque_account_no);
        this.element.find('#check_digit_1').val(cheque.check_digit_1);
        this.element.find('#amount').off().val(cheque.amount).priceFormat($.extend({}, this.priceFormat, { 'clearOnEmpty': 'true' }));
        if (cheque.check_digit_2 !== null && cheque.check_digit_2 !== '') {
            this.element.find('#check_digit_2').val(cheque.check_digit_2);
        }
        else {
            this.element.find('#check_digit_2').val('');
        }
    };
    return TradeFinanceBatchRejectedDocuments;
}(rejected_documents_1.RejectedDocuments));
exports.TradeFinanceBatchRejectedDocuments = TradeFinanceBatchRejectedDocuments;
var TradeFinanceCustomQueryRejectedDocuments = /** @class */ (function (_super) {
    __extends(TradeFinanceCustomQueryRejectedDocuments, _super);
    function TradeFinanceCustomQueryRejectedDocuments(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        return _this;
    }
    TradeFinanceCustomQueryRejectedDocuments.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            trans_code: String(this.element.find('#trans_code').val()),
            cheque_no: String(this.element.find('#cheque_no').val()),
            bank_code: String(this.element.find('#bank_code').val()),
            cheque_account_no: String(this.element.find('#cheque_account_no').val()),
            check_digit_1: String(this.element.find('#check_digit_1').val()),
            amount: String(this.element.find('#amount').unmask().trim()),
            check_digit_2: String(this.element.find('#check_digit_2').val())
        };
        cheque.trans_code = String(this.element.find('#trans_code').val());
        cheque.cheque_no = String(this.element.find('#cheque_no').val());
        cheque.bank_code = String(this.element.find('#bank_code').val());
        cheque.cheque_account_no = String(this.element.find('#cheque_account_no').val());
        cheque.check_digit_1 = String(this.element.find('#check_digit_1').val());
        cheque.amount = String(this.element.find('#amount').unmask().trim());
        cheque.check_digit_2 = String(this.element.find('#check_digit_2').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.hasValidTransCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_trans_code'));
            return false;
        }
        if (!validator.hasValidBankCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_bank_code'));
            return false;
        }
        //Validate the whole Check Digit 1 Code Line.
        if (!validator.hasValidCheckDigit1CodeLine(data)) {
            return false;
        }
        //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
        if (validator.isValidAmount(data.amount)) {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                if (!validator.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(validator.translator.get('reconciliation.validation.empty_check_digit_2'));
                    return false;
                }
                if (!validator.hasValidCheckDigit2CodeLine(data)) {
                    return false;
                }
            }
        }
        else {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
                return false;
            }
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.trans_code = data.trans_code;
                cheque.cheque_no = data.cheque_no;
                cheque.bank_code = data.bank_code;
                cheque.cheque_account_no = data.cheque_account_no;
                cheque.check_digit_1 = data.check_digit_1;
                cheque.amount = data.amount;
                cheque.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return TradeFinanceCustomQueryRejectedDocuments;
}(TradeFinanceBatchRejectedDocuments));
exports.TradeFinanceCustomQueryRejectedDocuments = TradeFinanceCustomQueryRejectedDocuments;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var rejected_documents_1 = __webpack_require__(18);
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var translator_1 = __importDefault(__webpack_require__(7));
var ForeignChequesBatchRejectedDocuments = /** @class */ (function (_super) {
    __extends(ForeignChequesBatchRejectedDocuments, _super);
    function ForeignChequesBatchRejectedDocuments(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.route = route;
        return _this;
    }
    ForeignChequesBatchRejectedDocuments.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='micr']").html(cheque.micr);
        if (cheque.rejected == 1) {
            row.find('td.reject').removeClass('color-red').addClass('color-green').html(translator_1.default.get('reconciliation.rc'));
        }
    };
    ForeignChequesBatchRejectedDocuments.prototype.updateBatchData = function (batch_data) {
        var div = document.querySelector('#reconciliation-batch-documents-general-info-box');
        div.querySelector('#batch-data-batch-documents').innerHTML = String(batch_data.batch_documents);
        div.querySelector('#batch-data-valid-documents').innerHTML = String(batch_data.valid_documents);
        div.querySelector('#batch-data-rejected-documents').innerHTML = String(batch_data.rejected_documents);
        div.querySelector('#batch-data-deleted-documents').innerHTML = String(batch_data.deleted_documents);
        div.querySelector('#batch-data-documents-difference').innerHTML = String(batch_data.documents_difference);
        div.querySelector('#batch-data-batch-total-amount').innerHTML = String(batch_data.batch_total_amount);
        div.querySelector('#batch-data-cheques-amount').innerHTML = String(batch_data.cheques_amount);
        div.querySelector('#batch-data-amount-difference').innerHTML = String(batch_data.amount_difference);
        if (batch_data.documents_difference == 0) {
            div.querySelector('#batch-data-documents-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-documents-difference').classList.add('bg-red-a-0-5');
        }
        if (batch_data.amount_difference_without_formatting == 0) {
            div.querySelector('#batch-data-amount-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-amount-difference').classList.add('bg-red-a-0-5');
        }
    };
    ForeignChequesBatchRejectedDocuments.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            micr: String(this.element.find('#micr').val())
        };
        cheque.micr = String(this.element.find('#micr').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidMicr(data.micr)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_micr'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.micr = data.micr;
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    ForeignChequesBatchRejectedDocuments.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(cheque.batch_no);
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#micr').val(cheque.micr);
    };
    return ForeignChequesBatchRejectedDocuments;
}(rejected_documents_1.RejectedDocuments));
exports.ForeignChequesBatchRejectedDocuments = ForeignChequesBatchRejectedDocuments;
var ForeignChequesCustomQueryRejectedDocuments = /** @class */ (function (_super) {
    __extends(ForeignChequesCustomQueryRejectedDocuments, _super);
    function ForeignChequesCustomQueryRejectedDocuments(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        return _this;
    }
    ForeignChequesCustomQueryRejectedDocuments.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            micr: String(this.element.find('#micr').val())
        };
        cheque.micr = String(this.element.find('#micr').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidMicr(data.micr)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_micr'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.micr = data.micr;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return ForeignChequesCustomQueryRejectedDocuments;
}(ForeignChequesBatchRejectedDocuments));
exports.ForeignChequesCustomQueryRejectedDocuments = ForeignChequesCustomQueryRejectedDocuments;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var data_entry_1 = __webpack_require__(12);
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var ForeignChequesBatchDataEntry = /** @class */ (function (_super) {
    __extends(ForeignChequesBatchDataEntry, _super);
    function ForeignChequesBatchDataEntry(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.currency = options.currency;
        _this.route = route;
        _this.priceFormat = __assign({}, _this.priceFormat, { 'prefix': options.batch.currency + ' ' });
        return _this;
    }
    ForeignChequesBatchDataEntry.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(String(cheque.batch_no));
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#amount').off().val(cheque.amount).priceFormat(__assign({}, this.priceFormat, { 'clearOnEmpty': 'true' }));
    };
    ForeignChequesBatchDataEntry.prototype.updateBatchData = function (batch_data) {
        var div = document.querySelector('#reconciliation-batch-documents-general-info-box');
        div.querySelector('#batch-data-batch-documents').innerHTML = String(batch_data.batch_documents);
        div.querySelector('#batch-data-valid-documents').innerHTML = String(batch_data.valid_documents);
        div.querySelector('#batch-data-rejected-documents').innerHTML = String(batch_data.rejected_documents);
        div.querySelector('#batch-data-deleted-documents').innerHTML = String(batch_data.deleted_documents);
        div.querySelector('#batch-data-documents-difference').innerHTML = String(batch_data.documents_difference);
        div.querySelector('#batch-data-batch-total-amount').innerHTML = String(batch_data.batch_total_amount);
        div.querySelector('#batch-data-cheques-amount').innerHTML = String(batch_data.cheques_amount);
        div.querySelector('#batch-data-amount-difference').innerHTML = String(batch_data.amount_difference);
        if (batch_data.documents_difference == 0) {
            div.querySelector('#batch-data-documents-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-documents-difference').classList.add('bg-red-a-0-5');
        }
        if (batch_data.amount_difference_without_formatting == 0) {
            div.querySelector('#batch-data-amount-difference').classList.remove('bg-red-a-0-5');
        }
        else {
            div.querySelector('#batch-data-amount-difference').classList.add('bg-red-a-0-5');
        }
    };
    ForeignChequesBatchDataEntry.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='amount']").off().html(cheque.amount).priceFormat(this.priceFormat);
    };
    /**
     * Save item.
     *
     * @param index
     * @returns {boolean}
     */
    ForeignChequesBatchDataEntry.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            amount: this.element.find('#amount').unmask().trim(),
        };
        cheque.amount = this.element.find('#amount').unmask().trim();
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidAmount(data.amount)) {
            toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.amount = data.amount;
                _this.calculateBatchAmounts();
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    return ForeignChequesBatchDataEntry;
}(data_entry_1.DataEntry));
exports.ForeignChequesBatchDataEntry = ForeignChequesBatchDataEntry;
var ForeignChequesCustomQueryDataEntry = /** @class */ (function (_super) {
    __extends(ForeignChequesCustomQueryDataEntry, _super);
    function ForeignChequesCustomQueryDataEntry(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.currency = options.currency;
        _this.route = route;
        _this.priceFormat = __assign({}, _this.priceFormat, { 'prefix': options.batch.currency + ' ' });
        return _this;
    }
    /**
     * Save item.
     *
     * @param index
     * @returns {boolean}
     */
    ForeignChequesCustomQueryDataEntry.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            amount: this.element.find('#amount').unmask().trim(),
        };
        cheque.amount = this.element.find('#amount').unmask().trim();
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.isValidAmount(data.amount)) {
            toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
            return false;
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.amount = data.amount;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    ForeignChequesCustomQueryDataEntry.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(String(cheque.batch_no));
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#amount').off().val(cheque.amount).priceFormat(__assign({}, this.priceFormat, { 'clearOnEmpty': 'true', 'prefix': cheque.currency + ' ' }));
    };
    ForeignChequesCustomQueryDataEntry.prototype.updateTableCell = function (cheque) {
        var row = $('div.documents-list').find('tr[data-item-id="' + cheque.id + '"]');
        row.find("td[data-name='amount']").off().html(cheque.amount).priceFormat(__assign({}, this.priceFormat, { 'clearOnEmpty': 'true', 'prefix': cheque.currency + ' ' }));
    };
    return ForeignChequesCustomQueryDataEntry;
}(ForeignChequesBatchDataEntry));
exports.ForeignChequesCustomQueryDataEntry = ForeignChequesCustomQueryDataEntry;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var technical_control_1 = __webpack_require__(23);
var router_1 = __importDefault(__webpack_require__(3));
var translator_1 = __importDefault(__webpack_require__(7));
var ClearingTechnicalControl = /** @class */ (function (_super) {
    __extends(ClearingTechnicalControl, _super);
    function ClearingTechnicalControl(element, options, route, signature) {
        var _this = _super.call(this, element, options, signature) || this;
        _this.route = route;
        return _this;
    }
    ClearingTechnicalControl.prototype.setDocumentRow = function (index) {
        var cheque = this.cheques[index];
        var row = this.element.find('#technical-control-documents-box table [data-document-id=' + cheque.id + ']');
        if (cheque.return_reason_1 !== null) {
            row.find('.rr1').html(cheque.return_reason_1);
            row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
        }
        else {
            row.find('.rr1').empty();
            row.find('.checked').empty();
        }
        if (cheque.return_reason_2 !== null) {
            row.find('.rr2').html(cheque.return_reason_2);
        }
        else {
            row.find('.rr2').empty();
        }
    };
    ClearingTechnicalControl.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('.position').html(String(this.currentItemIndex + 1));
        this.element.find('.cheque_no').html(cheque.cheque_no);
        this.element.find('.cheque_account_no').html(cheque.cheque_account_no);
        this.element.find('.amount').html(cheque.amount);
        this.element.find('.current .rr1').html(cheque.return_reason_1);
        this.element.find('.current .rr2').html(cheque.return_reason_2);
        if (cheque.return_reason_1 !== null && cheque.return_reason_1 !== '') {
            this.element.find('.current-checked').html('<i class="fa color-green fa-check-square"></i>');
        }
        else {
            this.element.find('.current-checked').html('');
        }
        var rr1TechnicalControlOptions = this.technicalControlStatuses.map(function (obj) {
            if (obj.code == '00' && cheque.return_reason_1 == null) {
                return {
                    id: obj.id || obj.code,
                    text: obj.description,
                    selected: true
                };
            }
            else {
                if (obj.code == cheque.return_reason_1) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.description,
                        selected: true
                    };
                }
                else {
                    return {
                        id: obj.id || obj.code,
                        text: obj.description
                    };
                }
            }
        });
        var rr2TechnicalControlOptions = this.technicalControlStatuses.map(function (obj) {
            if (obj.code == '00' && cheque.return_reason_2 == null) {
                return {
                    id: obj.id || obj.code,
                    text: obj.description,
                    selected: true
                };
            }
            else {
                if (obj.code == cheque.return_reason_2) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.description,
                        selected: true
                    };
                }
                else {
                    return {
                        id: obj.id || obj.code,
                        text: obj.description
                    };
                }
            }
        });
        this.element.find('#rr1-input').empty().select2({
            data: rr1TechnicalControlOptions,
            dropdownAutoWidth: true
        });
        this.element.find('#rr2-input').empty().select2({
            data: rr2TechnicalControlOptions,
            dropdownAutoWidth: true
        });
    };
    ClearingTechnicalControl.prototype.saveItem = function () {
        var _this = this;
        this.save_ajax_flag = true;
        var index = this.currentItemIndex;
        var cheque = this.cheques[index];
        var rr1 = this.element.find('#rr1-input').val();
        var rr2 = this.element.find('#rr2-input').val();
        if (rr1 == "00" && rr2 != "00") {
            toastr.error(translator_1.default.get('technical_control.branch.messages.error.wrong_return_reason_2'));
            return false;
        }
        if (rr1 != "00" && rr1 == rr2) {
            toastr.error(translator_1.default.get('technical_control.branch.messages.error.same_reason'));
            return false;
        }
        if (index === (this.cheques.length - 1)) {
            $('#confirm').prop('disabled', true);
        }
        else {
            $('#confirm').prop('disabled', true);
            this.nextItem();
        }
        $.ajax({
            url: router_1.default.get('technical-control.archive.save', { item_id: cheque.id }),
            type: 'PUT',
            data: { rr1: rr1, rr2: rr2 },
            dataType: 'json'
        }).done(function (data) {
            _this.save_ajax_flag = false;
            $('#confirm').prop('disabled', false);
            if (data.return === true) {
                cheque.return_reason_1 = rr1;
                cheque.return_reason_2 = rr2;
                _this.setProcessed();
                _this.setDocumentRow(index);
                toastr.success(data.message, '');
            }
            else {
                toastr.error(data.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
            _this.save_ajax_flag = false;
            $('#confirm').prop('disabled', false);
        });
    };
    return ClearingTechnicalControl;
}(technical_control_1.TechnicalControl));
exports.ClearingTechnicalControl = ClearingTechnicalControl;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var TechnicalControl = /** @class */ (function () {
    function TechnicalControl(element, options, signature) {
        var _this = this;
        this.technicalControlStatuses = [];
        this.priceFormat = {};
        this.save_ajax_flag = false;
        this.signature_ajax_flag = false;
        this.currentItemIndex = 0;
        this.images = [];
        this.element = $(element);
        this.cheques = options.items;
        this.technicalControlStatuses = options.technicalControlStatuses;
        this.image_manipulation_main = this.element.find('.image-manipulation-main');
        this.priceFormat = __assign({}, this.priceFormat, options.priceFormat);
        this.signature = signature;
        this.cheques.some(function (cheque, index) {
            if (cheque.return_reason_1 == null && cheque.return_reason_2 == null) {
                _this.currentItemIndex = index;
                return true;
            }
            return false;
        });
        this.cheques.forEach(function (cheque) {
            _this.images[cheque.id] = cheque.image;
        });
        this.displayImage();
        this.displayItem();
        this.displaySignatures();
        this.setEventHandlers();
    }
    TechnicalControl.prototype.setEventHandlers = function () {
        var _this = this;
        this.element.on('click', 'tr[data-document-id]', function (e) {
            var id = parseInt($(e.currentTarget).attr('data-document-id'));
            _this.cheques.some(function (cheque, index) {
                if (cheque.id == id) {
                    _this.currentItemIndex = index;
                    return true;
                }
                return false;
            });
            _this.displayItem();
            _this.displayImage();
            _this.displaySignatures();
        });
        $('#confirm').on('click', function () {
            if (_this.currentItemIndex <= (_this.cheques.length - 1)) {
                if (!_this.save_ajax_flag && !_this.signature_ajax_flag) {
                    _this.saveItem();
                    _this.displayImage();
                    _this.displaySignatures();
                }
            }
        }).prop('disabled', false);
        $(document).on('keydown', function (e) {
            var keyCode = e.keyCode || e.which;
            //Enter key was pressed.
            if (keyCode === 13) {
                if (_this.element.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                if (_this.currentItemIndex !== 0) {
                    if (!_this.signature_ajax_flag) {
                        _this.prevItem();
                    }
                }
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                if (_this.currentItemIndex !== (_this.cheques.length - 1)) {
                    if (!_this.signature_ajax_flag) {
                        _this.nextItem();
                    }
                }
            }
        }).on('keyup', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                if (_this.element.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
                if (_this.currentItemIndex <= (_this.cheques.length - 1)) {
                    $('#confirm').trigger('click');
                }
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                if (!_this.signature_ajax_flag) {
                    _this.displayImage();
                    _this.displaySignatures();
                }
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                if (!_this.signature_ajax_flag) {
                    _this.displayImage();
                    _this.displaySignatures();
                }
            }
        });
    };
    TechnicalControl.prototype.prevItem = function () {
        var previousItemIndex = this.currentItemIndex - 1;
        if (previousItemIndex >= 0) {
            this.currentItemIndex = previousItemIndex;
            this.displayItem();
        }
    };
    TechnicalControl.prototype.nextItem = function () {
        var nextItemIndex = this.currentItemIndex + 1;
        if (nextItemIndex <= (this.cheques.length - 1)) {
            this.currentItemIndex = nextItemIndex;
            this.displayItem();
        }
    };
    TechnicalControl.prototype.scroll = function () {
        var cheque = this.cheques[this.currentItemIndex];
        var position = this.element.find('#technical-control-documents-box table [data-document-id=' + cheque.id + ']').position().top;
        this.element.find('#technical-control-documents-box .cardbox-body').animate({
            scrollTop: position
        }, 0);
    };
    TechnicalControl.prototype.displayItem = function () {
        this.setCurrent();
        this.setItem();
        this.scroll();
        //highlightSignRule();
    };
    TechnicalControl.prototype.displayImage = function () {
        var cheque = this.cheques[this.currentItemIndex];
        if (cheque.image !== null) {
            this.image_manipulation_main.attr('data-image-id', cheque.id).attr('data-orientation', 'front');
            if (this.images[cheque.id].front_filename !== null) {
                this.image_manipulation_main.attr('src', this.images[cheque.id].front_filename).attr('data-front-image', this.images[cheque.id].front_filename);
            }
            if (this.images[cheque.id].back_filename !== null) {
                this.image_manipulation_main.attr('data-back-image', this.images[cheque.id].back_filename);
            }
        }
    };
    TechnicalControl.prototype.displaySignatures = function () {
        var cheque = this.cheques[this.currentItemIndex];
        if (typeof this.signature !== "undefined") {
            this.signature.sendRequest(this, cheque);
        }
        else {
            console.log('no signatures');
        }
    };
    TechnicalControl.prototype.setCurrent = function () {
        var cheque = this.cheques[this.currentItemIndex];
        var current_row = this.element.find('#technical-control-documents-box table [data-document-id=' + cheque.id + ']');
        this.element.find('span.current-document').html(String(this.currentItemIndex + 1));
        this.element.find('#technical-control-documents-box table tr.current').removeClass('current');
        current_row.addClass('current');
    };
    TechnicalControl.prototype.setProcessed = function () {
        var processed = this.cheques.filter(function (cheque) {
            return cheque.return_reason_1 !== null;
        }).length;
        $('.processed').html(String(processed));
        var percentage = (processed * 100) / this.cheques.length;
        if (percentage > 70) {
            $('.progress-bar').css('width', percentage + '%').prev().addClass('color-white');
        }
        else {
            $('.progress-bar').css('width', percentage + '%');
        }
    };
    return TechnicalControl;
}());
exports.TechnicalControl = TechnicalControl;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var held_cheques_1 = __webpack_require__(25);
var router_1 = __importDefault(__webpack_require__(3));
var ClearingHeldCheques = /** @class */ (function (_super) {
    __extends(ClearingHeldCheques, _super);
    function ClearingHeldCheques(element, options, route) {
        var _this = _super.call(this, element, options) || this;
        _this.route = route;
        return _this;
    }
    ClearingHeldCheques.prototype.setDocumentRow = function (index) {
        var cheque = this.cheques[index];
        var row = this.element.find('#held-cheques-documents-box table [data-document-id=' + cheque.id + ']');
        if (cheque.session_id == this.previousSessionId) {
            if ($.inArray(cheque.return_reason_1, this.heldChequesStatusCodes) !== -1 && $.inArray(cheque.return_reason_2, this.heldChequesStatusCodes) !== -1 && cheque.return_reason_1 != this.nextDayDecision) {
                row.find('.rr1').html(cheque.return_reason_1);
                row.find('.rr2').html(cheque.return_reason_2);
                row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
            }
            else {
                row.find('.rr1').empty();
                row.find('.rr2').empty();
                row.find('.checked').empty();
            }
        }
        else {
            if ($.inArray(cheque.return_reason_1, this.heldChequesStatusCodes) !== -1 && $.inArray(cheque.return_reason_2, this.heldChequesStatusCodes) !== -1) {
                row.find('.rr1').html(cheque.return_reason_1);
                row.find('.rr2').html(cheque.return_reason_2);
                row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
            }
            else {
                row.find('.rr1').empty();
                row.find('.rr2').empty();
                row.find('.checked').empty();
            }
        }
    };
    ClearingHeldCheques.prototype.setItem = function () {
        var _this = this;
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('.position').html(String(this.currentItemIndex + 1));
        this.element.find('.cheque_no').html(cheque.cheque_no);
        this.element.find('.cheque_account_no').html(cheque.cheque_account_no);
        this.element.find('.amount').html(cheque.amount);
        this.element.find('.current .rr1').html(cheque.return_reason_1);
        this.element.find('.current .rr2').html(cheque.return_reason_2);
        var rr1BankingSystemOptions = this.bankingSystemStatuses.filter(function (obj) {
            if (obj.code == cheque.return_reason_1) {
                return true;
            }
        }).map(function (obj) {
            if (obj.code == cheque.return_reason_1) {
                return {
                    id: obj.id || obj.code,
                    text: obj.text || obj.description,
                    disabled: true,
                    selected: true
                };
            }
        });
        var rr2BankingSystemOptions = this.bankingSystemStatuses.filter(function (obj) {
            if (obj.code == cheque.return_reason_2) {
                return true;
            }
        }).map(function (obj) {
            if (obj.code == cheque.return_reason_2) {
                return {
                    id: obj.id || obj.code,
                    text: obj.text || obj.description,
                    disabled: true,
                    selected: true
                };
            }
        });
        var rr1HeldChequeOptions = this.heldChequesStatuses.filter(function (obj) {
            if (_this.previousSessionId == cheque.session_id && obj.code == _this.nextDayDecision && cheque.return_reason_1 != _this.nextDayDecision) {
                return false;
            }
            return true;
        }).map(function (obj) {
            if (_this.previousSessionId == cheque.session_id) {
                if (obj.code == _this.nextDayDecision) {
                    if (obj.code == cheque.return_reason_1) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true,
                            disabled: true
                        };
                    }
                    else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            disabled: true
                        };
                    }
                }
                else {
                    if (obj.code == cheque.return_reason_1) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        };
                    }
                    else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        };
                    }
                }
            }
            else {
                if (obj.code == cheque.return_reason_1) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        selected: true
                    };
                }
                else {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description
                    };
                }
            }
        });
        var rr2HeldChequeOptions = this.heldChequesStatuses.filter(function (obj) {
            if (_this.previousSessionId == cheque.session_id && obj.code == _this.nextDayDecision && cheque.return_reason_2 != _this.nextDayDecision) {
                return false;
            }
            return true;
        }).map(function (obj) {
            if (_this.previousSessionId == cheque.session_id) {
                if (obj.code == _this.nextDayDecision) {
                    if (obj.code == cheque.return_reason_2) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true,
                            disabled: true
                        };
                    }
                    else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            disabled: true
                        };
                    }
                }
                else {
                    if (obj.code == cheque.return_reason_2) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        };
                    }
                    else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        };
                    }
                }
            }
            else {
                if (obj.code == cheque.return_reason_2) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        selected: true
                    };
                }
                else {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description
                    };
                }
            }
        });
        this.element.find('#rr1-input').empty().select2({
            //@ts-ignore
            data: rr1BankingSystemOptions.concat(rr1HeldChequeOptions),
            dropdownAutoWidth: true
        });
        this.element.find('#rr2-input').empty().select2({
            //@ts-ignore
            data: rr2BankingSystemOptions.concat(rr2HeldChequeOptions),
            dropdownAutoWidth: true
        });
        if (cheque.session_id == this.previousSessionId) {
            if ($.inArray(cheque.return_reason_1, this.heldChequesStatusCodes) !== -1 && $.inArray(cheque.return_reason_2, this.heldChequesStatusCodes) !== -1 && cheque.return_reason_1 != this.nextDayDecision) {
                this.element.find('.current .checked').html('<i class="fa color-green fa-check-square"></i>');
                this.element.find('#current-document-box .checked').html('<i class="fa color-green fa-check-square"></i>');
            }
            else {
                this.element.find('.current .checked').empty();
                this.element.find('#current-document-box .checked').empty();
            }
        }
        else {
            if ($.inArray(cheque.return_reason_1, this.heldChequesStatusCodes) !== -1 && $.inArray(cheque.return_reason_2, this.heldChequesStatusCodes) !== -1) {
                this.element.find('.current .checked').html('<i class="fa color-green fa-check-square"></i>');
                this.element.find('#current-document-box .checked').html('<i class="fa color-green fa-check-square"></i>');
            }
            else {
                this.element.find('.current .checked').empty();
                this.element.find('#current-document-box .checked').empty();
            }
        }
    };
    ClearingHeldCheques.prototype.saveItem = function () {
        var _this = this;
        this.save_ajax_flag = true;
        var index = this.currentItemIndex;
        var cheque = this.cheques[index];
        var rr1 = this.element.find('#rr1-input').val();
        var rr2 = this.element.find('#rr2-input').val();
        if (this.currentItemIndex === (this.cheques.length - 1)) {
            $('#confirm').prop('disabled', true);
        }
        else {
            $('#confirm').prop('disabled', true);
            this.nextItem();
        }
        $.ajax({
            url: router_1.default.get('held-cheques.archive.save', { item_id: cheque.id }),
            type: 'PUT',
            data: { rr1: rr1, rr2: rr2 },
            dataType: 'json'
        }).done(function (data) {
            _this.save_ajax_flag = false;
            $('#confirm').prop('disabled', false);
            if (data.return === true) {
                cheque.return_reason_1 = rr1;
                cheque.return_reason_2 = rr2;
                _this.setProcessed();
                _this.setDocumentRow(index);
                toastr.success(data.message, '');
            }
            else {
                toastr.error(data.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
            _this.save_ajax_flag = false;
            $('#confirm').prop('disabled', false);
        });
    };
    return ClearingHeldCheques;
}(held_cheques_1.HeldCheques));
exports.ClearingHeldCheques = ClearingHeldCheques;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var HeldCheques = /** @class */ (function () {
    function HeldCheques(element, options) {
        var _this = this;
        this.heldChequesStatuses = [];
        this.heldChequesStatusCodes = [];
        this.bankingSystemStatuses = [];
        this.priceFormat = {};
        this.previousSessionId = 0;
        this.nextDayDecision = '99';
        this.save_ajax_flag = false;
        this.currentItemIndex = 0;
        this.images = [];
        this.element = $(element);
        this.cheques = options.items;
        this.previousSessionId = options.previousSessionId;
        this.nextDayDecision = options.nextDayDecision;
        this.heldChequesStatuses = options.heldChequesStatuses;
        this.bankingSystemStatuses = options.bankingSystemStatuses;
        this.image_manipulation_main = this.element.find('.image-manipulation-main');
        this.priceFormat = __assign({}, this.priceFormat, options.priceFormat);
        this.heldChequesStatusCodes = this.heldChequesStatuses.map(function (status) {
            return status.code;
        });
        this.cheques.some(function (cheque, index) {
            if (cheque.session_id == _this.previousSessionId && (cheque.return_reason_1 == _this.nextDayDecision || cheque.return_reason_2 == _this.nextDayDecision)) {
                _this.currentItemIndex = index;
                return true;
            }
            else if ($.inArray(cheque.return_reason_1, _this.heldChequesStatusCodes) === -1 || $.inArray(cheque.return_reason_2, _this.heldChequesStatusCodes) === -1) {
                _this.currentItemIndex = index;
                return true;
            }
            return false;
        });
        this.cheques.forEach(function (cheque) {
            _this.images[cheque.id] = cheque.image;
        });
        this.displayImage();
        this.displayItem();
        this.setEventHandlers();
    }
    HeldCheques.prototype.setEventHandlers = function () {
        var _this = this;
        this.element.on('click', 'tr[data-document-id]', function (e) {
            var id = parseInt($(e.currentTarget).attr('data-document-id'));
            _this.cheques.some(function (cheque, index) {
                if (cheque.id == id) {
                    _this.currentItemIndex = index;
                    return true;
                }
                return false;
            });
            _this.displayItem();
            _this.displayImage();
        });
        $('#confirm').on('click', function () {
            if (_this.currentItemIndex <= (_this.cheques.length - 1)) {
                if (!_this.save_ajax_flag) {
                    _this.saveItem();
                    _this.displayImage();
                }
            }
        }).prop('disabled', false);
        $(document).on('keydown', function (e) {
            var keyCode = e.keyCode || e.which;
            //Enter key was pressed.
            if (keyCode === 13) {
                if (_this.element.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                if (_this.currentItemIndex !== 0) {
                    _this.prevItem();
                }
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                if (_this.currentItemIndex !== (_this.cheques.length - 1)) {
                    _this.nextItem();
                }
            }
        }).on('keyup', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                if (_this.element.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
                if (_this.currentItemIndex <= (_this.cheques.length - 1)) {
                    $('#confirm').trigger('click');
                }
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                _this.displayImage();
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                _this.displayImage();
            }
        });
    };
    HeldCheques.prototype.prevItem = function () {
        var previousItemIndex = this.currentItemIndex - 1;
        if (previousItemIndex >= 0) {
            this.currentItemIndex = previousItemIndex;
            this.displayItem();
        }
    };
    HeldCheques.prototype.nextItem = function () {
        var nextItemIndex = this.currentItemIndex + 1;
        if (nextItemIndex <= (this.cheques.length - 1)) {
            this.currentItemIndex = nextItemIndex;
            this.displayItem();
        }
    };
    HeldCheques.prototype.scroll = function () {
        var cheque = this.cheques[this.currentItemIndex];
        var position = this.element.find('#held-cheques-documents-box table [data-document-id=' + cheque.id + ']').position().top;
        this.element.find('#held-cheques-documents-box .cardbox-body').animate({
            scrollTop: position
        }, 0);
    };
    HeldCheques.prototype.displayItem = function () {
        this.setCurrent();
        this.setItem();
        this.scroll();
    };
    HeldCheques.prototype.displayImage = function () {
        var cheque = this.cheques[this.currentItemIndex];
        if (cheque.image !== null) {
            this.image_manipulation_main.attr('data-image-id', cheque.id).attr('data-orientation', 'front');
            if (this.images[cheque.id].front_filename !== null) {
                this.image_manipulation_main.attr('src', this.images[cheque.id].front_filename).attr('data-front-image', this.images[cheque.id].front_filename);
            }
            if (this.images[cheque.id].back_filename !== null) {
                this.image_manipulation_main.attr('data-back-image', this.images[cheque.id].back_filename);
            }
        }
    };
    HeldCheques.prototype.setCurrent = function () {
        var cheque = this.cheques[this.currentItemIndex];
        var current_row = this.element.find('#held-cheques-documents-box table [data-document-id=' + cheque.id + ']');
        this.element.find('span.current-document').html(String(this.currentItemIndex + 1));
        this.element.find('#held-cheques-documents-box table tr.current').removeClass('current');
        current_row.addClass('current');
    };
    HeldCheques.prototype.setProcessed = function () {
        var _this = this;
        var processed = this.cheques.filter(function (cheque) {
            if (cheque.session_id == _this.previousSessionId && cheque.return_reason_1 != _this.nextDayDecision && cheque.return_reason_2 != _this.nextDayDecision) {
                return ($.inArray(cheque.return_reason_1, _this.heldChequesStatusCodes) !== -1 && $.inArray(cheque.return_reason_2, _this.heldChequesStatusCodes) !== -1);
            }
            if (cheque.session_id != _this.previousSessionId) {
                return ($.inArray(cheque.return_reason_1, _this.heldChequesStatusCodes) !== -1 && $.inArray(cheque.return_reason_2, _this.heldChequesStatusCodes) !== -1);
            }
        }).length;
        $('.processed').html(String(processed));
        var percentage = (processed * 100) / this.cheques.length;
        if (percentage > 70) {
            $('.progress-bar').css('width', percentage + '%').prev().addClass('color-white');
        }
        else {
            $('.progress-bar').css('width', percentage + '%');
        }
    };
    return HeldCheques;
}());
exports.HeldCheques = HeldCheques;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = __webpack_require__(1);
var router_1 = __importDefault(__webpack_require__(3));
var luhn_1 = __webpack_require__(8);
var rejected_documents_1 = __webpack_require__(17);
var ClearingBatchCodelineGenerator = /** @class */ (function (_super) {
    __extends(ClearingBatchCodelineGenerator, _super);
    function ClearingBatchCodelineGenerator(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        _this.bank_accounts = options.bank_accounts;
        _this.bankAccountEventHandler();
        return _this;
    }
    ClearingBatchCodelineGenerator.prototype.bankAccountEventHandler = function () {
        var _this = this;
        this.element.find('#cheque_account_no').on('select2:select', function (e) {
            var data = e.params.data;
            if (data.text != undefined && _this.bank_accounts[data.text] != undefined) {
                _this.element.find('#trans_code').val(_this.bank_accounts[data.text].trans_code);
                _this.element.find('#bank_code').val(_this.bank_accounts[data.text].bank_code);
            }
            else {
                _this.element.find('#trans_code').val('');
                _this.element.find('#bank_code').val('');
            }
        });
    };
    ClearingBatchCodelineGenerator.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            trans_code: String(this.element.find('#trans_code').val()),
            cheque_no: String(this.element.find('#cheque_no').val()),
            bank_code: String(this.element.find('#bank_code').val()),
            cheque_account_no: String(this.element.find('#cheque_account_no :selected').text()),
            check_digit_1: String(this.element.find('#check_digit_1').val()),
            amount: String(this.element.find('#amount').unmask().trim()),
            check_digit_2: String(this.element.find('#check_digit_2').val())
        };
        cheque.trans_code = String(this.element.find('#trans_code').val());
        cheque.cheque_no = String(this.element.find('#cheque_no').val());
        cheque.bank_code = String(this.element.find('#bank_code').val());
        cheque.cheque_account_no = String(this.element.find('#cheque_account_no :selected').text());
        cheque.check_digit_1 = String(this.element.find('#check_digit_1').val());
        cheque.amount = String(this.element.find('#amount').unmask().trim());
        cheque.check_digit_2 = String(this.element.find('#check_digit_2').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.hasValidTransCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_trans_code'));
            return false;
        }
        if (!validator.hasValidBankCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_bank_code'));
            return false;
        }
        if (!validator.hasValidChequeNoLength(data.cheque_no)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_cheque_no_length', { 'length': validator.padding.cheque_no }));
            return false;
        }
        if (!validator.hasValidChequeAccountNoLength(data.cheque_account_no)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_cheque_account_no_length', { 'length': validator.padding.cheque_account_no }));
            return false;
        }
        data.check_digit_1 = cheque.check_digit_1 = this.calculateCheckDigit1(cheque);
        this.element.find('#check_digit_1').val(data.check_digit_1);
        //Validate the whole Check Digit 1 Code Line.
        if (!validator.hasValidCheckDigit1CodeLine(data)) {
            return false;
        }
        //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
        if (validator.isValidAmount(data.amount)) {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                if (!validator.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(validator.translator.get('reconciliation.validation.empty_check_digit_2'));
                    return false;
                }
                if (!validator.hasValidCheckDigit2CodeLine(data)) {
                    return false;
                }
            }
        }
        else {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
                return false;
            }
        }
        this.element.find('#cheque_account_no').val('').trigger('change');
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.trans_code = data.trans_code;
                cheque.cheque_no = data.cheque_no;
                cheque.bank_code = data.bank_code;
                cheque.cheque_account_no = data.cheque_account_no;
                cheque.check_digit_1 = data.check_digit_1;
                cheque.amount = data.amount;
                cheque.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                _this.updateBatchData(response.batch);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    ClearingBatchCodelineGenerator.prototype.setItem = function () {
        var cheque = this.cheques[this.currentItemIndex];
        this.element.find('#batch_no').html(cheque.batch_no);
        this.element.find('#doc_no input').val(this.currentItemIndex + 1);
        this.element.find('#trans_code').val(cheque.trans_code);
        this.element.find('#cheque_no').val(cheque.cheque_no);
        this.element.find('#bank_code').val(cheque.bank_code);
        this.element.find('#cheque_account_no').val(cheque.cheque_account_no).trigger('change');
        this.element.find('#check_digit_1').val(cheque.check_digit_1);
        this.element.find('#amount').off().val(cheque.amount).priceFormat($.extend({}, this.priceFormat, { 'clearOnEmpty': 'true' }));
        if (cheque.check_digit_2 !== null && cheque.check_digit_2 !== '') {
            this.element.find('#check_digit_2').val(cheque.check_digit_2);
        }
        else {
            this.element.find('#check_digit_2').val('');
        }
    };
    /**
     * Calculate the Check Digit 1.
     *
     * @param cheque
     *
     * @return number
     */
    ClearingBatchCodelineGenerator.prototype.calculateCheckDigit1 = function (cheque) {
        return luhn_1.Luhn(cheque.trans_code + cheque.cheque_no + cheque.bank_code + cheque.cheque_account_no).toString();
    };
    return ClearingBatchCodelineGenerator;
}(rejected_documents_1.ClearingBatchRejectedDocuments));
exports.ClearingBatchCodelineGenerator = ClearingBatchCodelineGenerator;
var ClearingCustomQueryCodelineGenerator = /** @class */ (function (_super) {
    __extends(ClearingCustomQueryCodelineGenerator, _super);
    function ClearingCustomQueryCodelineGenerator(element, options, route) {
        var _this = _super.call(this, element, options, route) || this;
        _this.route = route;
        _this.bank_accounts = options.bank_accounts;
        _this.bankAccountEventHandler();
        return _this;
    }
    ClearingCustomQueryCodelineGenerator.prototype.bankAccountEventHandler = function () {
        var _this = this;
        this.element.find('#cheque_account_no').on('select2:select', function (e) {
            var data = e.params.data;
            if (data.text != undefined && _this.bank_accounts[data.text] != undefined) {
                _this.element.find('#trans_code').val(_this.bank_accounts[data.text].trans_code);
                _this.element.find('#bank_code').val(_this.bank_accounts[data.text].bank_code);
            }
            else {
                _this.element.find('#trans_code').val('');
                _this.element.find('#bank_code').val('');
            }
        });
    };
    ClearingCustomQueryCodelineGenerator.prototype.saveItem = function (index) {
        var _this = this;
        var cheque = this.cheques[index];
        var data = {
            trans_code: String(this.element.find('#trans_code').val()),
            cheque_no: String(this.element.find('#cheque_no').val()),
            bank_code: String(this.element.find('#bank_code').val()),
            cheque_account_no: String(this.element.find('#cheque_account_no :selected').text()),
            check_digit_1: String(this.element.find('#check_digit_1').val()),
            amount: String(this.element.find('#amount').unmask().trim()),
            check_digit_2: String(this.element.find('#check_digit_2').val())
        };
        cheque.trans_code = String(this.element.find('#trans_code').val());
        cheque.cheque_no = String(this.element.find('#cheque_no').val());
        cheque.bank_code = String(this.element.find('#bank_code').val());
        cheque.cheque_account_no = String(this.element.find('#cheque_account_no :selected').text());
        cheque.check_digit_1 = String(this.element.find('#check_digit_1').val());
        cheque.amount = String(this.element.find('#amount').unmask().trim());
        cheque.check_digit_2 = String(this.element.find('#check_digit_2').val());
        var validator = common_1.Parallax.getComponent('cheque-validator');
        if (!validator.hasValidTransCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_trans_code'));
            return false;
        }
        if (!validator.hasValidBankCode(data)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_bank_code'));
            return false;
        }
        if (!validator.hasValidChequeNoLength(data.cheque_no)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_cheque_no_length'));
            return false;
        }
        if (!validator.hasValidChequeAccountNoLength(data.cheque_account_no)) {
            toastr.error(validator.translator.get('reconciliation.validation.invalid_cheque_account_no_length'));
            return false;
        }
        data.check_digit_1 = cheque.check_digit_1 = this.calculateCheckDigit1(cheque);
        this.element.find('#check_digit_1').val(data.check_digit_1);
        //Validate the whole Check Digit 1 Code Line.
        if (!validator.hasValidCheckDigit1CodeLine(data)) {
            return false;
        }
        //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
        if (validator.isValidAmount(data.amount)) {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                if (!validator.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(validator.translator.get('reconciliation.validation.empty_check_digit_2'));
                    return false;
                }
                if (!validator.hasValidCheckDigit2CodeLine(data)) {
                    return false;
                }
            }
        }
        else {
            if (validator.isValidCheckDigit2(data.check_digit_2)) {
                toastr.error(validator.translator.get('reconciliation.validation.empty_amount'));
                return false;
            }
        }
        $.ajax({
            url: router_1.default.get(this.route, { item_id: cheque.id }),
            type: 'PUT',
            data: data,
            dataType: 'json'
        }).done(function (response) {
            if (response.return === true) {
                cheque.trans_code = data.trans_code;
                cheque.cheque_no = data.cheque_no;
                cheque.bank_code = data.bank_code;
                cheque.cheque_account_no = data.cheque_account_no;
                cheque.check_digit_1 = data.check_digit_1;
                cheque.amount = data.amount;
                cheque.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                _this.updateTableCell(cheque);
                toastr.success(response.message, '');
            }
            else {
                toastr.error(response.message, '');
            }
        }).fail(function () {
            toastr.options = { "showDuration": 0 };
            toastr.error('Unexpected AJAX call error on saveItem().', '');
        });
        return true;
    };
    /**
     * Calculate the Check Digit 1.
     *
     * @param cheque
     *
     * @return number
     */
    ClearingCustomQueryCodelineGenerator.prototype.calculateCheckDigit1 = function (cheque) {
        return luhn_1.Luhn(cheque.trans_code + cheque.cheque_no + cheque.bank_code + cheque.cheque_account_no).toString();
    };
    return ClearingCustomQueryCodelineGenerator;
}(rejected_documents_1.ClearingCustomQueryRejectedDocuments));
exports.ClearingCustomQueryCodelineGenerator = ClearingCustomQueryCodelineGenerator;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var signatures_1 = __webpack_require__(28);
var loader_1 = __webpack_require__(2);
var ISign = /** @class */ (function (_super) {
    __extends(ISign, _super);
    function ISign(router, route) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.route = route;
        return _this;
    }
    /**
     * Send a request to obtain the signatures.
     *
     * @param {TechnicalControl} technicalControl
     * @param {Cheque} cheque
     */
    ISign.prototype.sendRequest = function (technicalControl, cheque) {
        technicalControl.signature_ajax_flag = true;
        loader_1.Loader.show('Getting signatures');
        $.ajax({
            url: this.router.get(this.route, { cheque_account_no: cheque.cheque_account_no }),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                $('#signatures-carousel-container').html(data.html);
            }
            else {
                $('#signatures-carousel-container').html('');
                toastr.error(data.message, '');
            }
            loader_1.Loader.hide();
            technicalControl.signature_ajax_flag = false;
        }).fail(function () {
            loader_1.Loader.hide();
            technicalControl.signature_ajax_flag = false;
        });
    };
    return ISign;
}(signatures_1.Signatures));
module.exports = ISign;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Signatures = /** @class */ (function () {
    function Signatures() {
    }
    return Signatures;
}());
exports.Signatures = Signatures;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var signatures_1 = __webpack_require__(28);
var loader_1 = __webpack_require__(2);
var Ancoria = /** @class */ (function (_super) {
    __extends(Ancoria, _super);
    function Ancoria(router, route) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.route = route;
        return _this;
    }
    /**
     * Send a request to obtain the signatures.
     *
     * @param {TechnicalControl} technicalControl
     * @param {Cheque} cheque
     */
    Ancoria.prototype.sendRequest = function (technicalControl, cheque) {
        technicalControl.signature_ajax_flag = true;
        loader_1.Loader.show('Getting signatures');
        $.ajax({
            url: this.router.get(this.route, { cheque_account_no: cheque.cheque_account_no }),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                $('#signatures-carousel-container').html(data.html);
            }
            else {
                $('#signatures-carousel-container').html('');
                toastr.error(data.message, '');
            }
            loader_1.Loader.hide();
            technicalControl.signature_ajax_flag = false;
        }).fail(function () {
            loader_1.Loader.hide();
            technicalControl.signature_ajax_flag = false;
        });
    };
    return Ancoria;
}(signatures_1.Signatures));
module.exports = Ancoria;


/***/ })
]]);