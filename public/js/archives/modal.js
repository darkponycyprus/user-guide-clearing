$(document).ready(function () {
    $(document).on('click', '.advanced-search', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $('.simple-search-modal').modal('hide');
        $.ajax({
            context: this,
            url: Parallax.Route.get($(this).data('ajax-route')),
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                $('.advanced-search-modal .modal-content').html(data.content);
                $('#archive-builder').queryBuilder({
                    allow_empty: true,
                    sort_filters: false,
                    plugins: {
                        'bt-tooltip-errors': {delay: 100}
                    },
                    data: JSON.parse(data.archive)
                }).on('afterAddingDefaultValue.queryBuilder afterUpdateRuleValue.queryBuilder.filter', function(e, node) {
                    if (node.filter != undefined) {
                        if (node.filter.type === 'amount') {
                            node.$el.find('.rule-value-container input').priceFormat({
                                prefix: "€ ",
                                centsSeparator: '.',
                                thousandsSeparator: ',',
                                limit: 11,
                                centsLimit: 2,
                                clearOnEmpty: false
                            });
                        }
                        if (node.filter.type === 'date') {
                            node.$el.find('.rule-value-container input').inputmask("99/99/9999", {"placeholder": "dd/mm/yyyy"});
                        }
                        if (node.filter.type === 'datetime') {
                            node.$el.find('.rule-value-container input').inputmask("99/99/9999 99:99:99", {"placeholder": "dd/mm/yyyy hh:ii:ss"});
                        }
                    }
                }).on('beforeValidation.queryBuilder', function(e, node) {
                    //Because the amount is marked as a number before validation we need to unmask it.
                    if (node.filter != undefined && node.filter.type === 'amount') {
                        node.value = node.$el.find('.rule-value-container input').unmask();
                    }
                });
                if(data.filter_custom_query_json) {
                    $('#archive-builder').queryBuilder('setRules', JSON.parse(data.filter_custom_query_json));
                }
                $('.advanced-search-modal').modal('show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });

        $('.advanced-search-modal .reset').on('click', function () {
            $('#archive-builder').queryBuilder('reset');
        });
    }).on('click', '.simple-search', function (e) {
        e.preventDefault();
        $('.advanced-search-modal').modal('hide');
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get($(this).data('ajax-route')),
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                $('.simple-search-modal .modal-content').html(data.content);
                $('.simple-search-modal').modal('show');
                $('.fields-select-2').select2({
                    placeholder: "Any Field",
                    allowClear: true
                });
                $('.process-select-2').select2({
                    placeholder: "Any Process",
                    allowClear: true
                });
                $('.branch-select-2').select2({
                    placeholder: "Any Branch",
                    allowClear: true
                });
                $('.currencies-select-2').select2({
                    placeholder: "Any Currency",
                    allowClear: true
                });
                var datepickerOptions = {
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                    weekStart: 1,
                    maxViewMode: 0,
                    disabledDates: []
                };
                $('.simple-search-modal input[id*="-date"]').each(function (index, element) {
                    datepickerOptions.container = "#" + $(element).attr('id') + "-container";
                    $(element).on('show', fix_xeditable_conflict).datepicker(datepickerOptions).inputmask("99/99/9999", {"placeholder": "dd/mm/yyyy"});
                });
                $('#filter_amount').priceFormat({
                    prefix: "€ ",
                    centsSeparator: '.',
                    thousandsSeparator: ',',
                    limit: 11,
                    centsLimit: 2,
                    clearOnEmpty: true
                });
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('submit', '#simple-search-modal-form', function () {
        $('.simple-search-modal').find('input[name=filter_amount]').val(
            $('#filter_amount').unmask()
        );
    }).on('click', '#simple-search-modal-form .reset', function () {
        $('.fields-select-2').val(null).trigger('change');
        $('#simple-search-modal-form').find("input[type=text]").val("");
    }).on("click", ".advanced-search-modal button[type='submit']", function(e){
        //on submit form get json group rules and pass it to the controller to apply the custom query
        var rules = $('#archive-builder').queryBuilder('getRules', {
            get_flags: false,
            skip_empty: true
        });
        if (!rules) {
            return false;
        } else {
            $("input[name='filter_custom_query_json']").val(JSON.stringify(rules, undefined, 2));
        }
    }).on('change', "select[name='preset_filter_custom_query']", function(){
        $('#archive-builder').queryBuilder('setRules', JSON.parse($(this).val()));
    });
});
