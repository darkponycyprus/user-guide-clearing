$(document).ready(function () {
    new Parallax.Plugins.Image('#archive-document-image-box', {
        rotate: 'archive.image.rotate',
        swap: 'archive.image.swap'
    });
    $("#data-entry-box input[name='amount']").priceFormat({
        prefix: "€ ",
        centsSeparator: '.',
        thousandsSeparator: ',',
        limit: 11,
        centsLimit: 2
    });
    $('#update-archive-btn').on('click', function(e){
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            type: 'POST',
            url: Parallax.Route.get('archive.validate-codeline'),
            dataType: 'json',
            data: $("#update-archive-form input[name!='_method']").serialize()
        }).done(function (data) {
            if(data.valid){
                $('#update-archive-form').submit();
            }else{
                toastr.error(data.errors);
                $('#update-archive-form .errors-container').html("<p class='color-red'>"+data.errors+"</p>");
            }
            Parallax.Loader.hide();
        });
    });
});