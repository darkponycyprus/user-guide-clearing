$(document).ready(function () {
    var container = $('#create-files-edit-document-container');

    container.find('input[name=amount]').priceFormat({
        prefix: "€ ",
        centsSeparator: '.',
        thousandsSeparator: ',',
        insertPlusSign: false,
        allowNegative: false,
        limit: 11,
        centsLimit: 2
    });

    $('#edit-document-form').submit(function () {
        container.find('input[name=amount]').val(container.find('input[name=amount]').unmask().trim());
    });
});