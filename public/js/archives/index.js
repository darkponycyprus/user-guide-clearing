$(document).ready(function () {
    new Parallax.Plugins.Image('#archive-document-image-box', {
        rotate: 'archive.image.rotate',
        swap: 'archive.image.swap'
    });

    var keys = Parallax.Plugins.Keys.init('#archive-documents-list');

    $(document).on('click', 'tr[data-item-id] td:not(".delete-item"):not(".commands-td"):not(".checkbox-td")', function (e) {
        keys.off();
        $(this).closest('tbody').find('tr').removeClass('bg-grey active');
        $(this).parent().addClass('bg-grey active');
        keys.currentIndex($(this).parent().attr('tabindex'));
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            url: Parallax.Route.get('archive.image', {item_id: $(this).parent().data('item-id')}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                var imgbox = $('#archive-document-image-box');
                imgbox.find('.image-manipulation-main').attr('src', data.front).attr('data-image-id', data.id).attr('data-orientation', 'front').attr('data-front-image', data.front).attr('data-back-image', data.back);
                keys.on();
            } else {
                keys.on();
                toastr.error(data.message,'');
            }
            Parallax.Loader.hide();
        });
    });

    $('.bulk-actions-menu a').on("click", function(e){
        var stringids = '';
        e.preventDefault();
        $("input[name='export_type']").val($(this).data('export-type'));
        $('.checkbox-td input').each(function(id, val){
            if($(val).prop('checked')){
                if(stringids == ''){
                    stringids += $(val).val();
                }else{
                    stringids += ','+$(val).val();
                }
            }
        });
        $("input[name='bulk_ids']").val(stringids);
        $('#archives-bulk-form').submit();
    });
});
