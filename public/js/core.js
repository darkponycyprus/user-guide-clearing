$(document).ready(function () {
    var windowHeight = $(window).height();

    setupSelect2();

    stretchToBottom();

    handleSingleCheckboxSelectAll();

    handleSortableThClick();

    handlePagePostRequest();

    $(".draggable-modal").draggable({
        handle: ".modal-header"
    });

    $('body').on('click', '[data-action]', function() {
        var buttonClicked = $(this);

        switch($(this).data('action')) {
            case 'delete':
                swal({
                    title: 'Are you sure?',
                    text: $(this).data('message'),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes'
                }).then(function(result) {
                    if (result.value) {
                        buttonClicked.closest('form').submit();
                    }
                });
                break;
        }
    });

    //handle responsive menu hovers when li is at the bottom
    if($(window).width() <= 1025) {
        $('.sidebar-content > .sidebar-nav > ul > .menu-li').on('mouseover', function(){
            var subnav = $(this).children('.sidebar-subnav');
            var subnavHeight = $(subnav).height();
            var liOffset = $(this).offset();

            if(windowHeight <= (subnavHeight + liOffset.top)){
                $(subnav).attr('style', 'margin-top: -' + (subnavHeight + liOffset.top - windowHeight) + 'px');
            }
        });
    }
});

$(window).resize(function () {
    stretchToBottom();
});

//helper function for dasha ui datepickers
function fix_xeditable_conflict() {
    $('.datepicker > div:first-of-type').css('display', 'initial');
}

function setupSelect2(){
    try {
        $('.select-2').select2({
            dropdownAutoWidth: 'true'
        });
        $('.select-2-wo-search').select2({
            dropdownAutoWidth: 'true',
            minimumResultsForSearch: -1
        });
    }
    catch(err) {
        console.log('error core js: '+err);
    }
}

//stretch cardboxes to bottom
function stretchToBottom() {
    var stretchElement = $('.stretch-to-bottom');
    var stretchTableElement = $('.stretch-cheques-table-to-bottom');
    var windowHeight = $(window).height();

    if(stretchElement.length > 0) {
      $('.stretch-to-bottom').each(function(id, element){
        var inlineStyle = 'overflow-y: auto;';
        if($(element).data('overflow-x') != undefined){
            inlineStyle += 'overflow-x: '+$(element).data('overflow-x')+';';
        }else{
          inlineStyle += 'overflow-x: hidden;';
        }

        var height = windowHeight - $(element).offset().top - 20;
        if(height > 130){
          $(element).attr('style', inlineStyle + ' max-height:' + height + 'px;');
        }else{
          $(element).attr('style', inlineStyle + ' max-height: initial;');
        }
      });
    }

    // if(stretchTableElement.length > 0) {
    //   var height = windowHeight - stretchTableElement.offset().top - 55;
    //   console.log(height);
    //   if(height > 100){
    //     stretchTableElement.attr('style', 'max-height:' + height + 'px;');
    //   }else{
    //     stretchTableElement.attr('style', 'max-height: initial;');
    //   }
    //     //stretchTableElement.attr('style', 'min-height:' + (windowHeight - stretchTableElement.offset().top - 70) + 'px;max-height:' + (windowHeight - stretchTableElement.offset().top - 70) + 'px;');
    // }
}

//make a POST request on pagination link click instad of GET(reconciliation session custom query)
function handlePagePostRequest(){
    $('body').on('click', '.pagination-post-links a', function (e) {
        e.preventDefault();

        //try to get page from href url
        var href = $(this).attr('href');

        var explodedHref = href.split('?page=');
        if(explodedHref.length > 1){
            explodedHref = explodedHref[1].split('&');//in case there are other get variables in the url
            if(explodedHref.length > 0){
                var page = parseInt(explodedHref[0]);
                $("input[name='page']").val(page);

                //submit form
                var closestForm = $(this).closest('form');
                if(closestForm.length > 0){
                    closestForm.submit();
                }
            }
        }
    });
}

function handleSortableThClick(){
    $('body').on('click', '.sortable-th', function (e) {
        e.preventDefault();

        var dbColumnName = $(this).data('column-name'),
        orderDirection = $(this).data('order-direction');

        $("input[name='filter_order_column']").val(dbColumnName);
        $("input[name='filter_order_direction']").val(orderDirection);
        $("input[name='delete']").val("");

        var closestForm = $(this).closest('form');
        if(closestForm.length > 0){
            closestForm.submit();
        }else{
            $('#archives-index-form').submit();
        }
    });
}

function handleSingleCheckboxSelectAll(){
    $('body').on("click", '.checkbox-select-all', function(){
        var state = $(this).prop('checked');
        var checkboxesContainer = $(this).data('for');

        $(checkboxesContainer+' input[type=checkbox]').each(function( index, value ) {
            $(this).prop('checked', state);
        });
    });
}

function showFullScreen(message)
{
    message = message || '';
    $('#full-screen-message').text(message);
    $('#full-screen-loading').addClass('open');
}

function hideFullScreen()
{
    $('#full-screen-loading').removeClass('open');
    $('#full-screen-message').text('');
}

function getCurrentDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();

    return yyyy+'-'+mm+'-'+dd;
}
