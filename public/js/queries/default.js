$(document).ready(function () {
    $(document).on('click', '#query-btn', function (e) {
        e.preventDefault();
        var rules = $('#query-builder').queryBuilder('getRules', {
            get_flags: false,
            skip_empty: true,
            plugins: {
                'bt-tooltip-errors': {delay: 100}
            }
        });
        if (!rules) {
            return false;
        } else {
            $("input[name='custom-query']").val(JSON.stringify(rules, undefined, 2));
            $('#query-form').submit();
        }
    });
    $('#processes').select2({
        placeholder: "Select Process",
        allowClear: true,
        multiple: true
    });
});