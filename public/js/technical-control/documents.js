$(document).ready(function () {
    var technical_control = $('#technical-control-branch');
    var url = '';
    new Parallax.Plugins.Image('#technical-control-branch');
    if(technical_control.data('branch-id')) {
        url = Parallax.Route.get('technical-control.branch.documents', {code: technical_control.data('branch-id')});
    } else {
        url = Parallax.Route.get('technical-control.documents');
    }
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {'filter_search': technical_control.find('input[name="filter_search"]').val()}
    }).done(function (data) {
        if (data.return == true) {
            new Parallax.Clearing.TechnicalControl('#technical-control-branch', {
                items: data.items,
                technicalControlStatuses: data.technicalControlStatuses,
                priceFormat: {
                    prefix: "€ ",
                    centsSeparator: '.',
                    thousandsSeparator: ',',
                    limit: 11,
                    centsLimit: 2
                }
            }, 'technical-control.archives.save');
        } else {
            toastr.error(data.message, '');
        }
    });

    $('body').on('click', '#signatures-container img, #signatures-carousel-container img', function(){
        $('#zoomed-img-container img').attr('src', $(this).attr('src'));
        $('#zoomed-img-container').attr('style', "background-color: rgba(0,0,0,0.2);left:0;bottom: 0;right: 0;top: 0;z-index: 10;");
    }).on('click', '#zoomed-img-close', function(e){
        e.preventDefault();
        $('#zoomed-img-container').attr('style', "background-color: rgba(0,0,0,0.2);left:0;bottom: 0;right: 0;top: 0;z-index: -10;");
    });
});
