$(document).ready(function () {
    $(document).on('click', '#end-of-session', function (e) {
        Parallax.Loader.show();
        e.preventDefault();
        $.ajax({
            url: Parallax.Route.get('technical-control.end-of-session'),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    });
});