$(document).ready(function () {
    $(document).on('click', '#send-to-banking-system', function (e) {
        Parallax.Loader.show();
        e.preventDefault();
        $.ajax({
            url: Parallax.Route.get('technical-control.send-to-banking-system'),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    });
});