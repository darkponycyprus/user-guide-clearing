"use strict";
var Luhn = /** @class */ (function () {
    function Luhn() {
    }
    Luhn.calculateLuhn = function (originalStr) {
        var sum = 0, delta = [0, 1, 2, 3, 4, -4, -3, -2, -1, 0];
        for (var i = 0; i < originalStr.length; i++) {
            sum += parseInt(originalStr.substring(i, i + 1));
        }
        for (var i = originalStr.length - 1; i >= 0; i -= 2) {
            sum += delta[parseInt(originalStr.substring(i, i + 1))];
        }
        if (10 - (sum % 10) === 10) {
            return 0;
        }
        return 10 - (sum % 10);
    };
    return Luhn;
}());
