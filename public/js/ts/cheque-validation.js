"use strict";
var ChequeValidation = /** @class */ (function () {
    function ChequeValidation(options, messages, configurations) {
        this.options = options;
        this.messages = messages;
        this.configurations = configurations;
    }
    /**
     * Check if the cheque is valid.
     *
     * @param cheque
     * @returns {boolean}
     */
    ChequeValidation.prototype.isValid = function (cheque) {
        return (!this.checkDigit1CodeLineRules(cheque) || !this.checkDigit2CodeLineRules(cheque));
    };
    /**
     * Check if it has a valid trans code.
     *
     * @param cheque
     * @returns {boolean}
     */
    ChequeValidation.prototype.hasValidTransCode = function (cheque) {
        return this.inArray(cheque.trans_code, this.options.cheque_validation_rules.trans_codes);
    };
    /**
     * Check if it has a valid bank code.
     *
     * @param cheque
     * @returns {boolean}
     */
    ChequeValidation.prototype.hasValidBankCode = function (cheque) {
        return this.inArray(cheque.bank_code, this.options.valid_banks);
    };
    /**
     * Check if the code line until check digit 1 is valid.
     *
     * @param cheque
     * @returns {boolean}
     */
    ChequeValidation.prototype.hasValidCheckDigit1CodeLine = function (cheque) {
        return this.checkDigit1CodeLineRules(cheque);
    };
    /**
     * Check if the code line until check digit 2 is valid.
     *
     * @param item
     * @returns {boolean}
     */
    ChequeValidation.prototype.hasValidCheckDigit2CodeLine = function (item) {
        return this.checkDigit2CodeLineRules(item);
    };
    /**
     * Set the rules for the code line until check digit 1.
     *
     * @param cheque
     */
    ChequeValidation.prototype.checkDigit1CodeLineRules = function (cheque) {
        if (!this.isValidTrasCode(cheque.trans_code)) {
            toastr.error(this.messages.reconciliation.validation.empty_trans_code);
            return false;
        }
        else {
            if (!this.isNumber(cheque.trans_code)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_trans_code);
                return false;
            }
        }
        if (!this.isValidChequeNo(cheque.cheque_no)) {
            toastr.error(this.messages.reconciliation.validation.empty_cheque_no);
            return false;
        }
        else {
            if (!this.isNumber(cheque.cheque_no)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_cheque_no);
                return false;
            }
        }
        if (!this.isValidBankCode(cheque.bank_code)) {
            toastr.error(this.messages.reconciliation.validation.empty_bank_code);
            return false;
        }
        else {
            if (!this.isNumber(cheque.bank_code)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_bank_code);
                return false;
            }
        }
        if (!this.isValidChequeAccountNo(cheque.cheque_account_no)) {
            toastr.error(this.messages.reconciliation.validation.empty_cheque_account_no);
            return false;
        }
        else {
            if (!this.isNumber(cheque.cheque_account_no)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_cheque_account_no);
                return false;
            }
        }
        if (!this.isValidCheckDigit1(cheque.check_digit_1)) {
            toastr.error(this.messages.reconciliation.validation.empty_check_digit_1);
            return false;
        }
        else {
            if (!this.isNumber(cheque.check_digit_1)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_check_digit_1);
                return false;
            }
        }
        if (!this.isValidCheckDigit1Luhn(cheque.trans_code, cheque.cheque_no, cheque.bank_code, cheque.cheque_account_no, cheque.check_digit_1)) {
            toastr.error(this.messages.reconciliation.validation.wrong_check_digit_1);
            return false;
        }
        return true;
    };
    /**
     * Set the rules for the code line until check digit 2.
     *
     * @param cheque
     */
    ChequeValidation.prototype.checkDigit2CodeLineRules = function (cheque) {
        //Check if the amount is null, empty string or a negative value.
        if (!this.isValidAmount(cheque.amount)) {
            toastr.error(this.messages.reconciliation.validation.empty_amount);
            return false;
        }
        else {
            if (!this.isNumber(cheque.amount)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_amount);
                return false;
            }
        }
        if (!this.isValidCheckDigit2(cheque.check_digit_2)) {
            toastr.error(this.messages.reconciliation.validation.empty_check_digit_2);
            return false;
        }
        else {
            if (!this.isNumber(cheque.check_digit_2)) {
                toastr.error(this.messages.reconciliation.validation.not_numeric_check_digit_2);
                return false;
            }
        }
        if (!this.isValidCheckDigit2Luhn(cheque.amount, cheque.check_digit_2)) {
            toastr.error(this.messages.reconciliation.validation.wrong_check_digit_2);
            return false;
        }
        return true;
    };
    /**
     * Calculate and check the check digit 1 Luhn.
     *
     * @param trans_code
     * @param cheque_no
     * @param bank_code
     * @param cheque_account_no
     * @param check_digit_1
     * @returns {boolean}
     */
    ChequeValidation.prototype.isValidCheckDigit1Luhn = function (trans_code, cheque_no, bank_code, cheque_account_no, check_digit_1) {
        if (Luhn.calculateLuhn(this.pad(trans_code, this.options.paddings.trans_code) +
            this.pad(cheque_no, this.options.paddings.cheque_no) +
            this.pad(bank_code, this.options.paddings.bank_code) +
            this.pad(cheque_account_no, this.options.paddings.cheque_account_no)) !== parseInt(check_digit_1)) {
            return false;
        }
        return true;
    };
    /**
     * Calculate and check the check digit 2 Luhn.
     *
     * @param amount
     * @param check_digit_2
     * @returns {boolean}
     */
    ChequeValidation.prototype.isValidCheckDigit2Luhn = function (amount, check_digit_2) {
        if (Luhn.calculateLuhn(this.pad(amount, this.options.paddings.amount)) !== parseInt(check_digit_2)) {
            return false;
        }
        return true;
    };
    //Check if the Trans Code is null or an empty string.
    ChequeValidation.prototype.isValidTrasCode = function (trans_code) {
        if (trans_code === null || trans_code === '') {
            return false;
        }
        return true;
    };
    //Check if the Cheque No is null or an empty string.
    ChequeValidation.prototype.isValidChequeNo = function (cheque_no) {
        if (cheque_no === null || cheque_no === '') {
            return false;
        }
        return true;
    };
    //Check if the Bank Code is null or an empty string.
    ChequeValidation.prototype.isValidBankCode = function (bank_code) {
        if (bank_code === null || bank_code === '') {
            return false;
        }
        return true;
    };
    //Check if the Cheque Account No is null or an empty string.
    ChequeValidation.prototype.isValidChequeAccountNo = function (cheque_account_no) {
        if (cheque_account_no === null || cheque_account_no === '') {
            return false;
        }
        return true;
    };
    //Check if the Check Digit 1 is null or an empty string and if the Check Digit 1 has any other number except between 0 and 9.
    ChequeValidation.prototype.isValidCheckDigit1 = function (cd1) {
        if (cd1 === null || cd1 === '' || parseInt(cd1) < 0 || parseInt(cd1) > 9) {
            return false;
        }
        return true;
    };
    //Check if the Amount is null, an empty string or has a negative value.
    ChequeValidation.prototype.isValidAmount = function (amount) {
        if (amount === null || amount === '' || amount <= 0) {
            return false;
        }
        return true;
    };
    //Check if the Check Digit 2 is null or an empty string and if the Check Digit 2 has any other number except between 0 and 9.
    ChequeValidation.prototype.isValidCheckDigit2 = function (cd2) {
        if (cd2 === null || cd2 === '' || parseInt(cd2) < 0 || parseInt(cd2) > 9) {
            return false;
        }
        return true;
    };
    //Check if the string is not a number.
    ChequeValidation.prototype.isNumber = function (string) {
        if (isNaN(string)) {
            return false;
        }
        return true;
    };
    //Check if the deposit date is correct.
    ChequeValidation.prototype.canDeposit = function (date) {
        var deposit_date = moment.utc(date, 'DD/MM/YYYY', true);
        if (!this.isValidDate(deposit_date)) {
            return false;
        }
        var today = moment.utc(new Date());
        var duration = moment.duration(today.diff(deposit_date));
        if (duration.asMonths() > parseFloat(this.configurations.js_validations.max_deposit_date_months)) {
            return false;
        }
        return true;
    };
    //Check if the date is correct.
    ChequeValidation.prototype.isValidDate = function (date_string) {
        var date = moment.utc(date_string, 'DD/MM/YYYY', true);
        return date.isValid();
    };
    /**
     * Padding left with "0"
     *
     * @param str
     * @param max
     * @returns {*}
     */
    ChequeValidation.prototype.pad = function (str, max) {
        str = str.toString();
        return str.length < max ? this.pad("0" + str, max) : str;
    };
    ChequeValidation.prototype.inArray = function (needle, haystack) {
        var count = haystack.length;
        for (var i = 0; i < count; i++) {
            if (haystack[i] === needle) {
                return true;
            }
        }
        return false;
    };
    return ChequeValidation;
}());
