$(document).ready(function () {
    //handle ajax calls in institutes index eccs switch
    $('body').on('click', ".assing-scanner-to-branch-user-checkbox", function() {
        Parallax.Loader.show();

        var statesMap = {"true": 1, "false": 0};
        var state = $(this).prop("checked");
        var scannerId = $(this).data("scanner-id");
        var branchUserId = $(this).data("branch-user-id");

        var sdata = {'state': statesMap[state], 'scannerId': scannerId, 'branchUserId': branchUserId};
        $.ajax({ url: Parallax.Route.get('scanners.assign.branch_user'), type: "post",
            data: sdata,
            success: function(data) {
                if(data === true){
                    toastr.options = {"closeButton":true,"debug":false,"newestOnTop":true,"progressBar":true,"positionClass":"toast-top-right","preventDuplicates":true,"onclick":null,"showDuration":"1000","hideDuration":"2000","timeOut":"5000","extendedTimeOut":"5000","showEasing":"swing","hideEasing":"linear","showMethod":"fadeIn","hideMethod":"fadeOut"};
                    if(statesMap[state] == 1){
                        toastr.success(Parallax.Lang.get('scanner_assign_success',''));
                    }else{
                        toastr.success(Parallax.Lang.get('scanner_unassign_success',''));
                    }
                }else{
                    toastr.error(Parallax.Lang.get('scanner_assign_error',''));
                }
                Parallax.Loader.hide();
            }
        });
    });
});
