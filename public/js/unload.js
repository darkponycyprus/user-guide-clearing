$(document).ready(function () {
    var formdata = $('form.unload-check').find('input, textarea, select').not(".ignore-unload").serialize();
    $(window).on('beforeunload', function (e) {
        if (formdata !== $('form.unload-check').find('input, textarea, select').not(".ignore-unload").serialize()) {
            return true;
        }
    });
    // Form Submit
    $(document).on("submit", "form", function (event) {
        // disable warning
        $(window).off('beforeunload');
    });
});