$(document).ready(function () {
    $(document).on('change', '#filter_limit, #filter_batch', function (e) {
        e.preventDefault();
        $('#reconciliation-form input[name="filter_batch"]').val($('#filter_batch').val());
        if($('#filter_limit').length) {
            $('#reconciliation-form input[name="filter_limit"]').val($('#filter_limit').val());
        } else {
            $('#reconciliation-form input[name="filter_limit"]').attr('disabled', 'disabled');
        }
        $("#reconciliation-form").submit();
    }).on('change', '#filter_custom_query', function (e) {
        e.preventDefault();
        if($(this).val() !== '') {
            $('#custom-query-form input[name="filter_custom_query"]').val($(this).val());
            $("#custom-query-form").submit();
        }
    }).on('click', '.edit-batch', function (e) {
        var element = $(this);
        var status = element.data('status');
        var batch_id = element.data('batch-id');
        var show_route = element.data('show-route');

        var current_user_id = element.data('current-user-id');
        var in_use_by_id = element.data('in-use-by-id');
        if(status === 2 && current_user_id !== in_use_by_id ) {
            e.preventDefault();
            swal({
                title: 'Batch In Use',
                html: 'The batch is currently in use by another user.',
                footer: '<button id="swal-close" type="button" class="btn btn-danger mr-2">Close</button>' +
                '<a href="' + show_route +'" class="btn btn-info mr-2"">View Only</a>' +
                '<button id="take-over" type="button" data-batch-id="' + batch_id + '" class="btn btn-warning">Take Over</button>',
                type: 'question',
                showCancelButton: false,
                showConfirmButton: false,
            })
        }
    }).on('click', '#take-over', function (e) {
        var element = $(this);
        var batch_id = element.data('batch-id');
        Parallax.Loader.show();
        $.ajax({
            url: Parallax.Route.get('reconciliation.batch.take-over', {batch_id: batch_id}),
            type: 'PUT',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                window.location.href = data.redirect;
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '#swal-close', function (e) {
        swal.close();
    })
});