$(document).ready(function () {
    $(document).on('click', '#validate-session', function (e) {
        e.preventDefault();
        handleValidation(Parallax.Route.get('reconciliation.session.validation', {session_id: $(this).data('session-id')}));
    });
    $(document).on('click', '#end-of-session', function (e) {
        e.preventDefault();
        handleValidation(Parallax.Route.get('reconciliation.session.validate-end', {session_id: $(this).data('session-id')}));
    });

    function handleValidation(route){
        Parallax.Loader.show();
        $.ajax({
            url: route,
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                Parallax.getComponent('asyncProgressBarWrapper').createProgressDialog(data.item);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }
});
