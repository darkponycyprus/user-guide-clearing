Images = {};
$(document).ready(function () {

    new Parallax.Plugins.Image('#reconciliation-batch-documents-image-box');
    new Parallax.Plugins.Image('#data-entry-modal');
    new Parallax.Plugins.Image('#correct-rejected-modal');
    new Parallax.Plugins.Image('#edit-modal');
    new Parallax.Plugins.Image('#codeline-generator-modal');

    //if an inst is added or edited and the current switch turns to true inform that we can only have one current
    $('body').on('click', "#reconciliation-batch-drawer input[name='delete']", function() {
        if($(this).prop("checked")){
            swal({
                title: Parallax.Lang.get('batch.delete_batch_title'),
                html: Parallax.Lang.get('batch.delete_batch_msg'),
                type: 'warning',
                reverseButtons: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: Parallax.Lang.get('buttons.yes'),
                cancelButtonText: Parallax.Lang.get('buttons.no')
            }).then((result) => {
                if (result.dismiss === "cancel") {
                    $(this).prop("checked", false);
                }
            });
        }
    }).on('click', "#reconciliation-batch-drawer input[name='restore']", function() {
        if($(this).prop("checked")){
            swal({
                title: Parallax.Lang.get('batch.restore_batch_title'),
                html: Parallax.Lang.get('batch.restore_batch_msg'),
                type: 'info',
                reverseButtons: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: Parallax.Lang.get('buttons.yes'),
                cancelButtonText: Parallax.Lang.get('buttons.no')
            }).then((result) => {
                if (result.dismiss === "cancel") {
                    $(this).prop("checked", false);
                }
            });
        }
    });

    var keys = Parallax.Plugins.Keys.init('#reconciliation-batch-documents-list');

    $(document).on('click', 'tr[data-item-id] td', function (e) {
        keys.off();
        $(this).closest('tbody').find('tr').removeClass('bg-grey active');
        $(this).parent().addClass('bg-grey active');
        keys.currentIndex($(this).parent().attr('tabindex'));
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            url: Parallax.Route.get('scan.item.image', {item_id: $(this).parent().data('item-id')}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('#reconciliation-batch-documents-image-box')
                    .find('.image-manipulation-main')
                    .attr('src', data.front)
                    .attr('data-image-id', data.id)
                    .attr('data-orientation', 'front')
                    .attr('data-front-image', data.front)
                    .attr('data-back-image', data.back);
                keys.on();
            } else {
                keys.on();
                toastr.error(data.message,'');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '.delete-item input[type="checkbox"]', function (e) {
        e.stopPropagation();
        Parallax.Loader.show();
        var checkbox = $(this);
        if(checkbox.is(':checked')) {
            // Delete the item
            $.ajax({
                url: Parallax.Route.get('scan.item.destroy', {item_id: checkbox.val()}),
                type: 'DELETE',
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    $('#reconciliation-batch-documents-general-info-box').html(data.html);
                    toastr.success(data.message,'');
                } else {
                    checkbox.prop('checked', false);
                    toastr.error(data.message,'');
                }
                Parallax.Loader.hide();
            });
        } else {
            // Restore the item
            $.ajax({
                url: Parallax.Route.get('scan.item.restore', {item_id: checkbox.val()}),
                type: 'PATCH',
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    $('#reconciliation-batch-documents-general-info-box').html(data.html);
                    toastr.success(data.message,'');
                } else {
                    checkbox.prop('checked', true);
                    toastr.error(data.message,'');
                }
                Parallax.Loader.hide();
            });
        }
    });
});