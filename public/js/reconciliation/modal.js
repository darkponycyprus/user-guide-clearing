$(document).ready(function () {
    $(document).on('click', '#reconciliation-advanced-search', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            method: 'GET',
            url: $(this).data('ajax-route'),
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                $('.reconciliation-advanced-search-modal .modal-content').html(data.html);
                $('#reconciliation-builder').queryBuilder({
                    allow_empty: true,
                    sort_filters: false,
                    plugins: {
                        'bt-tooltip-errors': {delay: 100}
                    },
                    data: JSON.parse(data.data)
                }).on('afterAddingDefaultValue.queryBuilder afterUpdateRuleValue.queryBuilder.filter', function(e, node) {
                    if (node.filter != undefined) {
                        if (node.filter.type === 'amount') {
                            node.$el.find('.rule-value-container input').priceFormat({
                                prefix: "€ ",
                                centsSeparator: '.',
                                thousandsSeparator: ',',
                                limit: 11,
                                centsLimit: 2,
                                clearOnEmpty: false
                            });
                        }
                        if (node.filter.type === 'date') {
                            node.$el.find('.rule-value-container input').inputmask("99/99/9999", {"placeholder": "dd/mm/yyyy"});
                        }
                        if (node.filter.type === 'datetime') {
                            node.$el.find('.rule-value-container input').inputmask("99/99/9999 99:99:99", {"placeholder": "dd/mm/yyyy hh:ii:ss"});
                        }
                    }
                }).on('beforeValidation.queryBuilder', function(e, node) {
                    //Because the amount is marked as a number before validation we need to unmask it.
                    if (node.filter != undefined && node.filter.type === 'amount') {
                        node.value = node.$el.find('.rule-value-container input').unmask();
                    }
                });
                if(data.filter_custom_query_json) {
                    $('#reconciliation-builder').queryBuilder('setRules', JSON.parse(data.filter_custom_query_json));
                }
                $('.reconciliation-advanced-search-modal').modal('show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });

        $('.reconciliation-advanced-search-modal .reset').on('click', function () {
            $('#reconciliation-builder').queryBuilder('reset');
        });
    }).on('click', '.reconciliation-advanced-search-modal button[type="submit"]', function(e){
        //on submit form get json group rules and pass it to the controller to apply the custom query
        var rules = $('#reconciliation-builder').queryBuilder('getRules', {
            get_flags: false,
            skip_empty: true
        });
        if (!rules) {
            return false;
        } else {
            $("input[name='filter_custom_query_json']").val(JSON.stringify(rules, undefined, 2));
        }
    }).on('click', '.reconciliation-session-selection', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            url: $(this).data('ajax-route'),
            method: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                $('.reconciliation-session-selection-modal .modal-content').html(data.html);
                $('.reconciliation-session-selection-modal').modal('show');
                $('.reconciliation-session-selection-modal .select-2').select2();
                $('#reconciliations-calendar-picker').on('show', fix_xeditable_conflict)
                    .datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        weekStart: 1,
                        daysOfWeekDisabled: '06',
                        endDate: '0d',
                        maxViewMode: 0,
                        disabledDates: [],
                        container: '.reconciliations-calendar-picker-col'
                    });
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('change', '.reconciliation-session-selection-modal [name="filter_branch"]', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $('[name="filter_date"]').html("<option value=''>Date</option>");
        var branch_id = ($('[name="filter_branch"]').val()) ? $('[name="filter_branch"]').val() : null;
        if(branch_id != null) {
            $.ajax({
                method: 'GET',
                url: $(this).data('ajax-route'),
                data: {'filter_branch': branch_id},
                dataType: 'json'
            }).done(function (data) {
                if (data.return === true) {
                    $.each(data.sessions, function(id, val){
                        $('[name="filter_date"]').append("<option value='"+val+"'>"+val+"</option>");
                    });
                    $('.reconciliation-session-selection-modal .select-2').select2();
                }
                Parallax.Loader.hide();
            });
        } else {
            Parallax.Loader.hide();
        }
    }).on('change', '[name="calendar_date"]', function(e){
        e.preventDefault();
        $('[name="filter_date"]').append("<option selected='selected' value='"+$(this).val()+"'>"+$(this).val()+"</option>");
        $('.reconciliation-session-selection-modal .select-2').select2();
    }).on('click', '.reconciliations-calendar-picker-col .fa-calendar-o', function(){
        $('#reconciliations-calendar-picker').on('show', fix_xeditable_conflict)
            .datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                weekStart: 1,
                daysOfWeekDisabled: '06',
                endDate: '0d',
                maxViewMode: 0,
                disabledDates: [],
                container: '.reconciliations-calendar-picker-col'
            });
        $('[name="calendar_date"]').focus();
    });
});