var Images = {};
$(document).ready(function () {
    new Parallax.Plugins.Image('#reconciliation-filter-documents-image-box');
    new Parallax.Plugins.Image('#data-entry-modal');
    new Parallax.Plugins.Image('#correct-rejected-modal');
    new Parallax.Plugins.Image('#edit-modal');
    new Parallax.Plugins.Image('#codeline-generator-modal');

    //gia otan kaneis sort ton pinaka gia na mh gamithout ta keyboard shortcuts
    var itemsTable = document.querySelector('#reconciliation-filter-documents-list table')
    itemsTable.addEventListener('Sortable.sorted', function(){
        refreshTableRowsIndex();
    });

    function refreshTableRowsIndex(){
        var step = 0;
        $('#reconciliation-filter-documents-list table tr').each(function(id, tr){
            $(tr).attr('tabindex', step);
            step++;
        });
    }

    $(document).on('click', 'tr[data-item-id] td', function (e) {
        keys.off();
        $(this).closest('tbody').find('tr').removeClass('bg-grey active');
        $(this).parent().addClass('bg-grey active');
        keys.currentIndex($(this).parent().attr('tabindex'));
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            url: Parallax.Route.get('scan.item.image', {item_id: $(this).parent().data('item-id')}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                var imgbox = $('#reconciliation-filter-documents-image-box');
                imgbox.find('.image-manipulation-main')
                    .attr('src', data.front)
                    .attr('data-image-id', data.id)
                    .attr('data-orientation', 'front')
                    .attr('data-front-image', data.front)
                    .attr('data-back-image', data.back);
                keys.on();
            } else {
                keys.on();
                toastr.error(data.message,'');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '.delete-item input[type="checkbox"]', function (e) {
        e.stopPropagation();
        Parallax.Loader.show();
        var checkbox = $(this);
        if(checkbox.is(':checked')) {
            // Delete the item
            $.ajax({
                url: Parallax.Route.get('scan.item.destroy', {item_id: checkbox.val()}),
                type: 'DELETE',
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    toastr.success(data.message,'');
                } else {
                    checkbox.prop('checked', false);
                    toastr.error(data.message,'');
                }
                Parallax.Loader.hide();
            });
        } else {
            // Restore the item
            $.ajax({
                url: Parallax.Route.get('scan.item.restore', {item_id: checkbox.val()}),
                type: 'PATCH',
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    toastr.success(data.message,'');
                } else {
                    checkbox.prop('checked', true);
                    toastr.error(data.message,'');
                }
                Parallax.Loader.hide();
            });
        }
    });
});