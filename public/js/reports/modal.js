$(document).ready(function () {
    $(document).on('click', '.reports-session-selection', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: $(this).data('ajax-route'),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.reports-session-selection-modal .modal-content').html(data.html);
                $('.reports-session-selection-modal').modal('show');
                $('.reports-session-selection-modal .select-2').select2();
                $('#report-datepicker').on('show', fix_xeditable_conflict)
                    .datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        weekStart: 1,
                        daysOfWeekDisabled: '06',
                        endDate: '0d',
                        maxViewMode: 0,
                        disabledDates: [

                        ],
                        container: '#report-datepicker-container'
                    });
            }
            Parallax.Loader.hide();
        });
    }).on('change', '.reports-session-selection-modal [name="filter_branch"]', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $('[name="filter_date"]').html("<option value=''>Date</option>");
        var branch_id = ($('[name="filter_branch"]').val()) ? $('[name="filter_branch"]').val() : null;
        if(branch_id != null) {
            $.ajax({
                context: this,
                method: 'GET',
                url: $(this).data('ajax-route'),
                data: {'filter_branch': branch_id},
                dataType: 'json'
            }).done(function (data) {
                if (data.return === true) {
                    $.each(data.sessions, function(id, val){
                        $('[name="filter_date"]').append("<option value='"+val+"'>"+val+"</option>");
                    });
                    $('.reports-session-selection-modal .select-2').select2();
                }
                Parallax.Loader.hide();
            });
        } else {
            Parallax.Loader.hide();
        }
    }).on('change', '[name="calendar_date"]', function(e){
        e.preventDefault();
        $('[name="filter_date"]').append("<option selected='selected' value='"+$(this).val()+"'>"+$(this).val()+"</option>");
        $('.reports-session-selection-modal .select-2').select2();
    }).on('click', '#report-datepicker-container .fa-calendar-o', function(){
        $('#reports-calendar-picker').on('show', fix_xeditable_conflict)
            .datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                weekStart: 1,
                daysOfWeekDisabled: '06',
                endDate: '0d',
                maxViewMode: 0,
                disabledDates: [],
                container: '#report-datepicker-container'
            });
        $('[name="calendar_date"]').focus();
    });
});
