$(document).ready(function () {

    qualitySlider();

    $('#image_type').on('change', function () {
        qualitySlider();
    });
});

function qualitySlider() {
    var type = $('#image_type').val();
    $('.ui-slider').addClass('d-none');
    $('#quality-value').val('');
    if(type === '') {
        $('#quality').addClass('d-none');
    } else {
        var element = $('#ui-slider-' + type);
        var slider = document.getElementById('ui-slider-' + type);

        if(!slider.noUiSlider) {
            noUiSlider.create(slider, {
                start: element.data('default'),
                connect: 'lower',
                behaviour: 'tap',
                step: 1,
                tooltips: true,
                format: wNumb({decimals: 0}),
                range: {
                    'min': element.data('min'),
                    'max': element.data('max')
                },
                pips: {
                    mode: 'range',
                    density: 5
                }
            });
        }

        slider.noUiSlider.on('slide', updateQualityValue);
        updateQualityValue();

        function updateQualityValue() {
            $('#quality-value').val(slider.noUiSlider.get());
        }

        $('#quality').removeClass('d-none');
        element.removeClass('d-none');
    }
}