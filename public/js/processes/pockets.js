$(document).ready(function () {

    pocketsSortable();

    $('.save-pocket-configurations').on('click', savePocket);

    $(document).on('click', '.add-pocket, .edit-pocket', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        var groupHtml, groupRuleHtml, groupRuleConditionHtml;
        var process_id = $('input[name=process-id]').val();
        var device_id = $(this).data('device-id');
        var pocket_id = $(this).data('pocket-id');
        if(pocket_id === undefined) {
            $.ajax({
                url : Parallax.Route.get('process.device.pockets.create', {process_id: process_id, device_id: device_id}),
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    groupHtml = data.group_html;
                    groupRuleHtml = data.group_rule_html;
                    groupRuleConditionHtml = data.group_rule_condition_html;
                    $('.configure-pocket-modal .modal-title').html(data.title);
                    $('.configure-pocket-modal .modal-body').html(data.html);
                    $('.configure-pocket-modal').modal('show');
                } else {
                    $('.configure-pocket-modal .modal-title').html(data.title);
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        } else {
            $.ajax({
                url : Parallax.Route.get('process.device.pockets.edit', {process_id: process_id, device_id: device_id, pocket: pocket_id}),
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    groupHtml = data.group_html;
                    groupRuleHtml = data.group_rule_html;
                    groupRuleConditionHtml = data.group_rule_condition_html;
                    $('.configure-pocket-modal .modal-title').html(data.title);
                    $('.configure-pocket-modal .modal-body').html(data.html);
                    $('.configure-pocket-modal').modal('show');
                } else {
                    $('.configure-pocket-modal .modal-title').html(data.title);
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        }

        //Off was used to prevent duplicate click event handlers
        $(document).off('click', '.configure-pocket-modal .add-rule-group-btn').on('click', '.configure-pocket-modal .add-rule-group-btn', function(){
            var groupRulesContainer = $('.configure-pocket-modal .group-rules-container');
            if(groupRulesContainer.find('.group-card').length !== 0) {
                groupRulesContainer.append(groupRuleConditionHtml).find('.group-title').each(function (index) {
                    $(this).html('Group ' + (index + 1));
                });
            } else {
                groupRulesContainer.append(groupHtml).find('.group-title').each(function (index) {
                    $(this).html('Group ' + (index + 1));
                });
            }
        }).off('click', '.configure-pocket-modal .remove-group-btn').on('click', '.configure-pocket-modal .remove-group-btn', function(){
            var groupRulesContainer = $('.configure-pocket-modal .group-rules-container');
            $(this).closest('.group-card').remove();
            if(groupRulesContainer.find('.group-card:first').find('.group-condition').length > 0) {
                groupRulesContainer.find('.group-card:first').find('.group-condition').remove();
            }
            groupRulesContainer.find('.group-title').each(function (index) {
                $(this).html('Group ' + (index + 1));
            });
        }).off('click', '.configure-pocket-modal .add-rule-btn').on('click', '.configure-pocket-modal .add-rule-btn', function(){
            $(this).parents('.card-header').siblings('.card-body').append(groupRuleHtml);
        }).off('click', '.configure-pocket-modal .rule-row .ion-trash-b').on('click', '.configure-pocket-modal .rule-row .ion-trash-b', function(){
            $(this).parents('.rule-row').remove();
        });
    }).on('click', '.delete-pocket', function (e) {
        e.preventDefault();
        var process_id = $('input[name=process-id]').val();
        var device_id = $(this).data('device-id');
        var pocket_id = $(this).data('pocket-id');
        swal({
            title: 'Are you sure?',
            text: $(this).data('message'),
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!'
        }).then(function(result) {
            if (result.value) {
                Parallax.Loader.show();
                $.ajax({
                    type: 'DELETE',
                    url : Parallax.Route.get('process.device.pockets.destroy', {process_id: process_id, device_id: device_id, pocket: pocket_id}),
                    dataType: 'json'
                }).done(function (data) {
                    if(data.return === true) {
                        toastr.success(data.message, '');
                        $('.processdevice-' + data.id + '-pockets').html(data.html);
                    } else {
                        toastr.error(data.message, '');
                    }
                    Parallax.Loader.hide();
                });
                swal.close()
            }
        });
    }).on('change', '.field-checkbox', function () {
        var checked = $(this).is(':checked');
        $('[data-field-name="' + $(this).data('field-name') + '"]').prop('checked',false);
        if(checked) {
            $(this).prop('checked',true);
        }
    });
});

function savePocket() {
    var object = {};

    object['groups'] = {};
    $('body .configure-document-modal .group-card').each(function (group) {
        object['groups'][group] = {'condition' : $(this).find('.group-condition-value').val()};
        object['groups'][group]['rules'] = {};
        $(this).find('.rule-row').each(function (rule) {
            object['groups'][group]['rules'][rule] = {
                'condition' : $(this).find('.condition').val(),
                'field' : $(this).find('.field').val(),
                'operator' : $(this).find('.operator').val(),
                'value' : $(this).find('.value').val()
            };
        });
    });

    object['read'] = [];
    $('.field-read').each(function () {
        if($(this).is(':checked')) {
            object.read.push($(this).val());
        }
    });

    object['exist'] = [];
    $('.field-exist').each(function () {
        if($(this).is(':checked')) {
            object.exist.push($(this).val());
        }
    });

    object['endorsement'] = $('#endorsement').val();

    Parallax.Loader.show();

    var process_id = $('input[name=process-id]').val();
    var device_id = $('input[name=device-id]').val();
    var pocket_id = $('input[name=pocket-id]').val();
    var physical_pocket = $('.physical-pocket option:selected').val();
    var title = $('#title').val();

    if(pocket_id === undefined) {
        $.ajax({
            type: 'POST',
            url : Parallax.Route.get('process.device.pockets.store', {process_id: process_id, device_id: device_id}),
            dataType: 'json',
            data: { 'parameters': JSON.stringify(object), 'physical_pocket' : physical_pocket, 'title' : title},
        }).done(function (data) {
            if(data.return === true) {
                toastr.success(data.message, '');
                $('.configure-pocket-modal').modal('hide');
                $('.processdevice-' + data.id + '-pockets').html(data.html);
                pocketsSortable();
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    } else {
        $.ajax({
            type: 'PUT',
            url : Parallax.Route.get('process.device.pockets.update', {process_id: process_id, device_id: device_id, pocket: pocket_id}),
            dataType: 'json',
            data: { 'parameters': JSON.stringify(object), 'physical_pocket' : physical_pocket, 'title' : title},
        }).done(function (data) {
            if(data.return === true) {
                toastr.success(data.message, '');
                $('.configure-pocket-modal').modal('hide');
                $('.processdevice-' + data.id + '-pockets').html(data.html);
                pocketsSortable();
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }
}

function pocketsSortable() {
    $(".pockets-table").sortable({
        items: "tr:not(:first)",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            //sendOrderToServer();
        }
    });
}