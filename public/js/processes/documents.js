$(document).ready(function () {

    documentsSortable();

    $('.save-document-configurations').on('click', saveDocument);

    $('.select-document-modal').on('hide.bs.modal', function (e) {
        Parallax.Loader.show();
        $.ajax({
            url : Parallax.Route.get('process.documents.index', {process_id: $('input[name=process-id]').val()}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('#documents-list').html(data.html);
            } else {
                toastr.error(data.message, '');
            }
            documentsSortable();
            Parallax.Loader.hide();
        });
    });

    $(document).on('click', '.configure-document', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        var groupHtml, groupRuleHtml, groupRuleConditionHtml;
        var process_id = $('input[name=process-id]').val();
        var document_id = $(this).data('document-id');
        $.ajax({
            url : Parallax.Route.get('process.documents.edit', {process_id: process_id, document: document_id}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                groupHtml = data.group_html;
                groupRuleHtml = data.group_rule_html;
                groupRuleConditionHtml = data.group_rule_condition_html;
                $('.configure-document-modal .modal-body').html(data.html);
                $('.configure-document-modal').modal('show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });

        //Off was used to prevent duplicate click event handlers
        $(document).off('click', '.configure-document-modal .add-rule-group-btn').on('click', '.configure-document-modal .add-rule-group-btn', function(){
            var groupRulesContainer = $('.configure-document-modal .group-rules-container');
            if(groupRulesContainer.find('.group-card').length !== 0) {
                groupRulesContainer.append(groupRuleConditionHtml).find('.group-title').each(function (index) {
                    $(this).html('Group ' + (index + 1));
                });
            } else {
                groupRulesContainer.append(groupHtml).find('.group-title').each(function (index) {
                    $(this).html('Group ' + (index + 1));
                });
            }
        }).off('click', '.configure-document-modal .remove-group-btn').on('click', '.configure-document-modal .remove-group-btn', function(){
            var groupRulesContainer = $('.configure-document-modal .group-rules-container');
            $(this).closest('.group-card').remove();
            if(groupRulesContainer.find('.group-card:first').find('.group-condition').length > 0) {
                groupRulesContainer.find('.group-card:first').find('.group-condition').remove();
            }
            groupRulesContainer.find('.group-title').each(function (index) {
                $(this).html('Group ' + (index + 1));
            });
        }).off('click', '.configure-document-modal .add-rule-btn').on('click', '.configure-document-modal .add-rule-btn', function(){
            $(this).parents('.card-header').siblings('.card-body').append(groupRuleHtml);
        }).off('click', '.configure-document-modal .rule-row .ion-trash-b').on('click', '.configure-document-modal .rule-row .ion-trash-b', function(){
            $(this).parents('.rule-row').remove();
        });
    });

    $(document).on('click', '.link', function (e) {
        Parallax.Loader.show();
        var statesMap = {"true": 1, "false": 0};
        var state = statesMap[$(this).prop('checked')];
        var process_id = $('input[name=process-id]').val();
        var document_id = $(this).data('document-id');
        $.ajax({
            type: 'PUT',
            url : Parallax.Route.get('process.documents.update', {process_id: process_id, document: document_id}),
            data: { 'state': state },
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                toastr.success(data.message, '');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    })
});

function saveDocument() {
    var object = {};

    object['groups'] = {};
    $('body .configure-document-modal .group-card').each(function (group) {
        object['groups'][group] = {'condition' : $(this).find('.group-condition-value').val()};
        object['groups'][group]['rules'] = {};
        $(this).find('.rule-row').each(function (rule) {
            object['groups'][group]['rules'][rule] = {
                'condition' : $(this).find('.condition').val(),
                'field' : $(this).find('.field').val(),
                'operator' : $(this).find('.operator').val(),
                'value' : $(this).find('.value').val()
            };
        });
    });

    object['capture_front'] = $('#capture_front').is(':checked');
    object['capture_back'] = $('#capture_back').is(':checked');
    object['rear_endorse'] = $('#rear_endorse').is(':checked');
    object['rear_stamp'] = $('#rear_stamp').is(':checked');
    object['save_scan_items'] = $('#save_scan_items').is(':checked');
    object['add_item_to_db'] = $('#add_item_to_db').is(':checked');
    object['max_number_of_items'] = Number($('#max_number_of_items').val());

    Parallax.Loader.show();

    var process_id = $('input[name=process-id]').val();
    var document_id = $('input[name=document-id]').val();
    $.ajax({
        type: 'PUT',
        url : Parallax.Route.get('process.documents.update', {process_id: process_id, document: document_id}),
        dataType: 'json',
        data: { 'parameters': JSON.stringify(object)},
    }).done(function (data) {
        if(data.return === true) {
            toastr.success(data.message, '');
            $('.configure-document-modal').modal('hide');
        } else {
            toastr.error(data.message, '');
        }
        Parallax.Loader.hide();
    });
}

function documentsSortable() {
    $("#processes-documents-table").sortable({
        items: "tr:not(:first)",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            //sendOrderToServer();
        }
    });
}