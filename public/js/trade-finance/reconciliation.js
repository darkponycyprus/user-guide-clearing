$(document).ready(function () {

    var keys = Parallax.Plugins.Keys.init('#reconciliation-batch-documents-list');

    $(document).on('click', '#data-entry', function (e) {
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.batch.data-entry', {batch_id: $(this).data('batch-id')}),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                new Parallax.TradeFinance.BatchDataEntry('#data-entry-modal', {
                    items: data.items,
                    batch: {
                        total_amount: data.total_amount
                    },
                    priceFormat: {
                        prefix: "€ ",
                        centsSeparator: '.',
                        thousandsSeparator: ',',
                        limit: 11,
                        centsLimit: 2
                    }
                }, 'batch.scan.item.update.data.entry');
                $('#data-entry-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '#correct-rejected', function (e) {
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.batch.rejected', {batch_id: $(this).data('batch-id')}),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                new Parallax.TradeFinance.BatchRejectedDocuments('#correct-rejected-modal', {
                    items: data.items,
                    batch: {
                        total_amount: data.total_amount
                    },
                    priceFormat: {
                        prefix: "€ ",
                        centsSeparator: '.',
                        thousandsSeparator: ',',
                        limit: 11,
                        centsLimit: 2
                    }
                }, 'batch.scan.item.update.reject');
                $('#correct-rejected-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show').draggable({
                    handle: ".modal-header"
                });
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '#edit', function (e) {
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.batch.edit-documents', {batch_id: $(this).data('batch-id')}),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                new Parallax.TradeFinance.BatchRejectedDocuments('#edit-modal', {
                    items: data.items,
                    batch: {
                        total_amount: data.total_amount
                    },
                    priceFormat: {
                        prefix: "€ ",
                        centsSeparator: '.',
                        thousandsSeparator: ',',
                        limit: 11,
                        centsLimit: 2
                    }
                }, 'batch.scan.item.update.document');
                $('#edit-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '#batch-processed', function (e) {
        var button = $(this);
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.batch.processed', {batch_id: $(this).data('batch-id')}),
            type: 'GET',
            dataType: 'json'
        }).done(function (data) {
            if (data.return === true) {
                button.closest('form').submit();
            } else {
                if(data.swal_messages.length !== 0) {
                    var messagelist = '';
                    $.each(data.swal_messages, function (index, message) {
                        messagelist += '<li>' + message + '</li>';
                    });
                    swal({
                        title: Parallax.Lang.get('reconciliation.batch.incomplete_batch'),
                        html: "<ol>" + messagelist + "</ol><br />" + Parallax.Lang.get('reconciliation.batch.incomplete_batch_message'),
                        type: 'info',
                        reverseButtons: true,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: Parallax.Lang.get('reconciliation.batch.exit'),
                        cancelButtonText: Parallax.Lang.get('reconciliation.batch.correct_errors')
                    }).then(function(result) {
                        if (result.value) {
                            button.closest('form').submit();
                        }
                    })
                } else {
                    toastr.error(data.message, '');
                }
            }
            Parallax.Loader.hide();
        });
    }).on('hide.bs.modal', '#data-entry-modal', function () {
        keys.on();
    }).on('hide.bs.modal', '#correct-rejected-modal', function () {
        keys.on();
    }).on('hide.bs.modal', '#edit-modal', function () {
        keys.on();
    });
});