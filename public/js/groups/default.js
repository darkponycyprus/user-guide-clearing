$(document).ready(function () {
    $('#processes').select2({
        placeholder: "Select Process",
        allowClear: true,
        multiple: true
    });
    fieldsSortable();
    $(document).on('click', '.unlink-field', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        var id = $('input[name=field-group-id]').val();
        var dbtablefieldid = $(this).data('field-id');
        $.ajax({
            type: 'PUT',
            url : Parallax.Route.get('group.update', {group_id: id}),
            dataType: 'json',
            data: {'field_id': dbtablefieldid, 'state': 0}
        }).done(function (data) {
            if(data.return === true) {
                $('#db-table-field-group-id-' + dbtablefieldid).remove();
                toastr.success(data.message,'');
            } else {
                toastr.error(data.message,'');
            }
            Parallax.Loader.hide();
        });
    }).on('submit', 'form#edit-field-form', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        var id = $('input[name=db-table-field-group-id]').val();
        $.ajax({
            type: 'PUT',
            url : Parallax.Route.get('dbtablefieldgroup.update', {dbtablefield_group_id: id}),
            data: $('form#edit-field-form').find('[name!=_token]').serialize(),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.edit-field-modal').modal('hide');
                toastr.success(data.message,'');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '.edit-field', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        var id = $(this).data('db-table-field-group-id');
        $.ajax({
            url : Parallax.Route.get('dbtablefieldgroup.edit', {dbtablefield_group_id: id}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.dbTableFieldGroup').html(data.html);
                $('.edit-field-modal').modal('show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '.add-field', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            url : Parallax.Route.get('group.dbtablesfields.index', {group_id: $('input[name=field-group-id]').val()}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.dbTableFields').html(data.html);
                $('.add-field-modal').modal('show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('hide.bs.modal', '.add-field-modal', function (e) {
        Parallax.Loader.show();
        $.ajax({
            url : Parallax.Route.get('group.show', {group_id: $('input[name=field-group-id]').val()}),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('#fields-list').html(data.html);
            } else {
                toastr.error(data.message, '');
            }
            fieldsSortable();
            Parallax.Loader.hide();
        });
    }).on('click', '.dbTableFields .pagination a', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        var page = $(this).attr('href').split('page=')[1];
        $.ajax({
            url : Parallax.Route.get('group.dbtablesfields.index', {group_id: $('input[name=field-group-id]').val()}) + '?page=' + page,
            data: $('form#add-fields-form').find('[name^=filter]').serialize(),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.dbTableFields').html(data.html);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('change', '[name="filter_table"], [name="filter_virtual"], [name="filter_limit"]', function (e) {
        e.preventDefault();
        $('form#add-fields-form').trigger('submit');
    }).on('submit', 'form#add-fields-form', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            url : Parallax.Route.get('group.dbtablesfields.index', {group_id: $('input[name=field-group-id]').val()}),
            data: $('form#add-fields-form').find('[name^=filter]').serialize(),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.dbTableFields').html(data.html);
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '.link', function (e) {
        Parallax.Loader.show();
        var statesMap = {"true": 1, "false": 0};
        var id = $(this).data('db-table-field-id');
        var state = statesMap[$(this).prop('checked')];
        $.ajax({
            type: 'PUT',
            url : Parallax.Route.get('group.update', {group_id: $('input[name=field-group-id]').val()}),
            data: { 'field_id': id, 'state': state },
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                toastr.success(data.message, '');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    });
});

function fieldsSortable() {
    $("#fields-list-table").sortable({
        items: "tr:not(:first)",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            Parallax.Loader.show();
            var fieldsPriority = [];
            $('#fields-list-table tbody tr').each(function(id, val){
                fieldsPriority[$(val).data('field-id')] = id + 1;
            });
            $.ajax({
                type: 'POST',
                url: Parallax.Route.get('dbtablefieldgroup.update-priorities', {dbtablefield_group_id: $('#group').data('obj').id}),
                dataType: 'json',
                data: {fields_priority: fieldsPriority}
            }).done(function (data) {
                Parallax.Loader.hide();
                toastr.success(data.message);
            });
        }
    });
}