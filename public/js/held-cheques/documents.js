$(document).ready(function () {
    var held_cheques = $('#held-cheques');
    var url = '';
    new Parallax.Plugins.Image('#held-cheques');
    if(held_cheques.data('branch-id')) {
        url = Parallax.Route.get('held-cheques.branch.documents', {code: held_cheques.data('branch-id')});
    } else {
        url = Parallax.Route.get('held-cheques.documents');
    }
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {'filter_search': held_cheques.find('input[name="filter_search"]').val()}
    }).done(function (data) {
        if (data.return === true) {
            new Parallax.Clearing.HeldCheques('#held-cheques', {
                items: data.items,
                heldChequesStatuses: data.heldChequesStatuses,
                bankingSystemStatuses: data.bankingSystemStatuses,
                previousSessionId: data.previousSessionId,
                nextDayDecision: data.nextDayDecision,
                priceFormat: {
                    prefix: "€ ",
                    centsSeparator: '.',
                    thousandsSeparator: ',',
                    limit: 11,
                    centsLimit: 2
                }
            }, 'held-cheques.archives.save');
        } else {
            toastr.error(data.message, '');
        }
    });
});