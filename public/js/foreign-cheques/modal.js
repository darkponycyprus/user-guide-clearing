$(document).ready(function () {
    $(document).on('click', '.reports-currency-selection', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: $(this).data('ajax-route'),
            dataType: 'json'
        }).done(function (data) {
            if(data.return === true) {
                $('.reports-currency-selection-modal .modal-content').html(data.html);
                $('.reports-currency-selection-modal').modal('show');
                $('.reports-currency-selection-modal .select-2').select2();
                $('#report-datepicker').on('show', fix_xeditable_conflict)
                    .datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        weekStart: 1,
                        daysOfWeekDisabled: '06',
                        endDate: '0d',
                        maxViewMode: 0,
                        disabledDates: [

                        ],
                        container: '#report-datepicker-container'
                    });
            }
            $('[name="filter_date"]').inputmask('dd/mm/yyyy', {
                clearMaskOnLostFocus: true,
                positionCaretOnClick: "none",
                placeholder: "dd/mm/" + new Date().getFullYear()
            });
            Parallax.Loader.hide();
        });
    }).on('change', '.reports-currency-selection-modal [name="filter_currency"]', function (e) {
        e.preventDefault();
        Parallax.Loader.show();
        $('.reports-currency-selection-modal [name="filter_bank_account"]').html("<option value=''>Bank Account</option>");
        var currency_id = ($('.reports-currency-selection-modal [name="filter_currency"]').val()) ? $('.reports-currency-selection-modal [name="filter_currency"]').val() : null;
        if(currency_id != null) {
            $.ajax({
                method: 'GET',
                url: $(this).data('ajax-route'),
                data: {'currency_id': currency_id},
                dataType: 'json'
            }).done(function (data) {
                if (data.return === true) {
                    if(Object.keys(data.accounts).length === 1) {
                        $.each(data.accounts, function(id, val) {
                            $('.reports-currency-selection-modal [name="filter_bank_account"]').append("<option selected='selected' value='" + id + "'>" + val + "</option>");
                        });
                    } else {
                        $.each(data.accounts, function(id, val){
                            $('.reports-currency-selection-modal [name="filter_bank_account"]').append("<option value='"+id+"'>"+val+"</option>");
                        });
                    }
                    $('.reports-currency-selection-modal .select-2').select2();
                }
                Parallax.Loader.hide();
            });
        } else {
            Parallax.Loader.hide();
        }
    }).on('change', '[name="calendar_date"]', function(e){
        e.preventDefault();
        $('[name="filter_date"]').val($(this).val());
    }).on('click', '#report-datepicker-container .fa-calendar-o', function(){
        $('#reports-calendar-picker').on('show', fix_xeditable_conflict)
            .datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                weekStart: 1,
                daysOfWeekDisabled: '06',
                endDate: '0d',
                maxViewMode: 0,
                disabledDates: [],
                container: '#report-datepicker-container'
            });
        $('[name="calendar_date"]').focus();
    });
});
