$(document).ready(function () {

    var keys = Parallax.Plugins.Keys.init('#reconciliation-filter-documents-list');

    $(document).on('click', '#data-entry', function (e) {
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.session.custom-query.data-entry', {session_id: $(this).data('session-id')}),
            type: 'POST',
            dataType: 'json',
            data: {
                custom_query_json: $(this).data('custom-query-json'),
                custom_query_id: $(this).data('custom-query-id')
            }
        }).done(function (data) {
            if (data.return === true) {
                new Parallax.ForeignCheques.CustomQueryDataEntry('#data-entry-modal', {
                    items: data.items,
                    batch: {
                        total_amount: data.total_amount,
                        currency: data.currency
                    },
                    priceFormat: {
                        prefix: data.currency + " ",
                        centsSeparator: '.',
                        thousandsSeparator: ',',
                        limit: 11,
                        centsLimit: 2
                    }
                }, 'custom-query.scan.item.update.data.entry');
                $('#data-entry-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '#correct-rejected', function (e) {
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.session.custom-query.rejected', {session_id: $(this).data('session-id')}),
            type: 'POST',
            dataType: 'json',
            data: {
                custom_query_json: $(this).data('custom-query-json'),
                custom_query_id: $(this).data('custom-query-id')
            }
        }).done(function (data) {
            if (data.return === true) {
                new Parallax.ForeignCheques.CustomQueryRejectedDocuments('#correct-rejected-modal', {
                    items: data.items,
                    batch: {
                        total_amount: data.total_amount
                    },
                    priceFormat: {
                        prefix: data.currency + " ",
                        centsSeparator: '.',
                        thousandsSeparator: ',',
                        limit: 11,
                        centsLimit: 2
                    }
                }, 'custom-query.scan.item.update.reject');
                $('#correct-rejected-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show').draggable({
                    handle: ".modal-header"
                });
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('click', '#edit', function (e) {
        keys.off();
        e.preventDefault();
        Parallax.Loader.show();
        $.ajax({
            context: this,
            url: Parallax.Route.get('reconciliation.session.custom-query.edit-documents', {session_id: $(this).data('session-id')}),
            type: 'POST',
            dataType: 'json',
            data: {
                custom_query_json: $(this).data('custom-query-json'),
                custom_query_id: $(this).data('custom-query-id')
            }
        }).done(function (data) {
            if (data.return === true) {
                new Parallax.ForeignCheques.CustomQueryRejectedDocuments('#edit-modal', {
                    items: data.items,
                    batch: {
                        total_amount: data.total_amount
                    },
                    priceFormat: {
                        centsSeparator: '.',
                        thousandsSeparator: ',',
                        limit: 11,
                        centsLimit: 2
                    }
                }, 'custom-query.scan.item.update.document');
                $('#edit-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');
            } else {
                toastr.error(data.message, '');
            }
            Parallax.Loader.hide();
        });
    }).on('hide.bs.modal', '#data-entry-modal', function () {
        keys.on();
    }).on('hide.bs.modal', '#correct-rejected-modal', function () {
        keys.on();
    }).on('hide.bs.modal', '#edit-modal', function () {
        keys.on();
    });
});