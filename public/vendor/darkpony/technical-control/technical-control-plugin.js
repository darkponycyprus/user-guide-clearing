(function( $ ) {

    $.fn.technicalControl = function(options) {
        var save_ajax_flag = false;
        var signature_ajax_flag = false;
        var technical_control = this;
        var image_manipulation_main = technical_control.find('.image-manipulation-main');
        var settings = $.extend({
            items: {},
            technicalControlStatuses: {},
        }, options );

        var currentItemIndex = 0;

        if(settings.items.some(function (item) {
            return (item.return_reason_1 == null && item.return_reason_2 == null);
        })) {

          $(settings.items).each(function(index, value){
            if(value.return_reason_1 == null && value.return_reason_2 == null){
              currentItemIndex = index;
              return false;
            }
          });
        }

        settings.items.forEach(function (item) {
            Images[item.id] = item.image;
        });

        technical_control.on('click', 'tr[data-document-id]', function () {
            var id = $(this).attr('data-document-id');
            $(settings.items).each(function(index, value){
              if(value.id == id){
                currentItemIndex = index;
                return false;
              }
            });

            displayItem();
            displayImage();
            displaySignatures();
        });

        displayImage();

        displayItem();

        $('#confirm').on("click", function () {
            if(currentItemIndex <= (settings.items.length - 1)) {
                if(save_ajax_flag == false && signature_ajax_flag == false) {
                    saveItem();
                    displayImage();
                    displaySignatures();
                }

            }
        }).prop('disabled', false);

        $(document).on('keydown', function (e) {
            var keyCode = e.keyCode || e.which;
            //Enter key was pressed.
            if (keyCode === 13) {
                if (technical_control.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                if (currentItemIndex !== 0) {
                    if(signature_ajax_flag == false) {
                        prevItem();
                    }
                }
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                if (currentItemIndex !== (settings.items.length - 1)) {
                    if(signature_ajax_flag == false) {
                        nextItem();
                    }
                }
            }
        }).on('keyup', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                if(technical_control.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
                if(currentItemIndex <= (settings.items.length - 1)) {
                    $('#confirm').click();
                }
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                if(signature_ajax_flag == false) {
                    displayImage();
                    displaySignatures();
                }
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                if(signature_ajax_flag == false) {
                    displayImage();
                    displaySignatures();
                }
            }
        });

        function setItem() {
            var item = settings.items[currentItemIndex];

            technical_control.find('.position').html(currentItemIndex + 1);
            technical_control.find('.cheque_no').html(item.cheque_no);
            technical_control.find('.cheque_account_no').html(item.cheque_account_no);
            technical_control.find('.amount').html(item.amount);
            technical_control.find('.current .rr1').html(item.return_reason_1);
            technical_control.find('.current .rr2').html(item.return_reason_2);
            if(item.return_reason_1 !== null && item.return_reason_1 !== '') {
                technical_control.find('.current-checked').html('<i class="fa color-green fa-check-square"></i>');
            } else {
                technical_control.find('.current-checked').html('');
            }

            var rr1TechnicalControlOptions = settings.technicalControlStatuses.map(function (obj) {
                if(obj.code == '00' && item.return_reason_1 == null) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        selected: true
                    }
                } else {
                    if(obj.code == item.return_reason_1) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        }
                    } else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        }
                    }
                }
            });

            var rr2TechnicalControlOptions = settings.technicalControlStatuses.map(function (obj) {
                if(obj.code == '00' && item.return_reason_2 == null) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        selected: true
                    }
                } else {
                    if (obj.code == item.return_reason_2) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        };
                    } else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        };
                    }
                }
            });

            technical_control.find('#rr1-input').empty().select2({
                data: rr1TechnicalControlOptions,
                dropdownAutoWidth : 'true'
            });

            technical_control.find('#rr2-input').empty().select2({
                data: rr2TechnicalControlOptions,
                dropdownAutoWidth : 'true'
            });
        }

        function prevItem() {
            var previousItemIndex = currentItemIndex - 1;

            if(previousItemIndex >= 0){
                currentItemIndex = previousItemIndex;
                displayItem();
            }
        }

        function nextItem() {
            var nextItemIndex = currentItemIndex + 1;

            if(nextItemIndex <= (settings.items.length - 1)) {
                currentItemIndex = nextItemIndex;
                displayItem();
            }
        }

        function scroll() {
            var item = settings.items[currentItemIndex];
            var position = technical_control.find('#technical-control-documents-box table [data-document-id=' + item.id + ']').position().top;
            technical_control.find('#technical-control-documents-box .cardbox-body').animate({
                scrollTop: position
            }, 0);
        }

        function displayItem() {
            setCurrent();
            setItem();
            scroll();
            //highlightSignRule();
        }

        function highlightSignRule(){
            var item = settings.items[currentItemIndex];
            var tr = $("#signatures-box tr").filter(function() {
                return (($(this).attr("data-amount-from") < item["clearAmount"]) && ($(this).attr("data-amount-to") > item["clearAmount"]));
            });
            $("#signatures-box tr").each(function(id,val){
                if($(val).attr("id") != $(tr).attr("id")){
                    $(this).find("[aria-expanded='true']").click();
                }
            });
            $(tr).find("[aria-expanded='false']").click();
        }

        function displayImage() {
            var item = settings.items[currentItemIndex];
            if(item.image !== null) {
                image_manipulation_main.attr('data-image-id', item.id).attr('data-orientation', 'front');
                if(Images[item.id].front_filename !== null) {
                    image_manipulation_main.attr('src', Images[item.id].front_filename).attr('data-front-image', Images[item.id].front_filename);
                }
                if(Images[item.id].back_filename !== null) {
                    image_manipulation_main.attr('data-back-image', Images[item.id].back_filename);
                }
            }
        }

        function displaySignatures() {
            signature_ajax_flag = true;
            Parallax.Loader.show('Getting signatures');
            var item = settings.items[currentItemIndex];
            Parallax.Loader.hide();
            signature_ajax_flag = false;
            /*$.ajax({
                url: 'technical-control/isign-conditions/' + item['cheque_account_no'] + '/' + item['db_amount'],
                type: 'GET'
            }).done(function (data) {
                signature_ajax_flag = false;
                $('#signatures-container').html(data.view);
                Parallax.Loader.hide();
            }).fail(function () {
                signature_ajax_flag = false;
                Parallax.Loader.hide();
            });*/
        }

        function setCurrent() {
            var item = settings.items[currentItemIndex];
            var current_row = technical_control.find('#technical-control-documents-box table [data-document-id=' + item.id + ']');
            technical_control.find('span.current-document').html(currentItemIndex + 1);
            technical_control.find('#technical-control-documents-box table tr.current').removeClass('current');
            current_row.addClass('current');
        }

        function setDocumentRow(index) {
            var item = settings.items[index];
            var row = technical_control.find('#technical-control-documents-box table [data-document-id=' + item.id + ']');
            if(item.return_reason_1 !== null) {
                row.find('.rr1').html(item.return_reason_1);
                row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
            } else {
                row.find('.rr1').empty();
                row.find('.checked').empty();
            }
            if(item.return_reason_2 !== null) {
                row.find('.rr2').html(item.return_reason_2);
            } else {
                row.find('.rr2').empty();
            }
        }

        function setProcessed() {
            var processed = settings.items.filter(function (item) {
                return item.return_reason_1 !== null;
            }).length;

            $('.processed').html(processed);
            var percentage = (processed * 100) / settings.items.length;
            if(percentage > 70) {
                $('.progress-bar').css('width', percentage + '%').prev().addClass('color-white');
            } else {
                $('.progress-bar').css('width', percentage + '%');
            }
        }

        function saveItem() {
            save_ajax_flag = true;
            var index = currentItemIndex;
            var item = settings.items[currentItemIndex];
            var rr1 = technical_control.find('#rr1-input').val();
            var rr2 = technical_control.find('#rr2-input').val();
            if (rr1 == "00" && rr2 != "00") {
                toastr.error(Parallax.Lang.get('technical_control.branch.messages.error.wrong_return_reason_2'));
                return false;
            }
            if (rr1 != "00" && rr1 == rr2) {
                toastr.error(Parallax.Lang.get('technical_control.branch.messages.error.same_reason'));
                return false;
            }
            if(currentItemIndex === (settings.items.length - 1)) {
                $('#confirm').prop('disabled', true);
            } else {
                $('#confirm').prop('disabled', true);
                nextItem();
            }
            $.ajax({
                url: Parallax.Route.get('technical-control.archive.save', {item_id: item.id}),
                type: 'PUT',
                data: {rr1: rr1, rr2: rr2},
                dataType: 'json'
            }).done(function (data) {
                save_ajax_flag = false;
                $('#confirm').prop('disabled', false);
                if(data.return === true) {
                    item.return_reason_1 = rr1;
                    item.return_reason_2 = rr2;
                    setProcessed();
                    setDocumentRow(index);
                    toastr.success(data.message,'');
                } else {
                    toastr.error(data.message, '');
                }
            }).fail(function () {
                toastr.options = {"showDuration": 0};
                toastr.error('Unexpected AJAX call error on saveItem().','');
                save_ajax_flag = false;
                $('#confirm').prop('disabled', false);
            })
        }

        return this;
    };

}( jQuery ));
