(function( $ ) {

    $.fn.heldCheques = function(options) {
        var save_ajax_flag = false;
        var held_cheques = this;
        var image_manipulation_main = held_cheques.find('.image-manipulation-main');
        var settings = $.extend({
            items: {},
            heldChequesStatuses: {},
            bankingSystemStatuses: {},
            previousSessionId: 0,
            nextDayDecision: 99
        }, options );

        var currentItemIndex = 0;

        var heldChequesStatuses = settings.heldChequesStatuses.map(function(value) {
            return value.code;
        });

        if(settings.items.some(function (item) {
            if(item.session_id == settings.previousSessionId && (item.return_reason_1 == settings.nextDayDecision || item.return_reason_2 == settings.nextDayDecision)) {
                return true;
            } else {
                return ($.inArray(item.return_reason_1, heldChequesStatuses) === -1 || $.inArray(item.return_reason_2, heldChequesStatuses) === -1);
            }
          })) {
            $(settings.items).each(function(index, item){
              if(item.session_id == settings.previousSessionId && (item.return_reason_1 == settings.nextDayDecision || item.return_reason_2 == settings.nextDayDecision)) {
                currentItemIndex = index;
                return false;
              }else if($.inArray(item.return_reason_1, heldChequesStatuses) === -1 || $.inArray(item.return_reason_2, heldChequesStatuses) === -1){
                currentItemIndex = index;
                return false;
              }
            });
        }

        settings.items.forEach(function (item) {
            Images[item.id] = item.image;
        });

        held_cheques.on('click', 'tr[data-document-id]', function () {
            var id = $(this).attr('data-document-id');
            $(settings.items).each(function(index, item){
              if(item.id == id){
                currentItemIndex = index;
                return false;
              }
            });

            displayItem();
            displayImage();
        });

        displayImage();

        displayItem();

        $('#confirm').on("click", function () {
            if(currentItemIndex <= (settings.items.length - 1)) {
                if(save_ajax_flag == false) {
                    saveItem();
                    displayImage();
                }
            }
        }).prop('disabled', false);


        $(document).on('keydown', function (e) {
            var keyCode = e.keyCode || e.which;
            //Enter key was pressed.
            if (keyCode === 13) {
                if (held_cheques.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                if (currentItemIndex !== 0) {
                    prevItem();
                }
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                if (currentItemIndex !== (settings.items.length - 1)) {
                    nextItem();
                }
            }

        }).on('keyup', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                if(held_cheques.find('input[name="filter_search"]').is(":focus")) {
                    return;
                }
                e.preventDefault();
                if(currentItemIndex <= (settings.items.length - 1)) {
                    $('#confirm').click();
                }
            }
            //Up key was pressed.
            if (keyCode === 38) {
                e.preventDefault();
                displayImage();
            }
            //Down key was pressed.
            if (keyCode === 40) {
                e.preventDefault();
                displayImage();
            }
        });


        function setItem() {
            var item = settings.items[currentItemIndex];

            held_cheques.find('.position').html(currentItemIndex + 1);
            held_cheques.find('.cheque_no').html(item.cheque_no);
            held_cheques.find('.cheque_account_no').html(item.cheque_account_no);
            held_cheques.find('.amount').html(item.amount);
            held_cheques.find('.current .rr1').html(item.return_reason_1);
            held_cheques.find('.current .rr2').html(item.return_reason_2);

            var rr1BankingSystemOptions = settings.bankingSystemStatuses.filter(function (obj) {
                if(obj.code == item.return_reason_1) {
                    return true;
                }
            }).map(function (obj) {
                if(obj.code == item.return_reason_1) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        disabled: true,
                        selected: true
                    };
                }
            });

            var rr2BankingSystemOptions = settings.bankingSystemStatuses.filter(function (obj) {
                if(obj.code == item.return_reason_2) {
                    return true;
                }
            }).map(function (obj) {
                if(obj.code == item.return_reason_2) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        disabled: true,
                        selected: true
                    };
                }
            });

            var rr1HeldChequeOptions = settings.heldChequesStatuses.filter(function (obj) {
                if(settings.previousSessionId == item.session_id && obj.code == settings.nextDayDecision && item.return_reason_1 != settings.nextDayDecision) {
                    return false;
                }
                return true;
            }).map(function (obj) {
                if(settings.previousSessionId == item.session_id) {
                    if(obj.code == settings.nextDayDecision) {
                        if(obj.code == item.return_reason_1) {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description,
                                selected: true,
                                disabled: true
                            }
                        } else  {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description,
                                disabled: true
                            }
                        }
                    } else {
                        if(obj.code == item.return_reason_1) {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description,
                                selected: true
                            }
                        } else {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description
                            }
                        }
                    }
                } else {
                    if(obj.code == item.return_reason_1) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        }
                    } else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        }
                    }
                }
            });

            var rr2HeldChequeOptions = settings.heldChequesStatuses.filter(function (obj) {
                if(settings.previousSessionId == item.session_id && obj.code == settings.nextDayDecision && item.return_reason_2 != settings.nextDayDecision) {
                    return false;
                }
                return true;
            }).map(function (obj) {
                if(settings.previousSessionId == item.session_id) {
                    if(obj.code == settings.nextDayDecision) {
                        if(obj.code == item.return_reason_2) {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description,
                                selected: true,
                                disabled: true
                            }
                        } else  {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description,
                                disabled: true
                            }
                        }
                    } else {
                        if(obj.code == item.return_reason_2) {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description,
                                selected: true
                            }
                        } else {
                            return {
                                id: obj.id || obj.code,
                                text: obj.text || obj.description
                            }
                        }
                    }
                } else {
                    if(obj.code == item.return_reason_2) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        }
                    } else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        }
                    }
                }
            });

            held_cheques.find('#rr1-input').empty().select2({
                data: rr1BankingSystemOptions.concat(rr1HeldChequeOptions),
                dropdownAutoWidth : 'true'
            });

            held_cheques.find('#rr2-input').empty().select2({
                data: rr2BankingSystemOptions.concat(rr2HeldChequeOptions),
                dropdownAutoWidth : 'true'
            });

            if(item.session_id == settings.previousSessionId) {
                if($.inArray(item.return_reason_1, heldChequesStatuses) !== -1 && $.inArray(item.return_reason_2, heldChequesStatuses) !== -1 && item.return_reason_1 != settings.nextDayDecision) {
                    held_cheques.find('.current .checked').html('<i class="fa color-green fa-check-square"></i>');
                    held_cheques.find('#current-document-box .checked').html('<i class="fa color-green fa-check-square"></i>');
                } else {
                    held_cheques.find('.current .checked').empty();
                    held_cheques.find('#current-document-box .checked').empty();
                }
            } else {
                if($.inArray(item.return_reason_1, heldChequesStatuses) !== -1 && $.inArray(item.return_reason_2, heldChequesStatuses) !== -1) {
                    held_cheques.find('.current .checked').html('<i class="fa color-green fa-check-square"></i>');
                    held_cheques.find('#current-document-box .checked').html('<i class="fa color-green fa-check-square"></i>');
                } else {
                    held_cheques.find('.current .checked').empty();
                    held_cheques.find('#current-document-box .checked').empty();
                }
            }
        }

        function prevItem() {
            var previousItemIndex = currentItemIndex - 1;

            if(previousItemIndex >= 0){
                currentItemIndex = previousItemIndex;
                displayItem();
            }
        }

        function nextItem() {
            var nextItemIndex = currentItemIndex + 1;

            if(nextItemIndex <= (settings.items.length - 1)) {
                currentItemIndex = nextItemIndex;
                displayItem();
            }
        }

        function scroll() {
            var item = settings.items[currentItemIndex];
            var position = held_cheques.find('#held-cheques-documents-box table [data-document-id=' + item.id + ']').position().top;
            held_cheques.find('#held-cheques-documents-box .cardbox-body').animate({
                scrollTop: position
            }, 0);
        }

        function displayItem() {
            setCurrent();
            setItem();
            scroll();
        }

        function displayImage() {
            var item = settings.items[currentItemIndex];
            if(item.image !== null) {
                image_manipulation_main.attr('data-image-id', item.id).attr('data-orientation', 'front');
                if(Images[item.id].front_filename !== null) {
                    image_manipulation_main.attr('src', Images[item.id].front_filename).attr('data-front-image', Images[item.id].front_filename);
                }
                if(Images[item.id].back_filename !== null) {
                    image_manipulation_main.attr('data-back-image', Images[item.id].back_filename);
                }
            }
        }

        function setCurrent() {
            var item = settings.items[currentItemIndex];
            var current_row = held_cheques.find('#held-cheques-documents-box table [data-document-id=' + item.id + ']');
            held_cheques.find('span.current-document').html(currentItemIndex + 1);
            held_cheques.find('#held-cheques-documents-box table tr.current').removeClass('current');
            current_row.addClass('current');
        }

        function setDocumentRow(index) {
            var item = settings.items[index];
            var row = held_cheques.find('#held-cheques-documents-box table [data-document-id=' + item.id + ']');
            if(item.session_id == settings.previousSessionId) {
                if($.inArray(item.return_reason_1, heldChequesStatuses) !== -1 && $.inArray(item.return_reason_2, heldChequesStatuses) !== -1 && item.return_reason_1 != settings.nextDayDecision) {
                    row.find('.rr1').html(item.return_reason_1);
                    row.find('.rr2').html(item.return_reason_2);
                    row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
                } else {
                    row.find('.rr1').empty();
                    row.find('.rr2').empty();
                    row.find('.checked').empty();
                }
            } else {
                if($.inArray(item.return_reason_1, heldChequesStatuses) !== -1 && $.inArray(item.return_reason_2, heldChequesStatuses) !== -1) {
                    row.find('.rr1').html(item.return_reason_1);
                    row.find('.rr2').html(item.return_reason_2);
                    row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
                } else {
                    row.find('.rr1').empty();
                    row.find('.rr2').empty();
                    row.find('.checked').empty();
                }
            }
        }

        function setProcessed() {
            var processed = settings.items.filter(function (item) {
                if(item.session_id == settings.previousSessionId && item.return_reason_1 != settings.nextDayDecision && item.return_reason_2 != settings.nextDayDecision) {
                    return ($.inArray(item.return_reason_1, heldChequesStatuses) !== -1 && $.inArray(item.return_reason_2, heldChequesStatuses) !== -1);
                }
                if(item.session_id != settings.previousSessionId) {
                    return ($.inArray(item.return_reason_1, heldChequesStatuses) !== -1 && $.inArray(item.return_reason_2, heldChequesStatuses) !== -1);
                }
            }).length;

            $('.processed').html(processed);
            var percentage = (processed * 100) / settings.items.length;
            if(percentage > 70) {
                $('.progress-bar').css('width', percentage + '%').prev().addClass('color-white');
            } else {
                $('.progress-bar').css('width', percentage + '%');
            }
        }

        function saveItem() {
            save_ajax_flag = true;
            var index = currentItemIndex;
            var item = settings.items[currentItemIndex];
            var rr1 = held_cheques.find('#rr1-input').val();
            var rr2 = held_cheques.find('#rr2-input').val();

            if(currentItemIndex === (settings.items.length - 1)) {
                $('#confirm').prop('disabled', true);
            } else {
                $('#confirm').prop('disabled', true);
                nextItem();
            }
            $.ajax({
                url: Parallax.Route.get('held-cheques.archive.save', {item_id: item.id}),
                type: 'PUT',
                data: {rr1: rr1, rr2: rr2},
                dataType: 'json'
            }).done(function (data) {
                save_ajax_flag = false;
                $('#confirm').prop('disabled', false);
                if(data.return === true) {
                    item.return_reason_1 = rr1;
                    item.return_reason_2 = rr2;
                    setProcessed();
                    setDocumentRow(index);
                    toastr.success(data.message,'');
                } else {
                    toastr.error(data.message, '');
                }
            }).fail(function () {
                toastr.options = {"showDuration": 0};
                toastr.error('Unexpected AJAX call error on saveItem().','');
                save_ajax_flag = false;
                $('#confirm').prop('disabled', false);
            })
        }

        return this;
    };

}( jQuery ));
