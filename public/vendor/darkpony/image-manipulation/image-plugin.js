(function( $ ) {

    $.fn.manipulation = function() {
        var allowDrag = 0, initial = {x:0 ,y:0}, cursorDistanceDiff = {x:0, y:0}, lastTranslated = {x:0, y:0}, translated = {x:0, y:0}, zoomed = 1, rotated = 0;

        var mainImg = this.find('.image-manipulation-main');
        var rotateBtn = this.find('.rotate-img-btn');
        var simpleRotateBtn = this.find('.simple-rotate-img-btn');
        var flipBtn = this.find('.flip-img-btn');
        var swapBtn = this.find('.swap-img-btn');
        var resetBtn = this.find('.reset-img-btn');
        var zoomSlider = this.find('.image-zoom-slider').get(0);

        //drag - translate x,y
        mainImg.on("mousedown", function(e){
            e.preventDefault();
            allowDrag = 1;
            initial.x = 0;
            initial.y = 0;
        });
        mainImg.on("mouseup", function(){
            allowDrag = 0;
            initial.x = 0;
            initial.y = 0;
            lastTranslated.x = translated.x;
            lastTranslated.y = translated.y;
            translated.x = 0;
            translated.y = 0;
        });
        mainImg.on("mousemove", function(e){
            if(allowDrag && zoomed>1){
                if(initial.x == 0 || initial.y == 0){
                    initial.x = e.pageX;
                    initial.y = e.pageY;
                }

                cursorDistanceDiff.x = e.pageX - initial.x;
                cursorDistanceDiff.y = e.pageY - initial.y;

                translated.x = lastTranslated.x + cursorDistanceDiff.x;
                translated.y = lastTranslated.y + cursorDistanceDiff.y;

                renderImage(mainImg, translated, zoomed, rotated);
            }
        });

        //slider and zoomedvar zoomSlider = $('#image-zoom-slider').get(0);
        if(zoomSlider !== undefined) {
            noUiSlider.create(zoomSlider, {
                start: 0,
                connect: true,
                range: {
                    'min': 1,
                    'max': 3,
                }
            });
            zoomSlider.noUiSlider.on('update', function(e){
                if(e[0] == 1 || e[0] == 1.00){
                    //reset
                    allowDrag = 0;
                    cursorDistanceDiff = {x:0, y:0};
                    initial = {x:0 ,y:0};
                    lastTranslated = {x:0, y:0};
                    zoomed = 1;
                    rotated = 0;
                    translated = {x:0, y:0}
                    renderImage(mainImg,lastTranslated,zoomed,rotated);
                }else{
                    renderImage(mainImg, lastTranslated, e[0], rotated);
                    zoomed = parseFloat(e[0]);
                }
            });
        }

        //dblclick zoom
        mainImg.on("dblclick", function(){
            zoomed = zoomed + 1;
            if(zoomed > 3){
                zoomed = 3;
            }

            zoomSlider.noUiSlider.set(zoomed);
            renderImage(mainImg, lastTranslated, zoomed, rotated);
        });

        //Swap & Save
        swapBtn.on('click', function () {
            Parallax.Loader.show();
            $.ajax({
                url: Parallax.Route.get('image.swap', {image_id: mainImg.attr('data-image-id')}),
                type: 'PUT',
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    if(Images[mainImg.attr('data-image-id')]) {
                        Images[mainImg.attr('data-image-id')].front_filename = data.front_image;
                        Images[mainImg.attr('data-image-id')].back_filename = data.back_image;
                    }
                    swap(data.front_image, data.back_image);
                    toastr.success(data.message,'');
                } else {
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        });

        //Flip only
        flipBtn.on('click', function () {
            flip();
        });

        //Rotate & Save
        rotateBtn.on('click', function(){
            Parallax.Loader.show();
            $.ajax({
                url: Parallax.Route.get('image.rotate', {image_id: mainImg.attr('data-image-id')}),
                type: 'PUT',
                dataType: 'json'
            }).done(function (data) {
                if(data.return === true) {
                    if(Images[mainImg.attr('data-image-id')]) {
                        Images[mainImg.attr('data-image-id')].front_filename = data.front_image;
                        Images[mainImg.attr('data-image-id')].back_filename = data.back_image;
                    }
                    rotate(data.front_image, data.back_image);
                    toastr.success(data.message,'');
                } else {
                    toastr.error(data.message, '');
                }
                Parallax.Loader.hide();
            });
        });

        //css simple rotate
        simpleRotateBtn.on('click', function(){
            rotated = rotated + 180;
            renderImage(mainImg, lastTranslated, zoomed, rotated);
        });

        //reset
        resetBtn.on('click', function(){
            zoomSlider.noUiSlider.set(1);
            //reset
            allowDrag = 0;
            cursorDistanceDiff = {x:0, y:0};
            initial = {x:0 ,y:0};
            lastTranslated = {x:0, y:0};
            zoomed = 1;
            rotated = 0;
            translated = {x:0, y:0};
            renderImage(mainImg,lastTranslated,zoomed,rotated);
        });

        //Rotate an image and update the urls with the new versions.
        function rotate(front_image, back_image) {
            if(mainImg.attr('data-orientation') === 'front') {
                mainImg.attr('src', front_image).attr('data-orientation', 'front');
            } else {
                mainImg.attr('src', back_image).attr('data-orientation', 'back');
            }
            mainImg.attr('data-front-image', front_image).attr('data-back-image', back_image);
        }


        //Swap an image and update the urls with the new versions.
        function swap(front_image, back_image) {
            if(mainImg.attr('data-orientation') === 'front') {
                mainImg.attr('src', front_image).attr('data-orientation', 'front');
            } else {
                mainImg.attr('src', back_image).attr('data-orientation', 'back');
            }
            mainImg.attr('data-front-image', front_image).attr('data-back-image', back_image);
        }

        //Flip an image.
        function flip() {
            if(mainImg.attr('data-orientation') === 'front') {
                mainImg.attr('src', mainImg.attr('data-back-image')).attr('data-orientation', 'back');
            } else {
                mainImg.attr('src', mainImg.attr('data-front-image')).attr('data-orientation', 'front');
            }
        }

        return this;

    };

    //render function
    function renderImage(mainImg, translate, zoom, rotate){
        mainImg.css({
            '-webkit-transform' : 'translate(' + translate.x + 'px,' + translate.y + 'px) scale('+zoom+') rotate('+rotate+'deg)',
            '-moz-transform'    : 'translate(' + translate.x + 'px,' + translate.y + 'px) scale('+zoom+') rotate('+rotate+'deg)',
            '-ms-transform'     : 'translate(' + translate.x + 'px,' + translate.y + 'px) scale('+zoom+') rotate('+rotate+'deg)',
            '-o-transform'      : 'translate(' + translate.x + 'px,' + translate.y + 'px) scale('+zoom+') rotate('+rotate+'deg)',
            'transform'         : 'translate(' + translate.x + 'px,' + translate.y + 'px) scale('+zoom+') rotate('+rotate+'deg)'
        });
    }

}( jQuery ));
