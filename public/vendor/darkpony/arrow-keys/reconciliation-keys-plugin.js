(function( $ ) {

    $.fn.keys = function() {
        var current = 0;
        var rows = this.find('tbody tr');
        var length = rows.length - 1;
        var table = this;
        var trHeight = this.find('tbody tr').height();

        this.currentIndex = function (value) {
            current = value;
        };

        this.off = function () {
            $(document).off('keydown');
        };

        this.on = function () {
            $(document).on('keydown', function(e){
                if(e.keyCode === 40){ //down
                    e.preventDefault();
                    current++;
                    if(current > length) {
                        current = 0;
                    }
                    $(this).find('tr[tabindex='+current+'] td:first').click();
                    $(table).scrollTop(parseInt($(table).find('tr[tabindex='+current+']').attr('tabindex'))*trHeight);
                }
                if(e.keyCode === 38){ //up
                    e.preventDefault();
                    current--;
                    if (current < 0) {
                        current = length;
                    }
                    $(this).find('tr[tabindex='+current+'] td:first').click();
                    $(table).scrollTop(parseInt($(table).find('tr[tabindex='+current+']').attr('tabindex'))*trHeight);
                }
            });
        };

        this.on();

        return this;
    };
}( jQuery ));