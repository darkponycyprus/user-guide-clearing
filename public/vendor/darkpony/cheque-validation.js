/**
 * Cheque JavaScript Validation.
 */
class ChequeValidation {

    /**
     * Check if the cheque is valid.
     *
     * @param cheque
     * @returns {boolean}
     */
    isValid(cheque) {
        return (!this.checkDigit1CodeLineRules(cheque) || !this.checkDigit2CodeLineRules(cheque));
    }

    /**
     * Check if it has a valid trans code.
     *
     * @param cheque
     * @returns {boolean}
     */
    hasValidTransCode(cheque) {
        return this.inArray(cheque.trans_code, Options.cheque_validation_rules.trans_codes);
    }

    /**
     * Check if it has a valid bank code.
     *
     * @param cheque
     * @returns {boolean}
     */
    hasValidBankCode(cheque) {
        return this.inArray(cheque.bank_code, Options.valid_banks);
    }

    /**
     * Check if the code line until check digit 1 is valid.
     *
     * @param cheque
     * @returns {boolean}
     */
    hasValidCheckDigit1CodeLine(cheque) {
        return this.checkDigit1CodeLineRules(cheque);
    }

    /**
     * Check if the code line until check digit 2 is valid.
     *
     * @param item
     * @returns {boolean}
     */
    hasValidCheckDigit2CodeLine(item) {
        return this.checkDigit2CodeLineRules(item);
    }

    /**
     * Set the rules for the code line until check digit 1.
     *
     * @param cheque
     */
    checkDigit1CodeLineRules(cheque) {
        if (!this.isValidTrasCode(cheque.trans_code)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_trans_code'));
            return false;
        } else {
            if(!this.isNumber(cheque.trans_code)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_trans_code'));
                return false;
            }
        }

        if (!this.isValidChequeNo(cheque.cheque_no)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_cheque_no'));
            return false;
        } else {
            if(!this.isNumber(cheque.cheque_no)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_cheque_no'));
                return false;
            }
        }

        if (!this.isValidBankCode(cheque.bank_code)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_bank_code'));
            return false;
        } else {
            if(!this.isNumber(cheque.bank_code)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_bank_code'));
                return false;
            }
        }

        if (!this.isValidChequeAccountNo(cheque.cheque_account_no)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_cheque_account_no'));
            return false;
        } else {
            if(!this.isNumber(cheque.cheque_account_no)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_cheque_account_no'));
                return false;
            }
        }

        if (!this.isValidCheckDigit1(cheque.check_digit_1)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_check_digit_1'));
            return false;
        } else {
            if(!this.isNumber(cheque.check_digit_1)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_check_digit_1'));
                return false;
            }
        }

        if (!this.isValidCheckDigit1Luhn(cheque.trans_code, cheque.cheque_no, cheque.bank_code, cheque.cheque_account_no, cheque.check_digit_1)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.wrong_check_digit_1'));
            return false;
        }

        return true;
    }

    /**
     * Set the rules for the code line until check digit 2.
     *
     * @param cheque
     */
    checkDigit2CodeLineRules(cheque) {
        //Check if the amount is null, empty string or a negative value.
        if (!this.isValidAmount(cheque.amount)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_amount'));
            return false;
        } else {
            if(!this.isNumber(cheque.amount)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_amount'));
                return false;
            }
        }

        if (!this.isValidCheckDigit2(cheque.check_digit_2)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.empty_check_digit_2'));
            return false;
        } else {
            if(!this.isNumber(cheque.check_digit_2)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.not_numeric_check_digit_2'));
                return false;
            }
        }

        if (!this.isValidCheckDigit2Luhn(cheque.amount, cheque.check_digit_2)) {
            toastr.error(Parallax.Lang.get('reconciliation.validation.wrong_check_digit_2'));
            return false;
        }

        return true;
    }

    /**
     * Calculate and check the check digit 1 Luhn.
     *
     * @param trans_code
     * @param cheque_no
     * @param bank_code
     * @param cheque_account_no
     * @param check_digit_1
     * @returns {boolean}
     */
    isValidCheckDigit1Luhn(trans_code, cheque_no, bank_code, cheque_account_no, check_digit_1) {
        if (Luhn.calculateLuhn(
                this.pad(trans_code, Options.paddings.trans_code) +
                this.pad(cheque_no, Options.paddings.cheque_no) +
                this.pad(bank_code, Options.paddings.bank_code) +
                this.pad(cheque_account_no, Options.paddings.cheque_account_no)) !== parseInt(check_digit_1)) {
            return false;
        }

        return true;
    }

    /**
     * Calculate and check the check digit 2 Luhn.
     *
     * @param amount
     * @param check_digit_2
     * @returns {boolean}
     */
    isValidCheckDigit2Luhn(amount, check_digit_2) {
        if (Luhn.calculateLuhn(this.pad(amount, Options.paddings.amount)) !== parseInt(check_digit_2)) {
            return false;
        }
        return true;
    }

    //Check if the Trans Code is null or an empty string.
    isValidTrasCode(trans_code) {
        if(trans_code === null || trans_code === '') {
            return false;
        }
        return true;
    }

    //Check if the Cheque No is null or an empty string.
    isValidChequeNo(cheque_no) {
        if(cheque_no === null || cheque_no === '') {
            return false;
        }
        return true;
    }

    //Check if the Bank Code is null or an empty string.
    isValidBankCode(bank_code) {
        if(bank_code === null || bank_code === '') {
            return false;
        }
        return true;
    }

    //Check if the Cheque Account No is null or an empty string.
    isValidChequeAccountNo(cheque_account_no) {
        if(cheque_account_no === null || cheque_account_no === '') {
            return false;
        }
        return true;
    }

    //Check if the Check Digit 1 is null or an empty string and if the Check Digit 1 has any other number except between 0 and 9.
    isValidCheckDigit1(cd1) {
        if(cd1 === null || cd1 === '' || parseInt(cd1) < 0 || parseInt(cd1) > 9) {
            return false;
        }
        return true;
    }

    //Check if the Amount is null, an empty string or has a negative value.
    isValidAmount(amount) {
        if(amount === null || amount === '' || amount <= 0) {
            return false;
        }
        return true;
    }

    //Check if the Check Digit 2 is null or an empty string and if the Check Digit 2 has any other number except between 0 and 9.
    isValidCheckDigit2(cd2) {
        if(cd2 === null || cd2 === '' || parseInt(cd2) < 0 || parseInt(cd2) > 9) {
            return false;
        }
        return true;
    }

    //Check if the string is not a number.
    isNumber(string) {
        if(isNaN(string)) {
            return false;
        }
        return true;
    }

    //Check if the deposit date is correct.
    canDeposit(date) {
        var deposit_date = moment.utc(date, 'DD/MM/YYYY', true);

        if(!this.isValidDate(deposit_date)) {
            return false;
        }

        var today = moment.utc(new Date());

        var duration = moment.duration(today.diff(deposit_date));

        if(duration.asMonths() > parseFloat(Configurations.js_validations.max_deposit_date_months)) {
            return false;
        }

        return true;
    }

    //Check if the date is correct.
    isValidDate(date_string) {
        var date = moment.utc(date_string, 'DD/MM/YYYY', true);

        return date.isValid();
    }

    /**
     * Padding left with "0"
     *
     * @param str
     * @param max
     * @returns {*}
     */
    pad(str, max) {
        str = str.toString();
        return str.length < max ? this.pad("0" + str, max) : str;
    }

    inArray(needle,haystack)
    {
        var count=haystack.length;
        for(var i=0;i<count;i++)
        {
            if(haystack[i]===needle){return true;}
        }
        return false;
    }
}