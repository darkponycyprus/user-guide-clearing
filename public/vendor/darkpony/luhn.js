/**
 * Luhn validation for cheques.
 */
class Luhn {
    /**
     * Calculate Luhn value.
     *
     * @param originalStr
     * @returns {number}
     */
    static calculateLuhn(originalStr) {
        var sum = 0, delta = [0, 1, 2, 3, 4, -4, -3, -2, -1, 0];

        for (var i = 0; i < originalStr.length; i++) {
            sum += parseInt(originalStr.substring(i, i + 1));
        }

        for (var i = originalStr.length - 1; i >= 0; i -= 2) {
            sum += delta[parseInt(originalStr.substring(i, i + 1))];
        }

        if (10 - (sum % 10) === 10) {
            return 0;
        }
        return 10 - (sum % 10);
    }

    /**
     * Validate Luhn string.
     *
     * @param luhnStr
     * @returns {boolean}
     */
    static validateLuhn(luhnStr) {
        var luhnStrDigit, luhnStrLess;

        luhnStrDigit = parseInt(luhnStr.substring(luhnStr.length - 1, luhnStr.length));
        luhnStrLess = luhnStr.substring(0, luhnStr.length - 1);

        return (this.calculate(luhnStrLess) === parseInt(luhnStrDigit));
    }
}