"use strict";
var Locale = /** @class */ (function () {
    function Locale(messages) {
        this.messages = [];
        this.messages = messages;
    }
    Locale.prototype.lmsg = function (key, params) {
        var value = this.messages[key];
        if ('undefined' === typeof value) {
            return '[' + key + ']';
        }
        if ('undefined' === typeof params) {
            return value;
        }
        return Object.keys(params).reduce(function (value, paramKey) {
            return value.replace('%%' + paramKey + '%%', params[paramKey]);
        }, value);
    };
    return Locale;
}());
var TaskProgressBar = /** @class */ (function () {
    function TaskProgressBar(id, elem, config) {
        this.itemsReady = false;
        this.items = [];
        this.id = id;
        this.elem = elem;
        this.config = config;
        this.locale = new Locale(this.getConfigParam('locale'));
        this.contentAreaId = this.id + '-content-area';
        this.componentElement = $('#' + this.id);
        this.update();
    }
    TaskProgressBar.prototype.getConfigParam = function (name, defaultvalue) {
        return this.hasConfigParam(name) ? this.config[name] : defaultvalue;
    };
    TaskProgressBar.prototype.hasConfigParam = function (name) {
        return 'undefined' !== typeof this.config[name];
    };
    TaskProgressBar.prototype.update = function () {
        var _this = this;
        $.ajax({
            url: this.getConfigParam('progressUrl'),
            dataType: 'json',
            success: function (data) {
                _this.setItems(data.items);
                _this.itemsReady = true;
                _this.renderItems();
                _this.addEvents();
            }
        });
    };
    TaskProgressBar.prototype.getItems = function () {
        return this.items;
    };
    TaskProgressBar.prototype.getItem = function (id) {
        return this.getItems().find(function (item) {
            return item.getId() === id;
        });
    };
    TaskProgressBar.prototype.checkPreviousStatus = function (newItems) {
        var _this = this;
        newItems.forEach(function (newItem) {
            var item = _this.getItem(newItem.id);
            if (item) {
                if (newItem.status !== item.status) {
                    TaskProgressBar.onItemStatusChange(newItem);
                }
            }
            else {
            }
        });
    };
    TaskProgressBar.onItemStatusChange = function (newItem) {
        var isComplete = function (status) {
            return ['finished', 'failed'].indexOf(status) !== -1;
        };
        if (isComplete(newItem.status)) {
        }
    };
    TaskProgressBar.prototype.initItems = function (items) {
        this.items = items.filter(function (item) {
            return !!item;
        }).map(function (item) {
            return new TaskProgressBarItem(item);
        });
    };
    TaskProgressBar.prototype.setItems = function (items) {
        this.initItems(items);
    };
    TaskProgressBar.prototype.removeCompletedTask = function () {
        var _this = this;
        var ids = this.items.filter(function (item) {
            return !(item.isQueued() || item.isExecuting());
        }).map(function (item) {
            return item.getId();
        });
        if (ids.length > 0) {
            $.ajax({
                url: this.getConfigParam('removeUrl'),
                dataType: 'json',
                method: 'post',
                data: { 'ids[]': ids },
                success: function (data) {
                    _this.removeItemsByIds(ids);
                }
            });
        }
    };
    TaskProgressBar.prototype.renderItems = function () {
        var _this = this;
        this.updateTitle();
        if (this.items.length) {
            this.items.forEach(function (item) {
                _this.renderItem(item);
            });
            this.componentElement.removeClass('d-none');
        }
        else {
            this.componentElement.addClass('d-none');
        }
    };
    TaskProgressBar.prototype.renderItem = function (item) {
        var renderTargetId = this.id + '-item-' + item.getId();
        if (!$('#' + renderTargetId).length) {
            $('#' + this.contentAreaId).prepend('<li id="' + renderTargetId + '" class="async-progress-bar-item"></li>');
        }
        item.setProgressBarElement(this);
        item.setRenderTarget($('#' + renderTargetId));
        item.render();
    };
    TaskProgressBar.prototype.removeItemsByIds = function (ids) {
        if (!ids.length) {
            return;
        }
        var items = this.getItems();
        for (var i = 0; i < ids.length; i++) {
            for (var j = 0; j < items.length; j++) {
                if (items[j].getId() === ids[i]) {
                    items[j].getRenderTarget().remove();
                    items.splice(j, 1);
                    break;
                }
            }
        }
        this.setItems(items);
        this.renderItems();
    };
    TaskProgressBar.prototype.toggle = function () {
        var element = $('#asyncProgressBar');
        element.toggleClass('async-progress-bar-collapsed');
        this.updateTitle();
    };
    TaskProgressBar.prototype.updateTitle = function () {
        var countRunning = 0;
        var countCompleteError = 0;
        var countCompleteSuccessfully = 0;
        this.items.forEach(function (item) {
            if (item.isFinished()) {
                countCompleteSuccessfully++;
            }
            else if (item.isFailed()) {
                countCompleteError++;
            }
            else {
                countRunning++;
            }
        });
        var taskRunningElement = $('#asyncProgressBarTitleTasks');
        if (countRunning > 0) {
            if (countRunning === this.items.length) {
                taskRunningElement.html(this.locale.lmsg('taskInProgress', { count: countRunning }));
            }
            else {
                taskRunningElement.html(String(countRunning));
            }
            $('#asyncProgressBar').removeClass('async-progress-bar-complete');
            taskRunningElement.removeClass('d-none');
        }
        else {
            $('#asyncProgressBar').addClass('async-progress-bar-complete');
            taskRunningElement.addClass('d-none');
        }
        if (countCompleteSuccessfully > 0 || countCompleteError > 0) {
            $('#asyncProgressBarHideCompletedTasks').removeClass('d-none');
        }
        else {
            $('#asyncProgressBarHideCompletedTasks').addClass('d-none');
        }
        this.updateTaskTitleElement('#asyncProgressBarTitleTasksError', countCompleteError);
        this.updateTaskTitleElement('#asyncProgressBarTitleTasksComplete', $('#asyncProgressBar').hasClass('async-progress-bar-collapsed') && countCompleteSuccessfully === this.items.length ? this.locale.lmsg('allTasksCompleted', { num: countCompleteSuccessfully }) : countCompleteSuccessfully);
    };
    TaskProgressBar.prototype.updateTaskTitleElement = function (id, title) {
        if (!this.itemsReady) {
            return;
        }
        var element = $(id);
        element.html(String(title));
        element.toggleClass('d-none', title === 0);
    };
    TaskProgressBar.prototype.addEvents = function () {
        var _this = this;
        $('#asyncProgressBarTop').on('click', function (e) {
            e.preventDefault();
            _this.toggle();
        });
        $('#asyncProgressBarHideCompletedTasks').on('click', function (e) {
            e.preventDefault();
            _this.removeCompletedTask();
        });
    };
    return TaskProgressBar;
}());
var TaskProgressBarItem = /** @class */ (function () {
    function TaskProgressBarItem(item) {
        this.STATUS_QUEUED = 'queued';
        this.STATUS_EXECUTING = 'executing';
        this.STATUS_FINISHED = 'finished';
        this.STATUS_FAILED = 'failed';
        this.status = '';
        this.title = '';
        this.currentProgress = 0;
        this.totalProgress = 0;
        this.percentageProgress = 0;
        this.id = item.id;
        if (item instanceof TaskProgressBarItem) {
            return item;
        }
        this.status = item.status;
        this.totalProgress = item.total_progress;
        this.currentProgress = item.current_progress;
        this.percentageProgress = item.progress_percentage;
        this.options = item.options;
    }
    TaskProgressBarItem.prototype.setRenderTarget = function (target) {
        this.renderTarget = target;
    };
    TaskProgressBarItem.prototype.getRenderTarget = function () {
        return this.renderTarget;
    };
    TaskProgressBarItem.prototype.render = function () {
        var progressBar = '';
        var element = this.getRenderTarget();
        if (this.isFinished()) {
            element.addClass('async-progress-bar-item-complete');
            progressBar =
                '<div class="async-progress-bar-item-wrap">' +
                    '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent() +
                    '<div class="async-progress-bar-item-msg-body">' + this.getProgressTitle() +
                    '<div><a href="' + this.options.redirect.url + '">' + this.options.redirect.title + '</a></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
        }
        else if (this.isFailed()) {
            element.addClass('async-progress-bar-item-error');
            progressBar =
                '<div class="async-progress-bar-item-wrap">' +
                    '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent() +
                    '<div class="async-progress-bar-item-msg-body">' + this.getProgressTitle() +
                    '<div><a href="' + this.options.redirect.url + '">' + this.options.redirect.title + '</a></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            //progressBar = ce('.async-progress-bar-item-msg', [this.getCloseButton(), ce('.async-progress-bar-item-msg-body', [this.getProgressTitle(), this.getErrorsString(), this.getRefreshLink()])]);
        }
        else {
            progressBar =
                '<div class="async-progress-bar-item-wrap">' +
                    '<div class="async-progress-bar-item-heading">' +
                    '<div class="async-progress-bar-item-title">' + this.getProgressTitle() + '</div>' +
                    '<div class="progress progress-sm">' +
                    '<div style="width:' + this.getPercentageProgress() + '%" class="progress-bar progress-bar-striped active"></div>' +
                    '</div>' +
                    '<div class="async-progress-bar-item-footer">' +
                    '<div class="async-progress-bar-item-control text-right">' + this.getCurrentProgress() + '/' + this.getTotalProgress() + '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            //progressBar = [ce('.async-progress-bar-item-heading', [ce('.async-progress-bar-item-control', this.getCancelButton()), ce('.async-progress-bar-item-title', this.getProgressTitle())]), ce('.progress.progress-sm', ce('.progress-bar', { style: 'width: ' + this.getProgressValue() + '%' })), ce('.async-progress-bar-item-footer', [this.getProgressDialogLink(), this.lmsg('percentCompleted', { percent: this.getProgressValue() })])];
        }
        element.html(progressBar);
    };
    TaskProgressBarItem.prototype.closeButtonEvent = function () {
        var _this = this;
        $(document).on('click', '#item-' + this.id, function (e) {
            e.preventDefault();
            _this.remove();
        });
        return '<a id="item-' + this.id + '" href="#"><span class="close"></span></a>';
    };
    TaskProgressBarItem.prototype.getProgressTitle = function () {
        return 'Validate & Lock';
    };
    TaskProgressBarItem.prototype.setProgressBarElement = function (progressBarElement) {
        this.progressBarElement = progressBarElement;
    };
    TaskProgressBarItem.prototype.getProgressBarElement = function () {
        return this.progressBarElement;
    };
    TaskProgressBarItem.prototype.isQueued = function () {
        return this.STATUS_QUEUED === this.status;
    };
    TaskProgressBarItem.prototype.isFinished = function () {
        return this.STATUS_FINISHED === this.status;
    };
    TaskProgressBarItem.prototype.isExecuting = function () {
        return this.STATUS_EXECUTING === this.status;
    };
    TaskProgressBarItem.prototype.isFailed = function () {
        return this.STATUS_FAILED === this.status;
    };
    TaskProgressBarItem.prototype.getCurrentProgress = function () {
        return this.currentProgress;
    };
    TaskProgressBarItem.prototype.getTotalProgress = function () {
        return this.totalProgress;
    };
    TaskProgressBarItem.prototype.getPercentageProgress = function () {
        return this.percentageProgress;
    };
    TaskProgressBarItem.prototype.getId = function () {
        return this.id;
    };
    TaskProgressBarItem.prototype.remove = function () {
        var _this = this;
        $.ajax({
            url: this.getProgressBarElement().getConfigParam('removeUrl'),
            dataType: 'json',
            method: 'post',
            success: function () {
                _this.getProgressBarElement().removeItemsByIds([_this.getId()]);
            }
        });
    };
    return TaskProgressBarItem;
}());
