class Locale
{
    public messages = [];

    constructor(messages: Array<never>) {
        this.messages = messages;
    }

    public lmsg(key: any, params: any) {

        let value = this.messages[key];

        if('undefined' === typeof value) {
            return '[' + key + ']';
        }

        if ('undefined' === typeof params) {
            return value;
        }

        return Object.keys(params).reduce(function (value, paramKey: any) {
            return value.replace('%%' + paramKey + '%%', params[paramKey]);
        }, value);
    }
}

class TaskProgressBar
{
    private id: string;

    private elem: HTMLElement;

    private config: any;

    private itemsReady: boolean = false;

    protected locale: Locale;

    protected items: TaskProgressBarItem[] = [];

    protected componentElement!: JQuery<HTMLElement>;

    protected contentAreaId: string;

    constructor(id: string, elem: HTMLElement, config: string) {
        this.id = id;

        this.elem = elem;

        this.config = config;

        this.locale = new Locale(this.getConfigParam('locale'));

        this.contentAreaId = this.id + '-content-area';

        this.componentElement = $('#' + this.id);

        this.update();
    }

    getConfigParam(name: string, defaultvalue?: string) {
        return this.hasConfigParam(name) ? this.config[name] : defaultvalue;
    }

    hasConfigParam(name: string) {
        return 'undefined' !== typeof this.config[name];
    }

    update() {
        $.ajax({
            url: this.getConfigParam('progressUrl'),
            dataType: 'json',
            success: (data) => {
                this.setItems(data.items);
                this.itemsReady = true;
                this.renderItems();
                this.addEvents();
            }
        });
    }

    getItems() {
        return this.items;
    }

    getItem(id: string) {
        return this.getItems().find((item: TaskProgressBarItem) => {
            return item.getId() === id;
        });
    }

    checkPreviousStatus(newItems: Array<any>) {
        newItems.forEach((newItem: any) => {
            let item = this.getItem(newItem.id);
            if(item) {
                if (newItem.status !== item.status) {
                    TaskProgressBar.onItemStatusChange(newItem);
                }
            } else {

            }
        });
    }

    public static onItemStatusChange(newItem: any) {
        let isComplete = function (status: string) {
            return ['finished', 'failed'].indexOf(status) !== -1;
        };
        if (isComplete(newItem.status)) {
            
        }
    }

    initItems(items: Array<any>) {
        this.items = items.filter(function (item) {
            return !!item;
        }).map(function (item) {
            return new TaskProgressBarItem(item);
        })
    }

    setItems(items: Array<any>) {
        this.initItems(items);
    }

    removeCompletedTask() {
        let ids = this.items.filter((item) => {
            return !(item.isQueued() || item.isExecuting());
        }).map((item) => {
            return item.getId();
        });

        if (ids.length > 0) {
            $.ajax({
                url: this.getConfigParam('removeUrl'),
                dataType: 'json',
                method: 'post',
                data: { 'ids[]': ids },
                success: (data) => {
                    this.removeItemsByIds(ids);
                }
            });
        }
    }

    renderItems() {
        this.updateTitle();
        if(this.items.length) {
            this.items.forEach((item: TaskProgressBarItem) => {
                this.renderItem(item);
            });
            this.componentElement.removeClass('d-none');
        } else {
            this.componentElement.addClass('d-none');
        }

    }

    renderItem(item: TaskProgressBarItem) {
        let renderTargetId = this.id + '-item-' + item.getId();
        if (!$('#' + renderTargetId).length) {
            $('#' + this.contentAreaId).prepend('<li id="' + renderTargetId + '" class="async-progress-bar-item"></li>');
        }
        item.setProgressBarElement(this);
        item.setRenderTarget($('#' + renderTargetId));
        item.render();
    }

    removeItemsByIds(ids: Array<string>) {
        if (!ids.length) {
            return;
        }

        let items = this.getItems();
        for (var i = 0; i < ids.length; i++) {
            for (var j = 0; j < items.length; j++) {
                if (items[j].getId() === ids[i]) {
                    items[j].getRenderTarget().remove();
                    items.splice(j, 1);
                    break;
                }
            }
        }

        this.setItems(items);
        this.renderItems();
    }

    toggle() {
        let element = $('#asyncProgressBar');
        element.toggleClass('async-progress-bar-collapsed');
        this.updateTitle();
    }

    updateTitle() {
        let countRunning = 0;
        let countCompleteError = 0;
        let countCompleteSuccessfully = 0;

        this.items.forEach(function (item: TaskProgressBarItem) {
            if (item.isFinished()) {
                countCompleteSuccessfully++;
            } else if (item.isFailed()) {
                countCompleteError++;
            } else {
                countRunning++;
            }
        });

        let taskRunningElement = $('#asyncProgressBarTitleTasks');

        if (countRunning > 0) {
            if (countRunning === this.items.length) {
                taskRunningElement.html(this.locale.lmsg('taskInProgress', { count: countRunning }));
            } else {
                taskRunningElement.html(String(countRunning));
            }
            $('#asyncProgressBar').removeClass('async-progress-bar-complete');
            taskRunningElement.removeClass('d-none');
        } else {
            $('#asyncProgressBar').addClass('async-progress-bar-complete');
            taskRunningElement.addClass('d-none');
        }
        if (countCompleteSuccessfully > 0 || countCompleteError > 0) {
            $('#asyncProgressBarHideCompletedTasks').removeClass('d-none');
        } else {
            $('#asyncProgressBarHideCompletedTasks').addClass('d-none');
        }

        this.updateTaskTitleElement('#asyncProgressBarTitleTasksError', countCompleteError);
        this.updateTaskTitleElement('#asyncProgressBarTitleTasksComplete', $('#asyncProgressBar').hasClass('async-progress-bar-collapsed') && countCompleteSuccessfully === this.items.length ? this.locale.lmsg('allTasksCompleted', { num: countCompleteSuccessfully }) : countCompleteSuccessfully);
    }

    updateTaskTitleElement(id: string, title: number | string) {
        if (!this.itemsReady) {
            return;
        }

        let element = $(id);

        element.html(String(title));
        element.toggleClass('d-none', title === 0);
    }

    addEvents () {
        $('#asyncProgressBarTop').on('click', (e) => {
            e.preventDefault();
            this.toggle();
        });
        $('#asyncProgressBarHideCompletedTasks').on('click', (e) => {
            e.preventDefault();
            this.removeCompletedTask();
        });
    }
}

class TaskProgressBarItem {

    private readonly STATUS_QUEUED = 'queued';
    private readonly STATUS_EXECUTING = 'executing';
    private readonly STATUS_FINISHED = 'finished';
    private readonly STATUS_FAILED = 'failed';

    public id: string;

    public status: string = '';

    public title: string = '';

    public currentProgress: number | string = 0;

    public totalProgress: number | string = 0;

    public percentageProgress: number | string = 0;

    private progressBarElement!: TaskProgressBar;

    private options: any;

    public renderTarget!: JQuery<HTMLElement>;

    constructor(item: any) {
        this.id = item.id;

        if(item instanceof TaskProgressBarItem) {
            return item;
        }

        this.status = item.status;
        this.totalProgress = item.total_progress;
        this.currentProgress = item.current_progress;
        this.percentageProgress = item.progress_percentage;
        this.options = item.options;
    }

    setRenderTarget(target: JQuery<HTMLElement>) {
        this.renderTarget = target;
    }

    getRenderTarget() {
        return this.renderTarget;
    }

    render() {
        let progressBar = '';
        let element = this.getRenderTarget();

        if (this.isFinished()) {
            element.addClass('async-progress-bar-item-complete');
            progressBar =
                '<div class="async-progress-bar-item-wrap">' +
                    '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent() +
                        '<div class="async-progress-bar-item-msg-body">' + this.getProgressTitle() +
                            '<div><a href="' + this.options.redirect.url + '">' + this.options.redirect.title + '</a></div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        } else if (this.isFailed()) {
            element.addClass('async-progress-bar-item-error');
            progressBar =
            '<div class="async-progress-bar-item-wrap">' +
                '<div class="async-progress-bar-item-msg">' + this.closeButtonEvent() +
                    '<div class="async-progress-bar-item-msg-body">' + this.getProgressTitle() +
                        '<div><a href="' + this.options.redirect.url + '">' + this.options.redirect.title + '</a></div>' +
                    '</div>' +
                '</div>' +
            '</div>';
            //progressBar = ce('.async-progress-bar-item-msg', [this.getCloseButton(), ce('.async-progress-bar-item-msg-body', [this.getProgressTitle(), this.getErrorsString(), this.getRefreshLink()])]);
        } else {
            progressBar =
                '<div class="async-progress-bar-item-wrap">' +
                    '<div class="async-progress-bar-item-heading">' +
                        '<div class="async-progress-bar-item-title">' + this.getProgressTitle() + '</div>' +
                        '<div class="progress progress-sm">' +
                            '<div style="width:' + this.getPercentageProgress() + '%" class="progress-bar progress-bar-striped active"></div>' +
                        '</div>' +
                        '<div class="async-progress-bar-item-footer">' +
                            '<div class="async-progress-bar-item-control text-right">' + this.getCurrentProgress() + '/' + this.getTotalProgress() + '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
            //progressBar = [ce('.async-progress-bar-item-heading', [ce('.async-progress-bar-item-control', this.getCancelButton()), ce('.async-progress-bar-item-title', this.getProgressTitle())]), ce('.progress.progress-sm', ce('.progress-bar', { style: 'width: ' + this.getProgressValue() + '%' })), ce('.async-progress-bar-item-footer', [this.getProgressDialogLink(), this.lmsg('percentCompleted', { percent: this.getProgressValue() })])];
        }
        element.html(progressBar);
    }

    closeButtonEvent() {
        $(document).on('click', '#item-' + this.id, (e) => {
            e.preventDefault();
            this.remove();
        });
        return '<a id="item-'+ this.id +'" href="#"><span class="close"></span></a>';
    }

    getProgressTitle() {
        return 'Validate & Lock';
    }

    setProgressBarElement(progressBarElement: TaskProgressBar) {
        this.progressBarElement = progressBarElement;
    }

    getProgressBarElement() {
        return this.progressBarElement;
    }

    isQueued() {
        return this.STATUS_QUEUED === this.status;
    }

    isFinished() {
        return this.STATUS_FINISHED === this.status;
    }

    isExecuting() {
        return this.STATUS_EXECUTING === this.status;
    }

    isFailed() {
        return this.STATUS_FAILED === this.status;
    }

    getCurrentProgress() {
        return this.currentProgress;
    }

    getTotalProgress() {
        return this.totalProgress;
    }

    getPercentageProgress() {
        return this.percentageProgress;
    }

    getId() {
        return this.id;
    }

    remove() {
        $.ajax({
            url: this.getProgressBarElement().getConfigParam('removeUrl'),
            dataType: 'json',
            method: 'post',
            success: () => {
                this.getProgressBarElement().removeItemsByIds([this.getId()]);
            }
        });
    }
}