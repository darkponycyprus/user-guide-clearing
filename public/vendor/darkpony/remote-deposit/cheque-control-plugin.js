(function( $ ) {

    $.fn.chequeControl = function(options) {
        var cheque_control = this;
        var image_manipulation_main = cheque_control.find('.image-manipulation-main');
        var settings = $.extend({
            items: {},
            chequeControlStatuses: {},
        }, options );

        var currentItemIndex = 0;

        if(settings.items.some(function (item) {
            return (item.return_reason_1 == null);
        })) {
            currentItemIndex = settings.items.findIndex(function (item) {
                return (item.return_reason_1 == null);
            });
        }

        settings.items.forEach(function (item) {
            Images[item.id] = item.image;
        });

        cheque_control.on('click', 'tr[data-document-id]', function () {
            var id = $(this).attr('data-document-id');
            currentItemIndex = settings.items.findIndex(function (item) {
                return item.id == id;
            });
            displayItem();
            displayImage();
        });

        displayImage();

        displayItem();

        enableKeyDown();

        enableConfirmClick();

        function enableConfirmClick() {
            $('#confirm').on("click", function () {
                if(currentItemIndex <= (settings.items.length - 1)) {
                    saveItem();
                    displayImage();
                }
            }).prop('disabled', false);
        }

        function enableKeyDown() {
            $(document).on('keydown', function(e) {
                var keyCode = e.keyCode || e.which;
                //Enter key was pressed.
                if (keyCode === 13) {
                    if(cheque_control.find('input[name="filter_search"]').is(":focus")) {
                        return;
                    }
                    e.preventDefault();
                    if(currentItemIndex <= (settings.items.length - 1)) {
                        saveItem();
                    }
                }
                //Up key was pressed.
                if(keyCode === 38) {
                    e.preventDefault();
                    if(currentItemIndex !== 0) {
                        prevItem();
                    }
                }
                //Down key was pressed.
                if(keyCode === 40) {
                    e.preventDefault();
                    if(currentItemIndex !== (settings.items.length - 1)) {
                        nextItem();
                    }
                }

            }).on('keyup', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    if(cheque_control.find('input[name="filter_search"]').is(":focus")) {
                        return;
                    }
                    e.preventDefault();
                    if(currentItemIndex <= (settings.items.length - 1)) {
                        displayImage();
                    }
                }
                //Up key was pressed.
                if (keyCode === 38) {
                    e.preventDefault();
                    displayImage();
                }
                //Down key was pressed.
                if (keyCode === 40) {
                    e.preventDefault();
                    displayImage();
                }
            })
        }

        function setItem() {
            var item = settings.items[currentItemIndex];

            cheque_control.find('.position').html(currentItemIndex + 1);
            cheque_control.find('.cheque_no').html(item.cheque_no);
            cheque_control.find('.cheque_account_no').html(item.cheque_account_no);
            cheque_control.find('.amount').html(item.amount);
            cheque_control.find('.current .rr1').html(item.return_reason_1);
            if(item.return_reason_1 !== null && item.return_reason_1 !== '') {
                cheque_control.find('.current-checked').html('<i class="fa color-green fa-check-square"></i>');
            } else {
                cheque_control.find('.current-checked').html('');
            }

            var rr1ChequeControlOptions = settings.chequeControlStatuses.map(function (obj) {
                if(obj.code == '00' && item.return_reason_1 == null) {
                    return {
                        id: obj.id || obj.code,
                        text: obj.text || obj.description,
                        selected: true
                    }
                } else {
                    if(obj.code == item.return_reason_1) {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description,
                            selected: true
                        }
                    } else {
                        return {
                            id: obj.id || obj.code,
                            text: obj.text || obj.description
                        }
                    }
                }
            });

            cheque_control.find('#rr1-input').empty().select2({
                data: rr1ChequeControlOptions,
                dropdownAutoWidth : 'true'
            });
        }

        function prevItem() {
            var previousItemIndex = currentItemIndex - 1;

            if(previousItemIndex >= 0){
                currentItemIndex = previousItemIndex;
                displayItem();
            }
        }

        function nextItem() {
            var nextItemIndex = currentItemIndex + 1;

            if(nextItemIndex <= (settings.items.length - 1)) {
                currentItemIndex = nextItemIndex;
                displayItem();
            }
        }

        function scroll() {
            var item = settings.items[currentItemIndex];
            var position = cheque_control.find('#cheque-control-documents-box table [data-document-id=' + item.id + ']').position().top;
            cheque_control.find('#cheque-control-documents-box .cardbox-body').animate({
                scrollTop: position
            }, 0);
        }

        function displayItem() {
            setCurrent();
            setItem();
            scroll();
        }

        function displayImage() {
            var item = settings.items[currentItemIndex];
            if(item.image !== null) {
                image_manipulation_main.attr('data-image-id', item.id).attr('data-orientation', 'front');
                if(Images[item.id].front_filename !== null) {
                    image_manipulation_main.attr('src', Images[item.id].front_filename).attr('data-front-image', Images[item.id].front_filename);
                }
                if(Images[item.id].back_filename !== null) {
                    image_manipulation_main.attr('data-back-image', Images[item.id].back_filename);
                }
            }
        }

        function setCurrent() {
            var item = settings.items[currentItemIndex];
            var current_row = cheque_control.find('#cheque-control-documents-box table [data-document-id=' + item.id + ']');
            cheque_control.find('span.current-document').html(currentItemIndex + 1);
            cheque_control.find('#cheque-control-documents-box table tr.current').removeClass('current');
            current_row.addClass('current');
        }

        function setDocumentRow(index) {
            var item = settings.items[index];
            var row = cheque_control.find('#cheque-control-documents-box table [data-document-id=' + item.id + ']');
            if(item.return_reason_1 !== null) {
                row.find('.rr1').html(item.return_reason_1);
                row.find('.checked').html('<i class="fa color-green fa-check-square"></i>');
            } else {
                row.find('.rr1').empty();
                row.find('.checked').empty();
            }
        }
        
        function setProcessed() {
            var processed = settings.items.filter(function (item) {
                return item.return_reason_1 !== null;
            }).length;

            $('.processed').html(processed);
            var percentage = (processed * 100) / settings.items.length;
            if(percentage > 70) {
                $('.progress-bar').css('width', percentage + '%').prev().addClass('color-white');
            } else {
                $('.progress-bar').css('width', percentage + '%');
            }
        }

        function saveItem() {
            var index = currentItemIndex;
            var item = settings.items[currentItemIndex];
            var rr1 = cheque_control.find('#rr1-input').val();
            if(currentItemIndex === (settings.items.length - 1)) {
                $(document).off('keydown');
                $('#confirm').off('click').prop('disabled', true);
            } else {
                $(document).off('keydown');
                $('#confirm').off('click').prop('disabled', true);
                nextItem();
            }
            $.ajax({
                url: Parallax.Route.get('remote-deposit.cheque-control.archives.save', {item_id: item.id}),
                type: 'PUT',
                data: {rr1: rr1},
                dataType: 'json'
            }).done(function (data) {
                enableKeyDown();
                enableConfirmClick();
                if(data.return === true) {
                    item.return_reason_1 = rr1;
                    setProcessed();
                    setDocumentRow(index);
                    toastr.success(data.message,'');
                } else {
                    toastr.error(data.message, '');
                }
            }).fail(function () {
                toastr.options = {"showDuration": 0};
                toastr.error('Unexpected AJAX call error on saveItem().','');
            })
        }

        return this;
    };

}( jQuery ));