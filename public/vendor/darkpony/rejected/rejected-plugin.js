(function( $ ) {

    $.fn.rejected = function(options) {
        var modal = this;
        var alertForEndOfDocument = true;
        var currentItemIndex = -1;
        var image_manipulation_main = modal.find('.image-manipulation-main');
        var settings = $.extend({
            items: {},
            priceFormat: {
                prefix: "€ ",
                centsSeparator: '.',
                thousandsSeparator: ',',
                limit: 11,
                centsLimit: 2
            }

        }, options );

        settings.items.forEach(function (item) {
            Images[item.id] = item.image;
        });
        var focusableInputsErrors = [], nextErrorToFocus = 0;
        var focusableInputs = modal.find('.user-input'), nextInputToFocus = 0;
        var id = $('div.documents-list').find('tr.active').data('item-id');

        //currentItemIndex = (settings.items.findIndex(item => item.id === id) === -1) ? 0 : settings.items.findIndex(item => item.id === id);
        currentItemIndex = 0;
        $(settings.items).each(function(index, value){
          if(value.id === id){
            currentItemIndex = index;
            return false;
          }
        });

        modal.off();

        modal.on('show.bs.modal', function () {
            displayImage();
            enableKeyDown();
        });

        modal.on('shown.bs.modal', function () {
            modal.draggable({
                handle: ".modal-content"
            });
            displayItem();
        });

        modal.on('hidden.bs.modal', function () {
            $(document).off('keydown');
            $(document).off('keyup');
        });

        modal.on('focus', '.user-input', function () {
            nextInputToFocus = parseInt($(this).attr('tabindex'));
        });

        modal.find('.first-item').on("click", function() {
            nextInputToFocus = 0;
            firstItem();
            displayImage();
        });

        modal.find('.last-item').on("click", function() {
            nextInputToFocus = 0;
            lastItem();
            displayImage();
        });

        modal.find('.prev-item').on("click", function() {
            nextInputToFocus = 0;
            prevItem();
            displayImage();
        });

        modal.find('.next-item').on("click", function() {
            nextInputToFocus = 0;
            nextItem();
            displayImage();
        });

        /**
         * Set an item to the modal.
         */
        function setItem() {
            var item = settings.items[currentItemIndex];
            modal.find('#batch_no').html(item.batch_no);
            modal.find('#doc_no input').val(item.item_no);
            modal.find('#trans_code').val(item.trans_code);
            modal.find('#cheque_no').val(item.cheque_no);
            modal.find('#bank_code').val(item.bank_code);
            modal.find('#cheque_account_no').val(item.cheque_account_no);
            modal.find('#check_digit_1').val(item.check_digit_1);
            modal.find('#amount').val(item.amount).priceFormat(
                $.extend({}, settings.priceFormat, {'clearOnEmpty': 'true'})
            );
            if(item.check_digit_2 !== null && item.check_digit_2 !== '') {
                modal.find('#check_digit_2').val(item.check_digit_2);
            } else {
                modal.find('#check_digit_2').val('');
            }
        }

        /**
         * Enable keydown event.
         */
        function enableKeyDown() {
            $(document).on('keydown', function (e) {
                searchForErrors();
                if (modal.hasClass('show')) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        if (modal.find('#doc_no input').is(":focus")) {
                            nextInputToFocus = 0;
                            changeDocNo();
                        } else {
                            if (focusableInputsErrors.length === 0) {
                                nextInputToFocus = 0;
                                focusInputByIndex();
                            } else {
                                focusInputByError();
                            }
                        }
                    }
                    if (keyCode === 9) {
                        e.preventDefault();
                        if (focusableInputsErrors.length === 0) {
                            nextInputToFocus = nextInputToFocus + 1;
                            if (nextInputToFocus > (focusableInputs.length - 1)) {
                                nextInputToFocus = 0;
                            }
                            focusInputByIndex();
                        } else {
                            focusInputByError();
                        }
                    }
                    if (keyCode === 38) {
                        e.preventDefault();
                        nextInputToFocus = 0;
                        nextItem();
                    }
                    if (keyCode === 40) {
                        e.preventDefault();
                        nextInputToFocus = 0;
                        prevItem();
                    }
                }
            }).on('keyup', function (e) {
                if (modal.hasClass('show')) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        if (focusableInputsErrors.length === 0) {
                            e.preventDefault();
                            nextItem(true);
                            displayImage();
                        }
                    }
                    if (keyCode === 38) {
                        e.preventDefault();
                        displayImage();
                    }
                    if (keyCode === 40) {
                        e.preventDefault();
                        displayImage();
                    }
                }
            })
        }

        function changeDocNo() {
            var doc_no_input = modal.find('#doc_no input');
            var doc_no = parseInt(doc_no_input.val());
            if(!isNaN(doc_no) && doc_no > 0) {
                if(doc_no <= settings.items.length) {
                    currentItemIndex = parseInt(doc_no_input.val()) - 1;
                    displayItem();
                    displayImage();
                } else {
                    toastr.error('The maximum number of documents is ' + settings.items.length + '.');
                }
            }
        }

        /**
         * Set Progress Bar
         */
        function setProgressBar() {
            var current = currentItemIndex + 1;
            var percentage = (100 * (currentItemIndex + 1)) / settings.items.length;
            modal.find('.current').html(current);
            modal.find('.total').html(settings.items.length);
            modal.find('.progress-bar').width(percentage + '%');
            if(percentage > 70) {
                modal.find('.progress-bar-w-centered-numbers span:first').addClass('color-white');
            } else {
                modal.find('.progress-bar-w-centered-numbers span:first').removeClass('color-white');
            }
        }

        function focusInputByError() {
            if(nextErrorToFocus > (focusableInputsErrors.length-1)){
                nextErrorToFocus = 0;
            }
            var elem = focusableInputsErrors[nextErrorToFocus].element;
            var char = focusableInputsErrors[nextErrorToFocus].character;
            $(elem).focus().selectRange(char);
            nextErrorToFocus = nextErrorToFocus + 1;
        }

        /**
         * Focus by index.
         */
        function focusInputByIndex() {
            $(focusableInputs[nextInputToFocus]).focus().select();
        }

        /**
         * Go to the first item.
         */
        function firstItem() {
            currentItemIndex = 0;
            displayItem();
        }

        /**
         * Got to the last item.
         */
        function lastItem() {
            currentItemIndex = settings.items.length - 1;
            displayItem();
        }

        /**
         * Got to previous item.
         */
        function prevItem() {
            var previousItemIndex = currentItemIndex - 1;
            if(previousItemIndex >= 0){
                currentItemIndex = previousItemIndex;
                displayItem();
            } else {
                previousItemIndex = settings.items.length - 1;
                currentItemIndex = previousItemIndex;
                displayItem();
            }
        }

        /**
         * Go to next item.
         *
         * @param save
         * @returns {boolean}
         */
        function nextItem(save) {
            save = save || false;
            if(save === true) {
                if (currentItemIndex <= settings.items.length - 1) {
                    if(!saveItem(currentItemIndex)) {
                        return false;
                    }
                }
            }

            if (currentItemIndex === settings.items.length - 1) {
                if(alertForEndOfDocument){
                    swal({
                        title: 'End of document',
                        timer: 1500,
                        type: 'info'
                    });
                    alertForEndOfDocument = false;
                }else{
                    alertForEndOfDocument = true;
                    var nextItemIndex = 0;
                    if(nextItemIndex <= settings.items.length - 1) {
                        currentItemIndex = nextItemIndex;
                        displayItem();
                    }
                }
            } else {
                var nextItemIndex = currentItemIndex + 1;
                if(nextItemIndex <= settings.items.length - 1) {
                    currentItemIndex = nextItemIndex;
                    displayItem();
                }
            }
        }

        function searchForErrors() {
            focusableInputsErrors = [];
            $.each(focusableInputs, function (key, elem) {
                var hasError = false, str = $(elem).val(), i = 0;
                for(i=0; i < str.length; i++) {
                    var error = {};
                    error["element"] = elem;
                    error["character"] = i;
                    if (str[i] === "?" || str[i] === ":" || str[i] === ";") {
                        focusableInputsErrors.push(error);
                        hasError = true;
                    }
                }
                if(hasError) {
                    $(elem).addClass('color-red');
                } else {
                    $(elem).removeClass('color-red');
                }
            });
            nextErrorToFocus = 0;
        }

        function displayItem() {
            setItem();
            searchForErrors();
            setProgressBar();
            focusInputByIndex();
        }

        function displayImage() {
            var item = settings.items[currentItemIndex];
            if(item.image !== null) {
                image_manipulation_main.attr('data-image-id', Images[item.id].id).attr('data-orientation', 'front');
                if(Images[item.id].front_filename !== null) {
                    image_manipulation_main.attr('src', Images[item.id].front_filename).attr('data-front-image', Images[item.id].front_filename);
                }
                if(Images[item.id].back_filename !== null) {
                    image_manipulation_main.attr('data-back-image', Images[item.id].back_filename);
                }
            }
        }

        function updateBatchData(batch_data) {
            var div = $('#reconciliation-batch-documents-general-info-box');
            div.find('#batch-data-batch-documents').html(batch_data.batch_documents);
            div.find('#batch-data-valid-documents').html(batch_data.valid_documents);
            div.find('#batch-data-rejected-documents').html(batch_data.rejected_documents);
            div.find('#batch-data-deleted-documents').html(batch_data.deleted_documents);
            div.find('#batch-data-documents-difference').html(batch_data.documents_difference);
            div.find('#batch-data-batch-total-amount').html(batch_data.batch_total_amount);
            div.find('#batch-data-cheques-amount').html(batch_data.cheques_amount);
            div.find('#batch-data-amount-difference').html(batch_data.amount_difference);
            if(batch_data.documents_difference == 0) {
                div.find('#batch-data-documents-difference').removeClass('bg-red-a-0-5');
            } else {
                div.find('#batch-data-documents-difference').addClass('bg-red-a-0-5');
            }
            if(batch_data.amount_difference_without_formatting == 0) {
                div.find('#batch-data-amount-difference').removeClass('bg-red-a-0-5');
            } else {
                div.find('#batch-data-amount-difference').addClass('bg-red-a-0-5');
            }
        }

        function updateTableCell(item) {
            var row = $('div.documents-list').find('tr[data-item-id="' + item.id + '"]');
            row.find("td[data-name='trans_code']").html(item.trans_code);
            row.find("td[data-name='cheque_no']").html(item.cheque_no);
            if(item.rejected == 1) {
                row.find('td.reject').removeClass('color-red').addClass('color-green').html(Parallax.Lang.get('reconciliation.rc'));
            }
            row.find("td[data-name='bank_code']").html(item.bank_code);
            row.find("td[data-name='cheque_account_no']").html(item.cheque_account_no);
            row.find("td[data-name='check_digit_1']").html(item.check_digit_1);
            row.find("td[data-name='amount']").html(item.amount).priceFormat(settings.priceFormat);
            row.find("td[data-name='check_digit_2']").html(item.check_digit_2);
        }

        /**
         * Save item.
         *
         * @param index
         * @returns {boolean}
         */
        function saveItem(index) {
            var item = settings.items[index];
            var data = {
                trans_code: modal.find('#trans_code').val(),
                cheque_no: modal.find('#cheque_no').val(),
                bank_code: modal.find('#bank_code').val(),
                cheque_account_no: modal.find('#cheque_account_no').val(),
                check_digit_1: modal.find('#check_digit_1').val(),
                amount: modal.find('#amount').unmask().trim(),
                check_digit_2: modal.find('#check_digit_2').val()
            };

            item.trans_code = modal.find('#trans_code').val();
            item.cheque_no = modal.find('#cheque_no').val();
            item.bank_code = modal.find('#bank_code').val();
            item.cheque_account_no = modal.find('#cheque_account_no').val();
            item.check_digit_1 = modal.find('#check_digit_1').val();
            item.amount = modal.find('#amount').unmask().trim();
            item.check_digit_2 = modal.find('#check_digit_2').val();

            var validation = new ChequeValidation(Options, Messages);

            if(!validation.hasValidTransCode(data)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.invalid_trans_code'));
                return false;
            }

            if(!validation.hasValidBankCode(data)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.invalid_bank_code'));
                return false;
            }

            //Validate the whole Check Digit 1 Code Line.
            if(!validation.hasValidCheckDigit1CodeLine(data)) {
                return false;
            }

            //If the check digit 2 has a number and or amount is bigger than zero then validate the check digit 2.
            if(validation.isValidAmount(data.amount)) {
                if(validation.isValidCheckDigit2(data.check_digit_2)) {
                    if(!validation.isValidCheckDigit2(data.check_digit_2)) {
                        toastr.error(Parallax.Lang.get('reconciliation.validation.empty_check_digit_2'));
                        return false;
                    }
                    if (!validation.hasValidCheckDigit2CodeLine(data)) {
                        return false;
                    }
                }
            } else {
                if(validation.isValidCheckDigit2(data.check_digit_2)) {
                    toastr.error(Parallax.Lang.get('reconciliation.validation.empty_amount'));
                    return false;
                }
            }

            $.ajax({
                url: Parallax.Route.get('scan.item.update.reject', {item_id: item.id}),
                type: 'PUT',
                data: data,
                dataType: 'json'
            }).done(function (response) {
                if(response.return === true) {
                    item.trans_code = data.trans_code;
                    item.cheque_no = data.cheque_no;
                    item.bank_code = data.bank_code;
                    item.cheque_account_no = data.cheque_account_no;
                    item.check_digit_1 = data.check_digit_1;
                    item.amount = data.amount;
                    item.check_digit_2 = data.check_digit_2 = response.item.check_digit_2;
                    updateTableCell(item);
                    updateBatchData(response.batch);
                    toastr.success(response.message,'');
                } else {
                    toastr.error(response.message, '');
                }
            }).fail(function () {
                toastr.options = {"showDuration": 0};
                toastr.error('Unexpected AJAX call error on saveItem().','');
            });
            return true;
        }

        return this;
    };

    return this;

}( jQuery ));
