(function( $ ) {

    $.fn.dataEntry = function(options) {
        var modal = this;
        var alertForEndOfDocument = true;
        var image_manipulation_main = modal.find('.image-manipulation-main');
        var settings = $.extend({
            items: {},
            batch: {total_amount: 0},
            priceFormat: {
                prefix: "€ ",
                centsSeparator: '.',
                thousandsSeparator: ',',
                limit: 11,
                centsLimit: 2
            }

        }, options );

        settings.items.forEach(function (item) {
            Images[item.id] = item.image;
        });
        var keys_enabled = true, currentImageType = 'front', focusableInputs = modal.find('.user-input'), nextInputToFocus = 0;
        var id = $('div.documents-list').find('tr.active').data('item-id');

        //var currentItemIndex = (settings.items.findIndex(item => item.id === id) === -1) ? 0 : settings.items.findIndex(item => item.id === id);
        var currentItemIndex = 0;
        $(settings.items).each(function(index, value){
          if(value.id === id){
            currentItemIndex = index;
            return false;
          }
        });

        modal.off();

        modal.on('show.bs.modal', function () {
            displayImage();
            setBatchAmounts();
            enableKeydown();
        });

        modal.on('shown.bs.modal', function () {
            displayItem();
        });

        modal.on('hidden.bs.modal', function () {
            $(document).off('keydown');
            $(document).off('keyup');
        });

        modal.on('focus', '.user-input', function () {
            nextInputToFocus = parseInt($(this).attr('tabindex'));
        });

        modal.find('.first-item').on("click", function() {
            firstItem();
            displayImage();
        });

        modal.find('.last-item').on("click", function() {
            lastItem();
            displayImage();
        });

        modal.find('.prev-item').on("click", function() {
            prevItem();
            displayImage();
        });

        modal.find('.next-item').on("click", function() {
            nextItem();
            displayImage();
        });

        /**
         * Set an item to the modal.
         *
         */
        function setItem() {
            var item = settings.items[currentItemIndex];
            modal.find('#batch_no').html(item.batch_no);
            modal.find('#doc_no input').val(item.item_no);
            modal.find('#amount').val(item.amount).priceFormat(
                $.extend({}, settings.priceFormat, {'clearOnEmpty': 'true'})
            );
        }

        function calculateBatchAmounts() {
            var cheques_amount = 0;
            $.each(settings.items, function (index, item) {
                cheques_amount += parseInt(Number(item.amount));
            });

            modal.find('#cheques_amount').val(cheques_amount).priceFormat(settings.priceFormat);
            modal.find('#amount_diff').val(parseInt(settings.batch.total_amount) - cheques_amount).priceFormat(
                $.extend({}, settings.priceFormat, {'allowNegative': 'true'})
            );

            if((parseInt(settings.batch.total_amount) - cheques_amount) !== 0) {
                modal.find('#total_amount, #cheques_amount, #amount_diff').removeClass('color-green').addClass('color-red');
            } else {
                modal.find('#total_amount, #cheques_amount, #amount_diff').removeClass('color-red').addClass('color-green');
            }
        }

        function setBatchAmounts() {
            modal.find('#total_amount').val(settings.batch.total_amount).priceFormat(settings.priceFormat);
            calculateBatchAmounts();
        }

        /**
         * Enable keydown event.
         */
        function enableKeydown() {
            keys_enabled = true;
            $(document).on('keydown', function(e) {
                if(modal.hasClass('show')) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        if(modal.find('#doc_no input').is(":focus")) {
                            nextInputToFocus = 0;
                            changeDocNo();
                        } else {
                            nextInputToFocus = nextInputToFocus + 1;
                            focusInputByIndex();
                        }
                    }
                    if (keyCode === 9) {
                        e.preventDefault();
                        nextInputToFocus = nextInputToFocus + 1;
                        if(nextInputToFocus > (focusableInputs.length-1)){
                            nextInputToFocus = 0;
                        }
                    }
                    if (keyCode === 38) {
                        e.preventDefault();
                        nextInputToFocus = 0;
                        nextItem();
                    }
                    if (keyCode === 40) {
                        e.preventDefault();
                        nextInputToFocus = 0;
                        prevItem();
                    }
                }
            }).on('keyup', function(e) {
                if(modal.hasClass('show')) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        if(nextInputToFocus > (focusableInputs.length-1)){
                            nextInputToFocus = 0;
                            nextItem(true);
                            displayImage();
                        }
                    }
                    if (keyCode === 38) {
                        e.preventDefault();
                        displayImage();
                    }
                    if (keyCode === 40) {
                        e.preventDefault();
                        displayImage();
                    }
                }
            })
        }

        function changeDocNo() {
            var doc_no_input = modal.find('#doc_no input');
            var doc_no = parseInt(doc_no_input.val());
            if(!isNaN(doc_no) && doc_no > 0) {
                if(doc_no <= settings.items.length) {
                    currentItemIndex = parseInt(doc_no_input.val()) - 1;
                    displayItem();
                    displayImage();
                } else {
                    toastr.error('The maximum number of documents is ' + settings.items.length + '.');
                }
            }
        }

        /**
         * Set Progress Bar
         */
        function setProgressBar() {
            var current = currentItemIndex + 1;
            var percentage = (100 * (currentItemIndex + 1)) / settings.items.length;
            modal.find('.current').html(current);
            modal.find('.total').html(settings.items.length);
            modal.find('.progress-bar').width(percentage + '%');
            if(percentage > 70) {
                modal.find('.progress-bar-w-centered-numbers span:first').addClass('color-white');
            } else {
                modal.find('.progress-bar-w-centered-numbers span:first').removeClass('color-white');
            }
        }

        /**
         * Focus by index.
         */
        function focusInputByIndex() {
            $(focusableInputs[nextInputToFocus]).focus().select();
        }

        /**
         * Go to the first item.
         */
        function firstItem() {
            currentItemIndex = 0;
            displayItem();
        }

        /**
         * Got to the last item.
         */
        function lastItem() {
            currentItemIndex = settings.items.length - 1;
            displayItem();
        }

        /**
         * Got to previous item.
         */
        function prevItem() {
            var previousItemIndex = currentItemIndex - 1;
            if(previousItemIndex >= 0){
                currentItemIndex = previousItemIndex;
                displayItem();
            } else {
                previousItemIndex = settings.items.length - 1;
                currentItemIndex = previousItemIndex;
                displayItem();
            }
        }

        /**
         * Go to next item.
         *
         * @param save
         * @returns {boolean}
         */
        function nextItem(save) {
            save = save || false;
            if(save === true) {
                if (currentItemIndex <= settings.items.length - 1) {
                    if(!saveItem(currentItemIndex)) {
                        return false;
                    }
                }
            }

            if (currentItemIndex === settings.items.length - 1) {
                if(alertForEndOfDocument){
                    swal({
                        title: 'End of document',
                        timer: 1500,
                        type: 'info'
                    });
                    alertForEndOfDocument = false;
                }else{
                    alertForEndOfDocument = true;
                    var nextItemIndex = 0;
                    if(nextItemIndex <= settings.items.length - 1) {
                        currentItemIndex = nextItemIndex;
                        displayItem();
                    }
                }
            } else {
                var nextItemIndex = currentItemIndex + 1;
                if(nextItemIndex <= settings.items.length - 1) {
                    currentItemIndex = nextItemIndex;
                    displayItem();
                }
            }
        }

        function displayItem() {
            setItem();
            setProgressBar();
            focusInputByIndex();
        }

        function displayImage() {
            var item = settings.items[currentItemIndex];
            if(item.image !== null) {
                image_manipulation_main.attr('data-image-id', Images[item.id].id).attr('data-orientation', 'front');
                if(Images[item.id].front_filename !== null) {
                    image_manipulation_main.attr('src', Images[item.id].front_filename).attr('data-front-image', Images[item.id].front_filename);
                }
                if(Images[item.id].back_filename !== null) {
                    image_manipulation_main.attr('data-back-image', Images[item.id].back_filename);
                }
            }
        }

        function updateBatchData(batch_data) {
            var div = $('#reconciliation-batch-documents-general-info-box');
            div.find('#batch-data-batch-documents').html(batch_data.batch_documents);
            div.find('#batch-data-valid-documents').html(batch_data.valid_documents);
            div.find('#batch-data-rejected-documents').html(batch_data.rejected_documents);
            div.find('#batch-data-deleted-documents').html(batch_data.deleted_documents);
            div.find('#batch-data-documents-difference').html(batch_data.documents_difference);
            div.find('#batch-data-batch-total-amount').html(batch_data.batch_total_amount);
            div.find('#batch-data-cheques-amount').html(batch_data.cheques_amount);
            div.find('#batch-data-amount-difference').html(batch_data.amount_difference);
            if(batch_data.documents_difference == 0) {
                div.find('#batch-data-documents-difference').removeClass('bg-red-a-0-5');
            } else {
                div.find('#batch-data-documents-difference').addClass('bg-red-a-0-5');
            }
            if(batch_data.amount_difference_without_formatting == 0) {
                div.find('#batch-data-amount-difference').removeClass('bg-red-a-0-5');
            } else {
                div.find('#batch-data-amount-difference').addClass('bg-red-a-0-5');
            }
        }

        function updateTableCell(item) {
            var row = $('div.documents-list').find('tr[data-item-id="' + item.id + '"]');
            row.find("td[data-name='amount']").html(item.amount).priceFormat(settings.priceFormat);
            row.find("td[data-name='check_digit_2']").html(item.check_digit_2);
        }

        /**
         * Save item.
         *
         * @param index
         * @returns {boolean}
         */
        function saveItem(index) {
            var item = settings.items[index];
            var data = {
                amount: modal.find('#amount').unmask().trim(),
            };

            item.amount = modal.find('#amount').unmask().trim();

            var validation = new ChequeValidation(Options, Messages);

            if(!validation.isValidAmount(data.amount)) {
                toastr.error(Parallax.Lang.get('reconciliation.validation.empty_amount'));
                return false;
            }

            $.ajax({
                url: Parallax.Route.get('scan.item.update.data.entry', {item_id: item.id}),
                type: 'PUT',
                data: data,
                dataType: 'json'
            }).done(function (response) {
                if(response.return === true) {
                    item.amount = data.amount;
                    item.check_digit_2 = response.item.check_digit_2;
                    calculateBatchAmounts();
                    updateTableCell(item);
                    updateBatchData(response.batch);
                    toastr.success(response.message,'');
                } else {
                    toastr.error(response.message, '');
                }
            }).fail(function () {
                toastr.options = {"showDuration": 0};
                toastr.error('Unexpected AJAX call error on saveItem().','');
            });
            return true;
        }

        return this;
    };

    return this;

}( jQuery ));
