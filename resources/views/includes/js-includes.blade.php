<script src="{{ URL::to('vendor/jquery/dist/jquery.js') }}"></script>
<script src="{{ URL::to('vendor/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ URL::to('vendor/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ URL::to('vendor/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ URL::to(mix('js/commons.js')) }}"></script>
<script src="{{ URL::to(mix('js/parallax.js')) }}"></script>
<!-- Flot charts-->
<script src="{{ URL::to('vendor/flot/jquery.flot.js') }}"></script>
<script src="{{ URL::to('vendor/flot/jquery.flot.categories.js') }}"></script>
<script src="{{ URL::to('vendor/jquery.flot.spline/jquery.flot.spline.js') }}"></script>
<script src="{{ URL::to('vendor/jquery.flot.tooltip/js/jquery.flot.tooltip.js') }}"></script>
<script src="{{ URL::to('vendor/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ URL::to('vendor/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ URL::to('vendor/flot/jquery.flot.time.js') }}"></script>
<!-- Sparkline-->
<script src="{{ URL::to('vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
<!-- jQuery Knob charts-->
<script src="{{ URL::to('vendor/jquery-knob/js/jquery.knob.js') }}"></script>
<!-- Material Colors-->
<script src="{{ URL::to('vendor/material-colors/dist/colors.js') }}"></script>
<script src="{{ URL::to('vendor/darkpony/query-builder/js/query-builder.standalone.js') }}"></script>
<script src="{{ URL::to('vendor/sidebysideimproved/jquery.flot.orderBars.js') }}"></script>
<script src="{{ URL::to(mix('js/app.js')) }}"></script>
<script src="{{ URL::to('vendor/toastr/toastr.min.js') }}"></script>
<script src="{{ URL::to('js/promise.min.js') }}"></script>
<script src="{{ URL::to('vendor/sweetalert2/v7/sweetalert2.all.min.js') }}"></script>
<script src="{{ URL::to('vendor/select2/dist/js/select2.js') }}"></script>
<script src="{{ URL::to('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::to('vendor/inputmask/js/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ URL::to('vendor/nouislider/distribute/nouislider.js') }}"></script>
<script src="{{ URL::to('vendor/sortable-tables/sortable.min.js') }}"></script>
<script src="{{ URL::to('vendor/moment/min/moment-with-locales.min.js') }}"></script>
<script src="{{ URL::to('js/reconciliation/modal.js') }}"></script>
<script src="{{ URL::to('js/reports/modal.js') }}"></script>
<script src="{{ URL::to('js/archives/modal.js') }}"></script>
<script src="{{ URL::to('js/queries/default.js') }}"></script>
<script src="{{ URL::to(mix('js/core.js')) }}"></script>
<script type="text/javascript">

</script>