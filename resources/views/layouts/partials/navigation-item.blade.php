<?php $fa = (isset($fa) ? $fa : "fa-barcode") ; ?>
<?php $url = (isset($url) ? $url : "#") ; ?>
<li fa="{{ $fa }}" class="menu-li <?php if (\Str::contains(\Request::getPathInfo(), $url)) { echo 'active'; } ?>">
    <a class="menu-a " href="/help/{{ $url }}">
        <span class="float-right nav-label"></span>
        <span class="nav-icon"><i class="fa {{ $fa }}" aria-hidden="true"></i></span>
        <span class="menu-title">{{ $title }}</span>
    </a>
    <span class="small-menu-tooltip-no-children">
        <a href="/help/{{ $url }}">{{ $title }}</a>
    </span>
</li>
