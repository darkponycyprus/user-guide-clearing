<!-- sidebar-->
<aside class="sidebar-container overflow-hidden">
    <div class="brand-header">
        <div class="float-left pt-4 text-muted sidebar-close"><em class="ion-arrow-left-c icon-lg"></em></div>
        <a class="brand-header-logo" href="#">
            <span class="brand-header-logo-text">Help</span>
        </a>
    </div>
    <div class="sidebar-content">
        <nav class="sidebar-nav">
            <ul>
                <li>
                    <div class="sidebar-nav-heading">MENU</div>
                </li>
                @include('layouts.partials.navigation-item', ['title' => 'Archive', 'url' => 'archive'])
                @include('layouts.partials.navigation-item', ['title' => 'Audit trail', 'url' => 'audits'])
                @include('layouts.partials.navigation-item', ['title' => 'Branches', 'url' => 'branches'])
                @include('layouts.partials.navigation-item', ['title' => 'Create Files', 'url' => 'create-files', 'fa' => 'fa-plus-square-o'])
                @include('layouts.partials.navigation-item', ['title' => 'Foreign Cheques', 'url' => 'foreignCheques'])
                @include('layouts.partials.navigation-item', ['title' => 'Holidays', 'url' => 'holidays'])
                @include('layouts.partials.navigation-item', ['title' => 'Jobs', 'url' => 'jobs'])
                @include('layouts.partials.navigation-item', ['title' => 'Load Files', 'url' => 'load-files'])
                @include('layouts.partials.navigation-item', ['title' => 'Options', 'url' => 'options'])
                @include('layouts.partials.navigation-item', ['title' => 'Reconciliation', 'url' => 'reconciliation', 'fa' => 'fa-balance-scale'])
                @include('layouts.partials.navigation-item', ['title' => 'Returns', 'url' => 'returns'])
                @include('layouts.partials.navigation-item', ['title' => 'Roles', 'url' => 'roles'])
                @include('layouts.partials.navigation-item', ['title' => 'Scanners', 'url' => 'scanners'])
                @include('layouts.partials.navigation-item', ['title' => 'Sessions', 'url' => 'session'])
                @include('layouts.partials.navigation-item', ['title' => 'Web scan', 'url' => 'web-scan'])
                @include('layouts.partials.navigation-item', ['title' => 'Users', 'url' => 'users'])
            </ul>
        </nav>
    </div>
</aside>
