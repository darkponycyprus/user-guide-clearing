<!DOCTYPE html>
<html lang="en" class="h-100 overflow-hidden">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link rel="stylesheet" href="{{ URL::to('css/bootstrap.min.css') }}">
        <style type="text/css">
            html {
                font-family: serif;
                font-size: 13px;
            }
            div.page
            {
                page-break-inside: avoid;
            }
            img {
                max-width: 100%;
                position: relative;
                z-index: 100;
            }
        </style>
    </head>
    <body class="theme-2 h-100">
        <div class="layout-container">
            @if (isset($custom_export))
                {!! $custom_export !!}
            @else
                @yield('content')
            @endif
        </div>
    </body>
</html>
