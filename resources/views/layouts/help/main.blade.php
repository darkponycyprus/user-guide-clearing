<!DOCTYPE html>
<html lang="en" class="h-100 overflow-hidden">
<head>
    <base href="{{URL::to('/')}}" target="_self">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Clearing</title>

    <!-- CSS -->
    @include('includes.css-includes')
    <link rel="stylesheet" type="text/css" href="/css/overrides.css">

    <style>
        .section-title {
            background: #eee;
            padding: 7px;
            font-weight: bolder;
            border-radius: 5px;
            border: 1px solid #dedede;
        }
    </style>
    <!-- CSS -->

    <!-- SCRIPTS -->
    @include('includes.js-includes')
    <!-- SCRIPTS -->
</head>
<body class="theme-2 h-100">
<div class="layout-container">
    @include('layouts.partials.navigation-help')
    <main class="main-container" style="overflow-x: hidden;">
        <section class="section-container bg-white">
            <div class="container-fluid">
                @yield('content')
            </div>
        </section>
    </main>
</div>
</body>
</html>
