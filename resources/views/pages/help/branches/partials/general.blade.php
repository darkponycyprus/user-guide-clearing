<div class="help-item text-center">
    <p class="font-16 text-left">
        When you visit the branches page you are presented with the list of all registered branches. In the far right of each row there is a color indication showcasing whether the branch is active or not. Above the table there are some filters that you can apply to narrow down the branches list.
    </p>
    <img src="{{urlOrPath('img/help/branches/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        You can edit/delete a scanner if you click on the highlighted buttons.
    </p>
    <img src="{{urlOrPath('img/help/branches/22.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To create a new branch click on the Add new button
    </p>
    <img src="{{urlOrPath('img/help/branches/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In the following screen enter the required fields
    </p>
    <ul class="text-left">
        <li><b>Name: </b>Name of the branch</li>
        <li><b>Code: </b>The code of the branch</li>
        <li><b>Status: </b>The status of the branch</li>
        <li><b>CPU: </b>Whether the branch is CPU(Central Process Unit)</li>
        <li><b>Is Virtual: </b>Whether the branch is virtual or not</li>
    </ul>
    <img src="{{urlOrPath('img/help/branches/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Whether you are creating a new branch or editing an existing one, to save it, click on the Save changes button
    </p>
    <img src="{{urlOrPath('img/help/branches/4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
