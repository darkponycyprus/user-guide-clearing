<div class="help-item text-center">
    <p class="font-16 text-left">
        Select Send Files from the menu
    </p>
    <img src="{{urlOrPath('img/help/create-files/0.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">You will be presented with <b>Completed</b> and <b>Pending</b> branches. </p>
    <ul class="text-left">
        <li><b>Completed:</b> The branch closed its outward clearing session successfully</li>
        <li><b>Pending:</b> The branch did not close its outward clearing session</li>
    </ul>
    <p class="font-16 text-left">
        If there are no pending branches to close their seesion click on the Proceed button. If there are still some pending ones, you can still proceed in generating the ECCS files by clicking the button Ignore & Proceed.
    </p>
    <img src="{{urlOrPath('img/help/create-files/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Once the button is pressed, SmartClear imports outward returns and validates the outward clearing cheques.
        If there are no errors found you should see the following message in the bottom-right corner of your screen
    </p>
    <img src="{{urlOrPath('img/help/create-files/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">In the next screen you can see the outward cheques grouped by banks. This is just an information screen.
    </p>
    <img src="{{urlOrPath('img/help/create-files/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you are ready to proceed then click the <img src="{{urlOrPath('img/help/create-files/btn_createfiles.png')}}" alt="Create Files"/>
        button and you will be presented with a progress bar.
    </p>
    <img src="{{urlOrPath('img/help/create-files/5.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Once the task is completed you will be redirected to a new screen and you will be presented with the created files. In order to send them to ECCS simply click the <img src="{{urlOrPath('img/help/create-files/btn_sendfiles.png')}}" alt=""/> button at the top-right corner.
    </p>
    <img src="{{urlOrPath('img/help/create-files/6.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
         If you wish to rollback the process, you can do that by clicking on the <img src="{{urlOrPath('img/help/create-files/btn_rollback.png')}}" alt="Rollback"/> button. This button will be present only if there are files created for the current day.
    </p>
    <img src="{{urlOrPath('img/help/create-files/7.png')}}" alt=""/>
</div>
