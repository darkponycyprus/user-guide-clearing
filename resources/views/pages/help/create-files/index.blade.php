@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="text-center mb-3">Create Files user guide</h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation"><a class="nav-link active" href="#scan" aria-controls="scan" role="tab" data-toggle="tab">Create Files</a></li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="scan" role="tabpanel">
                @include('pages.help.create-files.partials.create-files')
            </div>
        </div>
    </div>
@endsection
