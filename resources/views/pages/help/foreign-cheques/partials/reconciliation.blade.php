<div class="help-item text-center">
    <p class="font-16 text-left">
        The basic principles of <a href="{{ route('help.reconciliation') }}">reconciliation </a> apply here as well,
        but with some minor differences.
    </p>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        The batch list contains all batches along with their currencies.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/reco-batch-uncompressed.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        During data entry you will also need to enter the reference number.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/reco-ref-number.jpg')}}" alt=""/>
</div>
