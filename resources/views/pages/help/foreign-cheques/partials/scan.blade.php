<div class="help-item text-center">
    <p class="font-16 text-left">
        The basic principles of <a href="{{ route('help.web-scan') }}">web scan </a> also apply here, but with some
        minor differences.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/foreign-options.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        When creating a batch, you first need to select its currency.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/create-batch.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        The cheques table has been slightly modified to hold the Micr column instead of the different codeline parts
        (trans. code, bank code, etc.).
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/scanned.jpg')}}" alt=""/>
</div>
