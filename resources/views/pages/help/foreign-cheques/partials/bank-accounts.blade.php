<div class="help-item text-center">
    <p class="text-left font-16">
        The bank accounts under the Foreign Cheques section are used in relevant reports and exports. Each currency should
        be assigned to a single bank account.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/bank-accounts-list.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="text-left font-16">
        To add a new bank account click on the <img height="30" src="{{urlOrPath('img/help/foreign-checks/add-new-btn.jpg')}}" alt=""/>
        button and all required inputs.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/new-bank-account.jpg')}}" alt=""/>
    <p class="text-left font-16">
        Once all necessary information have been defined click on the save button.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/new-bank-account-btns.jpg')}}" alt=""/>
</div>
