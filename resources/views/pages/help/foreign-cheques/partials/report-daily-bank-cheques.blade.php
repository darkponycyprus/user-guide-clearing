<div class="help-item text-center">
    <p class="text-left font-16">
        To export the "Daily Bank Cheques report", click on the menu and then select a date.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/report-daily-cheques.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
