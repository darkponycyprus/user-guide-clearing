<div class="help-item text-center">
    <p class="text-left font-18">
        The following screen lists all pending foreign cheques.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/report-remittance-letter.jpg')}}" alt=""/>
    <hr class="mt-4 mb-2"/>
    <p class="text-left font-18">
        Select the cheques you want to include your pdf export and click the
        <img src="{{urlOrPath('img/help/foreign-checks/export-letter-btn.jpg')}}" alt=""/> button.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/export-report-letter-selected.jpg')}}" alt=""/>
    <hr class="mt-4 mb-2"/>
    <p class="text-left font-18">
        For GBP - Pound Sterling, follow the same process but before downloading/saving the PDF you will need to enter
        Contact Name, Giro Serial No. and Sort Code.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/remittance-letter-case-pounds.jpg')}}" alt=""/>
    <hr class="mt-4 mb-2"/>
    <p class="text-left font-18">
        When a cheque has been exported, its status will change from <span class="color-red">Pending</span>
        to <span class="color-green">Exported</span>.
    </p>
    <img src="{{urlOrPath('img/help/foreign-checks/remittance-letter-exported.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
