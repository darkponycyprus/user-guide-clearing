<h1>Foreign Cheques</h1>
<h2>Scan</h2>
@include('pages.help.foreign-cheques.partials.scan')
<h2>Reconciliation</h2>
@include('pages.help.foreign-cheques.partials.reconciliation')
<h2>Bank accounts</h2>
@include('pages.help.foreign-cheques.partials.bank-accounts')
<h2>Report - Remittance letter</h2>
@include('pages.help.foreign-cheques.partials.report-remittance-letter')
<h2>Report - Daily Bank Cheques</h2>
@include('pages.help.foreign-cheques.partials.report-daily-bank-cheques')
