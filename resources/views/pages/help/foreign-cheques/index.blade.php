@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="mb-3 section-title">Foreign Cheques User Guide</h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation"><a class="nav-link active" href="#f-scan" aria-controls="f-scan" role="tab" data-toggle="tab">Scan</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="#f-reconciliation" aria-controls="f-reconciliation" role="tab" data-toggle="tab">Reconciliation</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="#f-bank-accounts" aria-controls="f-bank-accounts" role="tab" data-toggle="tab">Bank Accounts</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="#f-reports-letter" aria-controls="f-reports-letter" role="tab" data-toggle="tab">Reports - Remittance Letter</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="#f-reports-daily" aria-controls="f-reports-daily" role="tab" data-toggle="tab">Reports - Daily Bank Cheques</a></li>
        </ul>
        <div class="tab-content">
            {{-- tab scan start --}}
            <div class="tab-pane active" id="f-scan" role="tabpanel">
                @include('pages.help.foreign-cheques.partials.scan')
            </div>
            {{-- tab scan end --}}

            {{-- tab reconciliation start --}}
            <div class="tab-pane" id="f-reconciliation" role="tabpanel">
                @include('pages.help.foreign-cheques.partials.reconciliation')
            </div>
            {{-- tab reconciliation end --}}

            {{-- tab bank accounts start --}}
            <div class="tab-pane" id="f-bank-accounts" role="tabpanel">
                @include('pages.help.foreign-cheques.partials.bank-accounts')
            </div>
            {{-- tab bank accounts end --}}

            {{-- tab reports - Remittance Letter start --}}
            <div class="tab-pane" id="f-reports-letter" role="tabpanel">
                @include('pages.help.foreign-cheques.partials.report-remittance-letter')
            </div>
            {{-- tab reports - Remittance Letter end --}}

            {{-- tab reports - Daily Bank Cheques start --}}
            <div class="tab-pane" id="f-reports-daily" role="tabpanel">
                @include('pages.help.foreign-cheques.partials.report-daily-bank-cheques')
            </div>
            {{-- tab reports - Daily Bank Cheques end --}}
        </div>
    </div>
@endsection
