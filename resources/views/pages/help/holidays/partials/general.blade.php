<div class="help-item text-center">
    <p class="font-16 text-left">
       Holidays are crucial part of the system, so they need to be up to date for each year. When you visit the holidays screen you can see the list of all the registered ones.
    </p>
    <img src="{{urlOrPath('img/help/holidays/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To add a new one click on the Add new button
    </p>
    <img src="{{urlOrPath('img/help/holidays/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        and from the modal select the date and enter a read-friendly name.
    </p>
    <img src="{{urlOrPath('img/help/holidays/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
