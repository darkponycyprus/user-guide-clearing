<div class="help-item text-center">
    <p class="font-16 text-left">
        Selecting a single batch leads us to the Reconcile Batch screen. This screen contains general information about the
        batch and its documents. In the top left section we have the batch details. In the top right section we have the
        image preview area of the currently selected document. In the bottom section we have the documents table. Users can
        sort the table (by a descending or ascending order) by clicking on an applicable column(table headers with green color).
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-1.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>

<div class="help-item text-center">
    <p class="font-16 text-left">
        A batch is considered valid:
    </p>
    <ul class="text-left">
        <li>
            If all of its non deleted and non rejected cheques are equal with the total number of documents.
        </li>
        <li>
            When the batch amount is equal with the cheques total amount.
        </li>
    </ul>
    <p class="font-16 text-left">
        If there is a difference between valid cheques and the total number of batch documents, the field
        "Documents Diff." will be marked with a red color.
    </p>
    <p class="font-16 text-left">
        In case there is a difference between the batch amount and the total cheques amount, the field "Amount Diff."
        will be marked with a red color.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-1-1.jpg')}}" alt=""/>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-1-1b.jpg')}}" alt=""/>
    <p class="font-16 text-left mt-3">
        Clicking on <img src="{{urlOrPath('img/help/reconciliation/cogs.jpg')}}" alt=""/> button allows the users to edit
        the batch's information, as well as delete the batch.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-1-2.jpg')}}" alt=""/>
    <p class="font-16 text-left mt-3">
        To delete the batch (and all of its documents) click on the
        <img src="{{urlOrPath('img/help/reconciliation/delete-batch.jpg')}}" alt=""/> button.
        The following warning will appear. <span class="color-red">Important:</span> if you choose yes, the batch will
        <strong>NOT</strong> be deleted until you click
        <img src="{{urlOrPath('img/help/reconciliation/save-changes.jpg')}}" alt=""/>
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-1-3.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>

<div class="help-item text-center">
    <p class="font-16 text-left">
        Documents table in consisted of various columns:
    </p>
    <ul class="text-left">
        <li>
            <b>Del.</b> (short for Deleted) - This column allows users to delete a document by clicking on the corresponding
            checkbox. This action is reversible; users can restore deleted documents.
        </li>
        <li>
            <b>Rej.</b> (short for Rejected)
            <ul>
                <li>
                    <span class="color-red">RJ</span> -
                    If the system was not able to extract the codeline from the scanned image it will mark the cheque as
                    rejected.
                </li>
                <li>
                    <span class="color-green">RC</span> -
                    When a rejected image has been corrected the system will flag this cheque accordingly.
                </li>
            </ul>
        </li>
        <li><b>Item No.</b> (short for Item Number)</li>
        <li><b>Trans Code</b> (short for Transaction Code)</li>
        <li><b>Cheque No.</b> (short Cheque Number)</li>
        <li><b>Bank Code</b></li>
        <li><b>Cheque Account No.</b> (short Cheque Account Number)</li>
        <li><b>CD1</b> (short for Cheque digit 1)</li>
        <li><b>Amount</b></li>
        <li><b>CD2</b> (short for Cheque digit 2)</li>
    </ul>
    <p class="font-16 text-left">
        Clicking on any of the table's header columns (except "Del." and "Rej.") allows users to sort the table in any
        direction.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-2.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>

<div class="help-item text-center">
    <p class="font-16 text-left">
        Selecting a document from the table (either by clicking or by using the arrow keys), loads the corresponding
        image in the preview section. The cheque images can be further manipulated via the following buttons:
    </p>
    <table class="table">
        <thead>
        <tr>
            <th>Icon</th>
            <th class="text-left">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/rotate.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Rotate the image and update the cheque.
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/flip.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Flip the image - preview only.
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/swap.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Swap front and back image sides and update the cheque.
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/zoom.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Zoom in/out
            </td>
        </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-3.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>

<div class="help-item text-center">
    <p class="font-16 text-left">
        The following table summarizes the basic actions of Reconciliation. Users can either edit a single cheque
        or all of applicable documents under each action/group.
    </p>
    <table class="table">
        <thead>
        <tr>
            <th>Icon</th>
            <th class="text-left">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/codeline_generator.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Generate codelines for cheques that don't have any. The account numbers can be managed from the Bank Accounts section
                <div class="text-center">
                    <img src="{{urlOrPath('img/help/reconciliation/bank-accounts-menu.png')}}" alt=""/>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/edit2.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Edit all the cheques
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/correct_rejected.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Correct rejected cheques
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/data_entry.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Fill amounts (uses a service to call the Banking System and retrieve the cheque amount when possible)
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/back.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Back button - Return to the Batch list page
            </td>
        </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-4.jpg')}}" alt=""/>
    <p class="font-16 text-left mt-3">
        <b>Codeline Generator</b>
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-4-1.jpg')}}" alt=""/>
    <ol class="text-left">
        <li style="padding: 5px">
            <p class="font-16">
                Users can select the appropriate "Account No." from a dropdown list. Upon account selection, the
                "Trans Code" and "Bank" fields will be filled automatically.
            </p>
            <div class="text-center">
                <img src="{{urlOrPath('img/help/reconciliation/1-4-4-2.jpg')}}" alt=""/>
            </div>
        </li>
        <li style="padding: 5px">
            <p class="font-16">
                Fill the "Cheque No." (if not already filled)
            </p>
            <div class="text-center">
                <img src="{{urlOrPath('img/help/reconciliation/1-4-4-3.jpg')}}" alt=""/>
            </div>
        </li>
        <li style="padding: 5px">
            <p class="font-16">
                <strong>*</strong>
                It is not necessary to fill the amount at this step.
            </p>
        </li>
        <li style="padding: 5px">
            <p class="font-16">
                <strong class="color-red">Important: </strong>
                To store the changes, users must press the <strong class="color-orange">ENTER</strong> button
                (CD1 will be automatically generated).
            </p>
        </li>
    </ol>
    <hr class="mt-4 mb-2"/>

    <p class="font-16 text-left mt-3">
        <b>Edit</b>
    </p>
    <p class="font-16 text-left">
        <strong>*</strong>
        It is not necessary to fill the amount at this step.
    </p>
    <p class="font-16 text-left">
        <strong class="color-red">Important:</strong>
    </p>
    <ul class="text-left">
        <li>
            To store the changes, users must press <strong class="color-orange">ENTER</strong>.
        </li>
        <li>
            Any changes made at this stage are validated with <strong class="color-green">CD1</strong> (Check Digit 1)
        </li>
    </ul>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-4-4.jpg')}}" alt=""/>
    <hr class="mt-4 mb-2"/>

    <p class="font-16 text-left mt-3">
        <b>Correct rejected</b>
    </p>
    <p class="font-16 text-left">
        <strong>*</strong>
        It is not necessary to fill the amount at this step.
    </p>
    <p class="font-16 text-left">
        <strong class="color-red">Important:</strong>
    </p>
    <ul class="text-left">
        <li>
            To store the changes, users must press <strong class="color-orange">ENTER</strong>.
        </li>
        <li>
            Any changes made at this stage are validated with <strong class="color-green">CD1</strong> (Check Digit 1)
        </li>
    </ul>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-4-5.jpg')}}" alt=""/>
    <hr class="mt-4 mb-2"/>

    <p class="font-16 text-left mt-3">
        <b>Data entry</b>
    </p>
    <p class="font-16 text-left">
        <strong>*</strong>
        It is necessary to fill the amount at this step.
    </p>
    <div class="text-center">
        <img src="{{urlOrPath('img/help/reconciliation/1-4-4-7.jpg')}}" alt=""/>
    </div>
    <p class="font-16 text-left">
        <strong class="color-red">Important:</strong>
    </p>
    <ul class="text-left">
        <li>
            To store the changes, users must press <strong class="color-orange">ENTER</strong>.
        </li>
    </ul>
    <p class="font-16 text-left">
        Each time a cheque is edited; the "Cheques Amount" and "Amount Diff." is updated to reflect the new total values.
        This way users are able to visualize their data entry progress.
    </p>
    <div class="text-center">
        <img src="{{urlOrPath('img/help/reconciliation/1-4-4-8.jpg')}}" alt=""/>
    </div>
    <img src="{{urlOrPath('img/help/reconciliation/1-4-4-6.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Once the data entry process has been completed that Batch Amount and Cheques Amount must be equal.
    </p>
</div>
