<div class="help-item text-center">
    <p class="font-16 text-left">
        5. Validate and Lock<br/>
        Once all batches have been processed and there isn't any amount difference, the last table row will be marked
        with green color indicating that we have successfully Reconciled our batches.
    </p>
    <p class="font-16 text-left">
        The next step is to Validate and Lock the session by clicking the
        <img src="{{urlOrPath('img/help/reconciliation/validate_lock.jpg')}}" alt=""/>
        button.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-5.jpg')}}" alt=""/>
</div>
<div class="help-item text-center">
    <p class="font-16 text-left">
        1. This operation checks for duplicate documents and validates Check Digit 1 (CD1) inside all session batches.
    </p>
    <p class="font-16 text-left">
        If the validation finds any errors, the following message will appear:
    </p>
    <div class="text-center">
        <img src="{{urlOrPath('img/help/reconciliation/2-1.jpg')}}" alt=""/>
    </div>
    <p class="font-16 text-left mt-3">
        The system will automatically be redirected to an error handling page (users can also click on the link
        <strong class="color-blue">Click to view the validation errors.</strong>)
    </p>
    <div class="text-center">
        <img src="{{urlOrPath('img/help/reconciliation/2-2.jpg')}}" alt=""/>
    </div>
    <p class="font-16 text-left mt-3">
        Hovering the cursor over <strong class="color-red">!</strong> of the last column, will open up a popup message
        with the linked error of the document.
    </p>
    <div class="text-center">
        <img src="{{urlOrPath('img/help/reconciliation/2-3.jpg')}}" alt=""/>
    </div>
    <p class="font-16 text-left mt-3">
        Clicking <img src="{{urlOrPath('img/help/reconciliation/correct_errors.jpg')}}" alt=""/> button on the top right
        will load a new page (with very similar layout of the Reconciliation operation). In this screen users are able
        to correct the errors.
    </p>
</div>
<hr class="mt-4 mb-2"/>

<div class="help-item text-center">
    <p class="font-16 text-left">
        2. If case of duplicate documents, users must delete all duplicates except one.
    </p>
    <p class="font-16 text-left">
        <strong class="color-red">Important: </strong> If the duplicate document being deleted is the only document of
        the batch, then the batch will also be deleted (since it will be empty).
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-4.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        3. In case of rejected documents, click the <img src="{{urlOrPath('img/help/reconciliation/correct_rejected.jpg')}}" alt=""/>
        button on the top right to correct them.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-5.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        4. Once all errors have been corrected, the document table will look like the following screenshot. Click the
        back button <img src="{{urlOrPath('img/help/reconciliation/back.jpg')}}" alt=""/> on the top right.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-6.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        5. The status of the batches that were affected by errors is: "<strong class="color-light-blue-200">Errors</strong>".
        Users will need to click the link and inspect the correction page for any remaining issues. Once done, click on
        the back button <img src="{{urlOrPath('img/help/reconciliation/back.jpg')}}" alt=""/> on the top right.
    </p>
    <p class="font-16 text-left">
        The status will then change to "<strong class="color-dark-grey">Processed</strong>". Re-run the validation
        process by clicking <img src="{{urlOrPath('img/help/reconciliation/validate_lock.jpg')}}" alt=""/>
        on the top right corner. If no further issues are found, the following message will appear:
    </p>
    <div class="text-center">
        <img src="{{urlOrPath('img/help/reconciliation/2-7.jpg')}}" alt=""/>
    </div>
    <p class="font-16 text-left">
        To dismiss the messages click on "<strong class="color-blue">Hide Completed</strong>"
    </p>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        6. If the validation process completes successfully, the status ofthe batches will change to
        "<strong class="color-green">Locked</strong>"
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-8.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        6.1. If there is a need to unlock a batch <strong>BEFORE</strong> the end of the session, click on the
        "<strong class="color-green">Locked</strong>" link and then click on the
        <img src="{{urlOrPath('img/help/reconciliation/unlock_batch.jpg')}}" alt=""/> button on the top right corner
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-9.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        7. To End the session click the <img src="{{urlOrPath('img/help/reconciliation/end_of_session.jpg')}}" alt=""/>
        button on the top right corner (This marks that the Branch clearing related operations have concluded for the day).
    </p>
    <p class="font-16 text-left">
        <strong class="color-red">Important: </strong> All batches status should be
        "<strong class="color-green">Locked</strong>"
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-10.jpg')}}" alt=""/>
</div>
<div class="help-item text-center">
    <p class="font-16 text-left">
        8. If the End of Session is successful, the cheques are transfered to the Archive and all batches should be closed.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/2-11.jpg')}}" alt=""/>
</div>
