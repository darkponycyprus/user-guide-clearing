<div class="help-item text-center">
    <p class="font-16 text-left">
        The reconciliation operation applies to various processes. It is represented multiple times in the menu under each
        applicable operation. The current example simulates <b>Reconciliation</b> for the process of
        <b>OUTWARD CLEARING</b>.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/0-1.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        1. Clicking Reconciliation on the left side menu triggers the select Branch and date selection modal. For some users the branch selection may not appear.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/0-2.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        2. Select the applicable Branch and then the date for which you wish to reconcile the cheques.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/0-3.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        3. Then, click the <img src="{{urlOrPath('img/help/reconciliation/proceed.jpg')}}" alt=""/> button. The window
        will refresh and load all scan batches for the selected Branch and date.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/0-4.jpg')}}" alt=""/>
</div>
