<div class="help-item text-center">
    <p class="font-16 text-left">
        Selecting a Branch and a date for Reconciliation will load the following page with a list of batches.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-1.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To select a different branch or to change the date click on
        <img src="{{urlOrPath('img/help/reconciliation/edit.jpg')}}" alt=""/>
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-2.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Each row of this table represents a batch. To process a batch click on the last column - the status. Depending
        on the current status and the permissions of each user the batch may not be available for process. Below is a
        list with all possible statuses:
    </p>
    <ul class="text-left">
        <li>
            <b>Unprocessed:</b>
            The batch was recently scanned and still needs to be processed (correct rejects, data entry etc.)
        </li>
        <li>
            <b>Processed:</b>
            The batch has been processed, documents have been corrected and amounts have been filled either by manual
            entry or by service synchronization
        </li>
        <li>
            <b>Errors:</b>
            The batch contains errors. The operations that can produce this status are: Validate and Lock and End Session.
        </li>
        <li>
            <b>Closed:</b>
            The batch is closed and cannot be further edited. Only admins can unlock it.
        </li>
    </ul>
    <img src="{{urlOrPath('img/help/reconciliation/1-3.jpg')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To edit a batch and its documents click on its status. The Reconciliation screen of the batch will load.
    </p>
    <img src="{{urlOrPath('img/help/reconciliation/1-4.jpg')}}" alt=""/>
</div>
