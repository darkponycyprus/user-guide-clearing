@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="mb-3 section-title">
            Reconciliation
        </h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#branch-date" aria-controls="scan" role="tab" data-toggle="tab">
                    Select branch & date
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#batches" aria-controls="scan" role="tab" data-toggle="tab">
                    Batches
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#single-batch" aria-controls="scan" role="tab" data-toggle="tab">
                    Single batch
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#validate" aria-controls="scan" role="tab" data-toggle="tab">
                    Validate & Lock
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="branch-date" role="tabpanel">
                @include('pages.help.reconciliation.partials.branch-date')
            </div>
            <div class="tab-pane" id="batches" role="tabpanel">
                @include('pages.help.reconciliation.partials.batches')
            </div>
            <div class="tab-pane" id="single-batch" role="tabpanel">
                @include('pages.help.reconciliation.partials.single-batch')
            </div>
            <div class="tab-pane" id="validate" role="tabpanel">
                @include('pages.help.reconciliation.partials.validate')
            </div>
        </div>
    </div>
@endsection
