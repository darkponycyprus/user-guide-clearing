<h1>Reconciliation</h1>
<h2>Select branch & date</h2>
@include('pages.help.reconciliation.partials.branch-date')
<h2>Batches</h2>
@include('pages.help.reconciliation.partials.batches')
<h2>Single batch</h2>
@include('pages.help.reconciliation.partials.single-batch')
<h2>Validate & Lock</h2>
@include('pages.help.reconciliation.partials.validate')
