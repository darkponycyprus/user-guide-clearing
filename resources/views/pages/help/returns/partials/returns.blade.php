<div class="help-item text-center">
    <p class="font-16 text-left">
        Once you have imported the inward ECCS files you can see the inward returns by clicking on the Returns in the menu, under the section Inward Returns
    </p>
    <img src="{{urlOrPath('img/help/returns/0.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In the <b>Information</b> section you can see which cheques are <b>Completed</b> and which cheques are <b>Pending</b>.
    </p>
    <ul class="text-left">
        <li><b>Completed</b> cheques are the ones that are scanned with the appropriate return reason.</li>
        <li><b>Pending</b> cheques are the ones that are not scanned.</li>
    </ul>
    <img src="{{urlOrPath('img/help/returns/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In the table below you can see all the returned cheques. Among the other information presented you can also see the return reason. On the end of the row, a color is used to indicate whether the cheque is
        <span class="color-red">Pending</span> or <span class="color-green">Completed</span>.
    </p>
    <img src="{{urlOrPath('img/help/returns/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">If you are logged in as CPU then you an also see the branch that the cheque belongs to
    </p>
    <img src="{{urlOrPath('img/help/returns/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In order to <b>Complete</b> the <b>Pending</b> cheques; on your left side <b>MENU</b>,
        click <b>Scan</b> under section <b>INWARD RETURNS</b>.
        Scanning under <b>INWARD RETURNS</b> has only one major difference between scanning in any other section.
        When clicking on <b>Create Batch</b>, you will notice that you can enter only the number of cheques
    </p>
    <img src="{{urlOrPath('img/help/returns/5.png')}}" alt=""/>
    <img src="{{urlOrPath('img/help/returns/6.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">Once you have scanned the returned cheques you must follow the Reconciliation process, so visit Reconciliation under the section Inward Returns.</p>
    <img src="{{urlOrPath('img/help/returns/9.png')}}" alt=""/>
    <img src="{{urlOrPath('img/help/returns/10.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        When the Reconciliation is completed, if you visit the returned cheques screen you will notice that the cheques that matched those that were scanned have their status changed to <span class="color-green">Completed</span>
    </p>
    <img src="{{urlOrPath('img/help/returns/7.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you have scanned all the returned cheques then the Pending number from the information box should be set to 0.
    </p>
    <img src="{{urlOrPath('img/help/returns/8.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
