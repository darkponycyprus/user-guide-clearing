<div class="help-item text-center">
    <p class="font-16 text-left">
        If you wish to export the following data in a <b>.PDF file</b> then click on the <img src="{{urlOrPath('img/help/returns/btn_export.png')}}" alt="Export"/> button at the top-right corner of your screen.
    </p>
    <img src="{{urlOrPath('img/help/returns/4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Click save and open the <b>.PDF file</b>. You should see something like the example below
    </p>
    <img src="{{urlOrPath('img/help/returns/export.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
