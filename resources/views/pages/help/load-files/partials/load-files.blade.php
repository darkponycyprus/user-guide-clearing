<div class="help-item text-center">
    <p class="font-16 text-left">
        In order to load Inward Clearing files from ECCS select wither XML or DAT from the menu
    </p>
    <img src="{{urlOrPath('img/help/load-files/0.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">In the next screen you need to select the presentment date from the dropdown list and the click the <img src="{{urlOrPath('img/help/load-files/btn_load.png')}}" alt="Load Files"/> button</p>
    <img src="{{urlOrPath('img/help/load-files/01.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">The files are temporarily loaded and they are checked for errors. If an error is found it will be presented with <img src="{{urlOrPath('img/help/load-files/btn_error.png')}}" alt=""/>. You can hover over it to see the details of the error.</p>
    <img src="{{urlOrPath('img/help/load-files/02.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In order to load the cheques from the files you need to click the <img src="{{urlOrPath('img/help/load-files/btn_loadcheques.png')}}" alt="Load Cheques"/> button.
    </p>
    <img src="{{urlOrPath('img/help/load-files/03.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        When the progress of the task is completed, you will be presented with a report page, showing the imported cheques per bank. At this stage the cheques were imported into a temporary database table and you need to transfer them to the scan items table, by clicking the button <img src="{{urlOrPath('img/help/load-files/btn_import.png')}}" alt="Import"/> button
    </p>
    <img src="{{urlOrPath('img/help/load-files/04.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        When the import task is completed you will be automatically redirected to the <b>INWARD CLEARING - Reconciliation</b>
        section where you can see all your imported cheques.
    </p>
    <img src="{{urlOrPath('img/help/load-files/06.png')}}" alt=""/>
</div>
