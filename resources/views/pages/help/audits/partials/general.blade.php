<div class="help-item text-center">
    <p class="font-16 text-left">
        When you visit the Audit Trail page you are presented with the list of all registered audits. Every interaction with the database by a user or a job is recorded in this section. In the list you are presented with:
    </p>
    <ul class="text-left">
        <li><b>Datetime: </b>The datetime timestamp that the event occured</li>
        <li><b>Event: </b>The event that occured(created, deleted, updated, etc.)</li>
        <li><b>User: </b>User responsible for the change</li>
        <li><b>Table: </b>Database table</li>
        <li><b>Record details: </b>Soem details regarding the record</li>
    </ul>
    <img src="{{urlOrPath('img/help/audits/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you click on the View link on the right side, you will be presented with the old-new values of the changed record.
    </p>
    <img src="{{urlOrPath('img/help/audits/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To narrow down the list of audits presented you can apply some filters. Click on the filters button and the filter modal will pop up. Once you have selected the desired filters click on the Search button. When the modal is then reopened the applied filters from the previous search are selected.
    </p>
    <img src="{{urlOrPath('img/help/audits/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you have applied at least one filter, you are able to export the audit records by clicking on the export button.
    </p>
    <img src="{{urlOrPath('img/help/audits/4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
