@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="mb-3 section-title">
            Audit Trail
        </h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#audits" aria-controls="scan" role="tab" data-toggle="tab">
                    General
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="audits" role="tabpanel">
                @include('pages.help.audits.partials.general')
            </div>
        </div>
    </div>
@endsection
