<div class="help-item text-center">
    <p class="font-16 text-left">
        When you visit the scanners page you are presented with the list of all registered scanners. In the far right of each row there is a color indication showcasing whether the scanner is active or not. If the status is set to inactive then it means that it cannot be used. Above the table there are some filters that you can apply to narrow down the scanners list.
    </p>
    <img src="{{urlOrPath('img/help/scanners/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you click on the view button, you can see the list of users that are assigned to the scanner.
    </p>
    <img src="{{urlOrPath('img/help/scanners/2.png')}}" alt=""/>
    <img src="{{urlOrPath('img/help/scanners/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To add a new scanner click on the Add new button
    </p>
    <img src="{{urlOrPath('img/help/scanners/4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To add a new scanner click on the Add new button
    </p>
    <img src="{{urlOrPath('img/help/scanners/4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In the following screen enter the required fields
    </p>
    <ul class="text-left">
        <li><b>Name: </b>Name of the scanner</li>
        <li><b>Device: </b>If the scanner is the Panini EverneXt web scan set to "Evernext"</li>
        <li><b>Type: </b>If the scanner is the Panini EverneXt web scan set to "ethernet"</li>
        <li><b>Branch: </b>The scanner's branch</li>
        <li><b>Branch scanner number: </b>Scanner must be assigned with a unique number for each branch. The list of available numbers will automatically exclude the already registered ones. For example if for the branch 101 you already have created a scanner record and you have set its number to 1, if you create a new one the number 1 will not exist in the list.</li>
        <li><b>Ip Address: </b>The ip of the scanner, e.g http://192.168.178.134 or 192.168.178.134 (you can skip the http protocol)</li>
        <li><b>Port: </b>You can leave that empty, as it is filled by SmartClear</li>
        <li><b>Status: </b>Status of the scanner(Active - Inactive)</li>
    </ul>
    <img src="{{urlOrPath('img/help/scanners/5.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Once the scanner is created, below the canner details table you will be presented with the option to assign it to branch users. To assign the scanner to a user simply ceck the checkbox at the left side of the user's row.
    </p>
    <img src="{{urlOrPath('img/help/scanners/6.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        You can edit/delete a scanner if you click on the highlighted buttons.
    </p>
    <img src="{{urlOrPath('img/help/scanners/7.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
