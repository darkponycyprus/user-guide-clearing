<div class="help-item text-center">
    <p class="font-16 text-left">
        In the jobs screen you can see all the recorded system jobs. A Job is a background process that usually handles time consuming tasks, such as the validation of the cheques in the validate session process. If a job has failed you need to contact a system administrator. You have the ability to delete a failed job so that it does not constantly appear on the bottom right section of the screen.
    </p>
    <img src="{{urlOrPath('img/help/jobs/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
