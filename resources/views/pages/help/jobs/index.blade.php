@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="mb-3 section-title">
            Jobs
        </h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#jobs" aria-controls="scan" role="tab" data-toggle="tab">
                    General
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="jobs" role="tabpanel">
                @include('pages.help.jobs.partials.general')
            </div>
        </div>
    </div>
@endsection
