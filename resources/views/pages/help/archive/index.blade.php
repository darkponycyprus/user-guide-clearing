@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="text-center mb-3">Archive user guide</h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation"><a class="nav-link active" href="#scan" aria-controls="scan" role="tab" data-toggle="tab">Archive</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="#edit" aria-controls="scan" role="tab" data-toggle="tab">Edit</a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="#delete" aria-controls="scan" role="tab" data-toggle="tab">Delete/Restore</a></li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="scan" role="tabpanel">
                @include('pages.help.archive.partials.archive')
            </div>
            <div class="tab-pane" id="edit" role="tabpanel">
                @include('pages.help.archive.partials.edit')
            </div>
            <div class="tab-pane" id="delete" role="tabpanel">
                @include('pages.help.archive.partials.delete_restore')
            </div>
        </div>
    </div>
@endsection
