<div class="help-item text-center">
    <p class="font-16 text-left">To edit an archive cheque, navigate to the Archive screen and click on the edit button. You may need to scroll horizontally to see the action buttons</p>
    <img src="{{urlOrPath('img/help/archive-clearing/edit/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">In the following screen you are pesented with the data of that specific cheque. You can edit the codeline parts and the amount from the below section</p>
    <img src="{{urlOrPath('img/help/archive-clearing/edit/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">If you change the amount you need to change the check digit 2 as well. To do that you can click on the <img src="{{urlOrPath('img/help/archive-clearing/edit/3.png')}}" alt=""/> button so that the system can generate it for the new amount. To save the changes click on the Save changes button at the right top corner <img src="{{urlOrPath('img/help/archive-clearing/edit/4.png')}}" alt=""/>. Before saving the new values SmartClear performs validation for the two chech digits, if an error is found you will be notified accordingly <img src="{{urlOrPath('img/help/archive-clearing/edit/5.png')}}" alt=""/></p>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">You can also rotate/swap the image of the document, if for any reason it is not correct and needs to be edited.</p>
    <img src="{{urlOrPath('img/help/archive-clearing/edit/6.png')}}" alt=""/>
</div>
