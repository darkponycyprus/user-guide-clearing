<div class="help-item text-center">
    <p class="font-16 text-left">When searching for an archive first select the document type from the menu</p>
    <img src="{{urlOrPath('img/help/archive-clearing/00.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">Apply the desired filters and click <img src="{{urlOrPath('img/help/archive-clearing/btn_search.png')}}" alt=""></p>
    <p class="font-16 text-left">If you want to clear all the filters you have entered, click <img src="{{urlOrPath('img/help/archive-clearing/btn_clear.png')}}" alt=""></p>
    <img src="{{urlOrPath('img/help/archive-clearing/01.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">In the next screen there are 3 main sections</p>
    <ul class="text-left">
        <li><b>Search Information:</b> Filters applied</li>
        <li><b>Image of cheque:</b> The image of the selected image. When the page first loads the selected image by default is the first from the list. If you click on a document from the list the image section will be refreshed.</li>
        <li><b>List of items:</b> Archived cheques list.</li>
    </ul>
    <img src="{{urlOrPath('img/help/archive-clearing/03.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">In example we chose we applied 3 filters</p>
    <ul class="text-left">
        <li>Process: 23 (Outward clearing)</li>
        <li>Cheque account number contains '789'</li>
        <li>Return reason from code 1 to anything</li>
    </ul>
    <img src="{{urlOrPath('img/help/archive-clearing/04.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">You can order the list of items by a specific column, by clicking on the green headers. You can also see the direction of the order, if the arrow is looking up then the order is in ascending order.</p>
    <img src="{{urlOrPath('img/help/archive-clearing/05b.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">You can export a list of cheques by selecting the items from the checkboxes one the left, and the selecting the export type for the Actions dropdown on the right.</p>
    <img src="{{urlOrPath('img/help/archive-clearing/07.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">You can change how many items there are shown per page by selecting a new valye from the dropdown at the right bottom corner</p>
    <img src="{{urlOrPath('img/help/archive-clearing/09.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        Selecting a document from the table (either by clicking or by using the arrow keys), loads the corresponding
        image in the preview section. The cheque images can be further manipulated via the following buttons:
    </p>
    <img src="{{urlOrPath('img/help/archive-clearing/10.png')}}" alt=""/>
    <table class="table">
        <thead>
        <tr>
            <th>Icon</th>
            <th class="text-left">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/rotate.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Rotate the image and update the cheque.
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/flip.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Flip the image - preview only.
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/swap.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Swap front and back image sides and update the cheque.
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/reconciliation/zoom.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Zoom in/out
            </td>
        </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
</div>
