<div class="help-item text-center">
    <p class="font-16 text-left">To delete an archive cheque, navigate to the Archive screen and click on the delete button. You may need to scroll horizontally to see the action buttons</p>
    <img src="{{urlOrPath('img/help/archive-clearing/edit/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">You will be presented with a confirmation popup</p>
    <img src="{{urlOrPath('img/help/archive-clearing/delete/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">If the record was deleted succesfully you will be notified accordingly and the background color of the table row will change to <span class="color-red">red</span></p>
    <img src="{{urlOrPath('img/help/archive-clearing/delete/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">If you want to restore a deleted record, simply click on the restore button</p>
    <img src="{{urlOrPath('img/help/archive-clearing/delete/3.png')}}" alt=""/>
</div>
