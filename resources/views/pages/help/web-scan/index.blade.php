@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="mb-3 section-title">
            Web Scan
        </h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#scan" aria-controls="scan" role="tab" data-toggle="tab">
                    Scan
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#import-batch" aria-controls="scan" role="tab" data-toggle="tab">
                    Add documents to most recent batch
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#errors-warnings" aria-controls="scan" role="tab" data-toggle="tab">
                    Scanner errors
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#jam" aria-controls="jam" role="tab" data-toggle="tab">
                    Scanner jam
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="scan" role="tabpanel">
                @include('pages.help.web-scan.partials.scan')
            </div>
            <div class="tab-pane" id="errors-warnings" role="tabpanel">
                @include('pages.help.web-scan.partials.errors')
            </div>
            <div class="tab-pane" id="import-batch" role="tabpanel">
                @include('pages.help.web-scan.partials.add-docs-to-batch')
            </div>
            <div class="tab-pane" id="jam" role="tabpanel">
                @include('pages.help.web-scan.partials.jam')
            </div>
        </div>
    </div>
@endsection
