<h1>Web scan</h1>
<h2>General</h2>
@include('pages.help.web-scan.partials.scan')
<h2>Add documents to an already created batch</h2>
@include('pages.help.web-scan.partials.add-docs-to-batch')
<h2>Scanner errors</h2>
@include('pages.help.web-scan.partials.errors')
<h2>Scanner jam</h2>
@include('pages.help.web-scan.partials.jam')
