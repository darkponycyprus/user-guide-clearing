<div class="help-item text-center">
    <p class="font-16 text-left">
        The scan operation applies to various processes. For this reason its represented multiple times in the menu under
        different operations. The current example simulates a <b>Scan</b> for the process of <b>OUTWARD CLEARING</b>.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/intro.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        1. Before starting please make sure that the scanner is connected and ready to use. This can verified by inspecting
        the top-right corner of the Scan window.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        2. Next step is to create a batch (you can NOT scan a document without a batch!). To achieve this click on the
        <img src="{{urlOrPath('img/help/web-scan/create-batch.jpg')}}" alt=""/> button.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        3. During the batch creation you will need to fill up all necessary information (different processes may have
        slightly different input fields):
    </p>
    <ul class="text-left font-16">
        <li>Batch items: Must be equal to the number of cheques to be scanned.</li>
        <li>Amount: Must be equal to the total amount of all cheques.</li>
    </ul>
    <p class="font-16 text-left">
        Once all values have been filled, click the <img src="{{urlOrPath('img/help/web-scan/save.jpg')}}" alt=""/> button.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        4. On successful batch creation the batch details will be visible in the highlighted area. On page reload,
        SmartClear will fetch the details of the most recent batch. Please be aware that if the most recent batch is empty
        you will not be able to create a new one. Each batch must have at-least one scan item.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        5. The basic Scan Actions are performed using the following buttons:
    </p>
    <table class="table">
        <thead>
        <tr>
            <th>Icon</th>
            <th class="text-left">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/play.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Starts the scan operation
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/stop.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Stops the scan
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/jam.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Clears the scanner's track in case of a jam
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/disconnect.jpg')}}" alt=""/>
            </td>
            <td class="text-left">
                Used to forcibly disconnect the scanner (in case of issues with automatic disconnect)
            </td>
        </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
    <p class="font-16 text-left">
        While "Stop" and "Clear" operations can be triggered automatically by SmartClear, once the scanner has stopped
        scanning for a predefined duration, the "Start" scan action is always triggered by a user action.
        <br/>
        Please make sure you that all documents are placed correctly inside the scanner's feeder and with the proper
        orientation (<b>the front side of the cheque must face away from the scanner</b>).
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step5.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        6. During scan operations the status changes from "READY" to "FEEDING". <span class="color-red">Important:</span>
        To prevent possible data loss, please avoid manually stopping the scan process (unless absolutely necessary).
        The scanner will automatically stop "Feeding" a few seconds after the last document has been processed.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step6.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        7. Once the scan operation completes, the screen contents will be updated (similarly with the screenshot below).
        In the <span class="color-blue">Batch scanned documents</span> area we have a quick summary of the scan process that
        includes the number of: Successful, failed and rejected documents of this batch.
    </p>
    <ul class="font-16 text-left">
        <li>
            <span class="color-red">Failed</span> means that these documents were not stored, something went wronf in the system, contact a system administrator.
        </li>
        <li>
            <span class="color-red">Rejected</span> means that these documents were stored but are not valid for some reason
            (e.g the scanner returned the character '?' instead of a digit).
        </li>
        <li>
            If the number of scanned documents is not equal with the information defined during batch creation,
            then the "Total" will be <span class="color-red">red</span>.
        </li>
    </ul>
    <img src="{{urlOrPath('img/help/web-scan/step7.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        8. Scanned documents are displayed in this table. The <span class="color-green">Stored</span> status means the
        document was stored successfully without any errors
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step8.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        9. Rejected documents are identified via the <span class="color-red">Rejected</span> status. This means
        that the document was stored successfully but with errors. To see the error/s, hover over the exclamation mark.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step9.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        10. Lastly, in the image preview area you can scanned images and manipulate them:
    </p>
    <table class="table">
        <thead>
        <tr>
            <th>Icon</th>
            <th class="text-left">Description</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/btn_flip.png')}}" alt=""/>
            </td>
            <td class="text-left">
                Flip image (show back side)
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/btn_rotate.png')}}" alt=""/>
            </td>
            <td class="text-left">
                Rotates image
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/btn_slider.png')}}" alt=""/>
            </td>
            <td class="text-left">
                Zoom in/out
            </td>
        </tr>
        <tr>
            <td>
                <img src="{{urlOrPath('img/help/web-scan/btn_default.png')}}" alt=""/>
            </td>
            <td class="text-left">
                Reverts image to original condition
            </td>
        </tr>
        </tbody>
        <tfoot></tfoot>
    </table>
    <img src="{{urlOrPath('img/help/web-scan/step10.png')}}" alt=""/>
</div>
