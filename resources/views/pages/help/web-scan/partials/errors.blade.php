<div class="help-item text-center">
    <p class="font-16 text-left">
        Any bubble <span class="color-red">ERROR</span>: <img src="{{urlOrPath('img/help/web-scan/error_bubble.png')}}" alt=""/>
        These errors mean that the scanner returned an error. Errors will persist until the user closes them (please
        make sure that you read the message before discarding it). Bellow are some possible errors along with suggested
        actions:
    </p>
    <ul class="text-left">
        <li><b>AppInstId missing</b> Could not initialize the connection with scanner - Restart the scanner</li>
        <li>
            <b>Double Feeding detected</b> - The scanner stopped because it detected that two documents were scanned at the same
            time - Clear the track if the documents are stuck there and press the scan button again.
        </li>
        <li><b>GUID already set (i.e. another application connected)</b> - Restart the scanner.</li>
        <li><b>GUID not set (i.e. application is not connected)</b> - Restart the scanner.</li>
        <li><b>Invalid Send Printer sequence</b> - Change endorsing area(1-4) from the edit scanner screen</li>
    </ul>
</div>
