<div class="help-item text-center">
    <p class="font-16 text-left">
        1. The scan screen always loads the latest batch of <em>the day</em> along with its documents.
        To add cheques to this batch follow the scan process, without creating a new batch.
    </p>
    <img src="{{urlOrPath('img/help/web-scan/step4.png')}}" alt=""/>
</div>
