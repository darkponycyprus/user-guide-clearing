<div class="help-item">
    <p class="font-16 text-left">
        In the event of a jam:
    </p>
    <ul>
        <li>If there are documents stuck in the scanner press the clear button <img src="{{urlOrPath('img/help/web-scan/jam.jpg')}}" alt=""/></li>
        <li>There is a possibility that some of the scanned documents may not have been not stored. In such a case the original document will need to retrieved from the scanner and be rescanned.</li>
    </ul>
</div>
