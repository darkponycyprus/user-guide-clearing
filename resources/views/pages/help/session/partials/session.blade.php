<div class="help-item text-center">
    <p class="font-16 text-left">
        The sessions operation applies to various processes. For this reason its represented multiple times in the menu under
        different operations. The current example simulates a <b>Session</b> for the process of <b>OUTWARD CLEARING</b>.
    </p>
    <img src="{{urlOrPath('img/help/sessions/0.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">In the following screen you can see the sessions of the current day for the branches that has started one. If a branch did not start the session of the day, it will not be present in the list.</p>
    <img src="{{urlOrPath('img/help/sessions/01.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">For each branch you can see the following information: </p>
    <ul class="text-left">
        <li>
            <b>Branch:</b>
            Name of the branch
        </li>
        <li>
            <b>Batches:</b>
            Number of batches
        </li>
        <li>
            <b>Cheques:</b>
            Total count of cheques
        </li>
        <li>
            <b>Opened at:</b>
            When the session was opened
        </li>
        <li>
            <b>Closed at:</b>
            When the session was closed. If this column is empty then the session is not closed
        </li>
        <li>
            <b>Deleted at:</b>
            When the session was deleted. If a session is deleted it can be restored
        </li>
    </ul>
    <img src="{{urlOrPath('img/help/sessions/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">If the session is closed and you would like to open it, click the <img src="{{urlOrPath('img/help/sessions/unlock.png')}}" alt=""/> icon</p>
    <img src="{{urlOrPath('img/help/sessions/2.png')}}" alt=""/>
    <p class="font-16 text-left">In the popup press Yes to unlock it</p>
    <img src="{{urlOrPath('img/help/sessions/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">If a session is not closed then you have the option to delete it.</p>
    <img src="{{urlOrPath('img/help/sessions/4.png')}}" alt=""/>
</div>
