<div class="help-item text-center">
    <p class="font-16 text-left">
        When you visit the users page you are presented with the list of all registered users. In the far right of each row there is a color indication showcasing whether the user is active or not. If the status is set to inactive then it means that he/she cannot log in SmartClear. Above the table there are some filters that you can apply to narrow down the users list.
    </p>
    <img src="{{urlOrPath('img/help/users/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you click on view you can see the user's roles
    </p>
    <img src="{{urlOrPath('img/help/users/2.png')}}" alt=""/>
    <img src="{{urlOrPath('img/help/users/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        From the buttons at the top right corner of the screen you can create a new user as well as export the existing ones.
    </p>
    <img src="{{urlOrPath('img/help/users/4.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        When creating or editing a user, you are presented with 3 section
    </p>
    <ul class="text-left">
        <li>
            <p><b>Information</b></p>
            <img src="{{urlOrPath('img/help/users/5.png')}}" alt=""/>
        </li>
        <li>
            <p><b>Assign roles per branch: </b>In this section you can select a branch from the dropdown and then assign a role to the user</p>
            <img src="{{urlOrPath('img/help/users/6.png')}}" alt=""/>
        </li>
        <li>
            <p><b>List of roles for all branches</b></p>
            <img src="{{urlOrPath('img/help/users/7.png')}}" alt=""/>
        </li>
    </ul>
</div>
<hr class="mt-4 mb-2"/>
