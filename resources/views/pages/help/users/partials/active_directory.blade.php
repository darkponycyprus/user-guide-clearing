<div class="help-item text-center">
    <p class="font-16 text-left">
        If there is an integration with Active Directory and SmartClear, when creating a user you are presented with two sections. On the left one there are the users available for sync in the active directory, and on the right there are the users imported in SmartClear. To import a new user in SmartClear you can select the name and then clin on the arrow -> or double click. When you have transfered all desired users at the right section you need to click on the Save Changes button.
    </p>
    <img src="{{urlOrPath('img/help/users/21.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
