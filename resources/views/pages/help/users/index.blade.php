@extends('layouts.help.main')

@section('content')
    <div id="help-content-section" class="py-3">
        <h3 class="mb-3 section-title">
            Users
        </h3>
        <!-- Nav tabs-->
        <ul class="nav nav-tabs mb-4" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="#database" aria-controls="scan" role="tab" data-toggle="tab">
                    General
                </a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="#active-directory" aria-controls="scan" role="tab" data-toggle="tab">
                    Active Directory
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="database" role="tabpanel">
                @include('pages.help.users.partials.database')
            </div>
            <div class="tab-pane" id="active-directory" role="tabpanel">
                @include('pages.help.users.partials.active_directory')
            </div>
        </div>
    </div>
@endsection
