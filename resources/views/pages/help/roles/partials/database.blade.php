<div class="help-item text-center">
    <p class="font-16 text-left">
        When you visit the Roles page you are presented with the list of all registered roles.
    </p>
    <img src="{{urlOrPath('img/help/roles/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        When creating or editing a role, the role's information fields are presented in the left part of the screen. For the level value please be advised from the system administrator.
    </p>
    <img src="{{urlOrPath('img/help/roles/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        and in the right part of the screen you can setup the role's permissions, for the separate sections of SmartClear
    </p>
    <img src="{{urlOrPath('img/help/roles/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        If you click on the title of a permission section, you can see its subsections.
    </p>
    <img src="{{urlOrPath('img/help/roles/4.png')}}" alt=""/>
    <img src="{{urlOrPath('img/help/roles/5.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
