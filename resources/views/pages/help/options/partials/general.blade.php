<div class="help-item text-center">
    <p class="font-16 text-left">
        In the options screen you can see grouped system parameters, that can be personalized per SmartClear installation. You can just edit the values of existing ones, creating one will not have an impact to how the system works.
    </p>
    <img src="{{urlOrPath('img/help/options/1.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        To edit one click on the edit button
    </p>
    <img src="{{urlOrPath('img/help/options/2.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In the following screen click on the Settings tab, do not change anything from the Information tab
    </p>
    <img src="{{urlOrPath('img/help/options/3.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
<div class="help-item text-center">
    <p class="font-16 text-left">
        In the setting tab, edit the desired value(e.g the "export": true to "export": false) and click the Save Changes button
    </p>
    <img src="{{urlOrPath('img/help/options/4.png')}}" alt=""/>
    <img src="{{urlOrPath('img/help/options/5.png')}}" alt=""/>
</div>
<hr class="mt-4 mb-2"/>
