@extends('layouts.export.main')

@section('content')
    <!-- Web Scan -->
    @include('pages.help.web-scan.partials.scan')
    @include('pages.help.web-scan.partials.errors')
    @include('pages.help.web-scan.partials.add-docs-to-batch')
    <!-- Reconciliation -->
    @include('pages.help.reconciliation.partials.branch-date')
    @include('pages.help.reconciliation.partials.batches')
    @include('pages.help.reconciliation.partials.single-batch')
    @include('pages.help.reconciliation.partials.validate')
    <!-- Archive -->
    @include('pages.help.archive.partials.scan')
    <!-- Session -->
    @include('pages.help.session.partials.session')
    <!-- Foreign Cheque -->
    @include('pages.help.foreign-cheques.partials.scan')
    @include('pages.help.foreign-cheques.partials.reconciliation')
    @include('pages.help.foreign-cheques.partials.bank-accounts')
    @include('pages.help.foreign-cheques.partials.report-remittance-letter')
    @include('pages.help.foreign-cheques.partials.report-daily-bank-cheques')
    <!-- Load Files -->
    @include('pages.help.load-files.partials.load-files')
    <!-- Create Files -->
    @include('pages.help.create-files.partials.create-files')
    <!-- Returns -->
    @include('pages.help.returns.partials.returns')
    @include('pages.help.returns.partials.report-return-cheques')
    {{--

    --}}
@endsection
